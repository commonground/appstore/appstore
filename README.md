# Common Ground Componentencatalogus

This is the repository of the Common Ground Componentencatalogus.


## Developer documentation

If you would like to contribute to the project, please consult the [contributing documentation](CONTRIBUTING.md).


## Licence

Copyright © VNG Realisatie 2019

[Licensed under the EUPLv1.2](LICENCE.md)
