"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

import api.views
import core.views

admin.site.site_header = 'Componentencatalogus administration'
admin.site.site_title = 'Componentencatalogus administration'

router = routers.DefaultRouter()
router.register(r'users', core.views.UserViewSet, basename='users')
router.register(r'products', core.views.ProductViewSet, basename='products')
router.register(r'components', core.views.ComponentViewSet, basename='components')

urlpatterns = [
    path('', api.views.index),
    path('health', api.views.health),
    path('oidc/', include('mozilla_django_oidc.urls')),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
]
