from django.utils.translation import gettext_lazy as _
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

MAX_URL_LENGTH = 2000


class UserManager(BaseUserManager):
    def create_user(self, full_name, email, password=None):
        if not full_name:
            raise ValueError('Users must have a full name')

        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            full_name=full_name,
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, full_name, email, password=None):
        user = self.create_user(
            full_name=full_name,
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    full_name = models.CharField(max_length=255, db_index=True)
    email = models.EmailField(verbose_name='email address', unique=True)
    picture_url = models.CharField(max_length=MAX_URL_LENGTH, blank=True, default='')
    external_id = models.CharField(max_length=255, unique=True, null=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_product_owner = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    class Meta:
        ordering = ['full_name']

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        # pylint: disable=unused-argument
        return True

    def has_module_perms(self, app_label):
        # pylint: disable=unused-argument
        return True

    @property
    def is_staff(self):
        return self.is_admin


class Chart(models.Model):
    name = models.CharField(max_length=255)
    version = models.CharField(max_length=10)
    url = models.URLField(max_length=MAX_URL_LENGTH, blank=True, default='')
    values = models.TextField(blank=True, default='')

    def __str__(self):
        return self.name


class Component(models.Model):
    class LayerTypes(models.TextChoices):
        INTERACTIE = 'interactie', _('Interactie')
        PROCES = 'proces', _('Proces')
        INTEGRATIE = 'integratie', _('Integratie')
        SERVICES = 'services', _('Services')
        DATA = 'data', _('Data')

    class Statuses(models.TextChoices):
        GEWENST = 'gewenst', _('Gewenst')
        GEPLAND = 'gepland', _('Gepland')
        BETA = 'beta', _('Beta')
        BRUIKBAAR = 'bruikbaar', _('Bruikbaar')
        UITGEFASEERD = 'uitgefaseerd', _('Uitgefaseerd')

    class ReuseTypes(models.TextChoices):
        ZELF_INSTALLEREN = 'zelf installeren', _('Zelf installeren')
        LANDELIJKE_VOORZIENING = 'landelijke voorziening', _('Landelijke voorziening')

    class DevelopBy(models.TextChoices):
        SELF = 'self', _('Self')
        THIRDPARTY = 'thirdparty', _('Thirdparty')
        UNKNOWN = 'unknown', _('Unknown')

    class ExpectedQuarter(models.TextChoices):
        Q1 = 'Q1', _('Q1')
        Q2 = 'Q2', _('Q2')
        Q3 = 'Q3', _('Q3')
        Q4 = 'Q4', _('Q4')


    name = models.CharField(max_length=255, db_index=True)
    layer_type = models.CharField(max_length=20, choices=LayerTypes.choices)
    docker_image_uri = models.CharField(max_length=MAX_URL_LENGTH, blank=True, default='')
    located_at = models.TextField(blank=True, default='')
    technology_stack = models.TextField(blank=True, default='')
    description = models.TextField(blank=True, default='')
    organisation_name = models.CharField(max_length=255, blank=True, default='', null=True)
    contact = models.CharField(max_length=255, blank=True, default='')
    repository_url = models.URLField(max_length=MAX_URL_LENGTH, blank=True, default='')
    status = models.CharField(max_length=20, choices=Statuses.choices, default=Statuses.GEWENST)
    reuse_type = models.CharField(max_length=25, choices=ReuseTypes.choices, default=ReuseTypes.ZELF_INSTALLEREN)
    developed_by = models.CharField(max_length=20, choices=DevelopBy.choices, blank=True, null=True)
    expected_quarter = models.CharField(max_length=2, choices=ExpectedQuarter.choices, blank=True, null=True)
    expected_year = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)

    chart = models.ForeignKey('Chart', on_delete=models.PROTECT, blank=True, null=True)
    owner = models.ForeignKey('User', on_delete=models.PROTECT, blank=True, null=True)
    tags = models.ManyToManyField('Tag', blank=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    @property
    def is_gewenst(self):
        return self.status == self.Statuses.GEWENST


class Product(models.Model):
    name = models.CharField(max_length=255, db_index=True)
    short_description = models.CharField(max_length=255, blank=True, default='')
    description = models.TextField(blank=True, default='')
    detail_page_image_url = models.URLField(max_length=MAX_URL_LENGTH, blank=True, default='')
    documentation_url = models.URLField(max_length=MAX_URL_LENGTH, blank=True, default='')
    demo_url = models.CharField(max_length=MAX_URL_LENGTH, blank=True, default='')
    bpmn_process_url = models.URLField(max_length=MAX_URL_LENGTH, blank=True, default='')
    is_published = models.BooleanField(default=False, db_index=True)
    owner = models.ForeignKey('User', on_delete=models.PROTECT, blank=True, null=True)
    components = models.ManyToManyField('Component', blank=True)
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
