from django.test import TestCase
from django.contrib.auth.models import AnonymousUser
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate

from .views import ProductViewSet
from .models import User, Product


class ProductViewSetTests(TestCase):
    def setUp(self):
        self.product_owner = User.objects.create(full_name='Product owner', email='product-owner@appstore.test', is_product_owner=True)
        self.normal_user = User.objects.create(full_name='Normal user', email='normal-user@appstore.test')

        self.unpublished_product = Product.objects.create(id=42, name='Unpublished Product', owner=self.product_owner, is_published=False)
        self.published_product = Product.objects.create(id=43, name='Published Product', owner=self.product_owner, is_published=True)

        self.factory = APIRequestFactory()


    def test_product_list_as_anonymous_user(self):
        view = ProductViewSet.as_view({'get': 'list'})

        request = self.factory.get('/products/')
        response = view(request)

        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['name'], 'Published Product')

    def test_product_list_as_product_owner(self):
        view = ProductViewSet.as_view({'get': 'list'})

        request = self.factory.get('/products/')
        force_authenticate(request, user=self.product_owner)

        response = view(request)

        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data[0]['name'], 'Published Product')
        self.assertEqual(response.data[1]['name'], 'Unpublished Product')

    def test_product_detail_as_anonymous_user(self):
        view = ProductViewSet.as_view({'get': 'retrieve'})

        request = self.factory.get('/products/42/')
        response = view(request, pk=42)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        request = self.factory.get('/products/43/')
        response = view(request, pk=43)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_product_detail_as_product_owner(self):
        view = ProductViewSet.as_view({'get': 'retrieve'})

        request = self.factory.get('/products/42/')
        force_authenticate(request, user=self.product_owner)
        response = view(request, pk=42)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        request = self.factory.get('/products/43/')
        force_authenticate(request, user=self.product_owner)
        response = view(request, pk=43)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
