# Generated by Django 3.0.2 on 2020-02-05 15:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20200210_1225'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='component',
            name='yaml_config_url',
        ),
        migrations.AddField(
            model_name='component',
            name='developed_by',
            field=models.CharField(blank=True, choices=[('self', 'Self'), ('thirdparty', 'Thirdparty'), ('unknown', 'Unknown')], max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='component',
            name='expected_quarter',
            field=models.CharField(blank=True, choices=[('Q1', 'Q1'), ('Q2', 'Q2'), ('Q3', 'Q3'), ('Q4', 'Q4')], max_length=2, null=True),
        ),
        migrations.AddField(
            model_name='component',
            name='expected_year',
            field=models.CharField(blank=True, max_length=4, null=True),
        ),
        migrations.AddField(
            model_name='component',
            name='reuse_type',
            field=models.CharField(choices=[('zelf installeren', 'Zelf installeren'), ('landelijke voorziening', 'Landelijke voorziening')], default='zelf installeren', max_length=25),
        ),
        migrations.AlterField(
            model_name='component',
            name='organisation_name',
            field=models.CharField(blank=True, default='', max_length=255, null=True),
        ),
    ]
