# Generated by Django 3.0.2 on 2020-02-10 12:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20200210_1211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='is_published',
            field=models.BooleanField(db_index=True, default=False),
        ),
    ]
