# Generated by Django 3.0.2 on 2020-01-28 12:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chart',
            name='url',
            field=models.URLField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='chart',
            name='values',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='component',
            name='contact',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='component',
            name='description',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='component',
            name='docker_image_uri',
            field=models.CharField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='component',
            name='located_at',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='component',
            name='organisation_name',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='component',
            name='repository_url',
            field=models.URLField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='component',
            name='technology_stack',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='component',
            name='yaml_config_url',
            field=models.URLField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='product',
            name='bpmn_process_url',
            field=models.URLField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='product',
            name='contact_url',
            field=models.URLField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='product',
            name='customerjourney_url',
            field=models.URLField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='product',
            name='demo_url',
            field=models.CharField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.TextField(blank=True, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='product',
            name='detail_page_image_url',
            field=models.URLField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='product',
            name='documentation_url',
            field=models.URLField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='product',
            name='maintainer',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='product',
            name='maintainer_url',
            field=models.URLField(blank=True, default='', max_length=2000),
        ),
        migrations.AlterField(
            model_name='product',
            name='short_description',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(max_length=254, unique=True, verbose_name='email address'),
        ),
        migrations.AlterField(
            model_name='user',
            name='full_name',
            field=models.CharField(db_index=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='user',
            name='picture_url',
            field=models.CharField(blank=True, default='', max_length=2000),
        ),
    ]
