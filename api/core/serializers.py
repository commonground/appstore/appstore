from rest_framework import serializers
from rest_framework.fields import Field

from .models import Product, Component, User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'full_name', 'email', 'picture_url', 'is_admin', 'is_product_owner']


class OwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'full_name', 'email', 'picture_url']


class ComponentSerializer(serializers.ModelSerializer):
    owner = OwnerSerializer(read_only=True)
    
    class Meta:
        model = Component
        fields = [
            'id',
            'name',
            'layer_type',
            'description',
            'organisation_name',
            'contact',
            'repository_url',
            'reuse_type',
            'expected_quarter',
            'expected_year',
            'developed_by',
            'status',
            'owner',
            'created_at'
        ]

        serializers.DateField(format="%Y-%m-%d %H:%M:%S")

    def is_valid(self, raise_exception=False):
        required_fields_mapping = {
            Component.Statuses.GEWENST: ("developed_by",),
            Component.Statuses.GEPLAND: ("expected_year", "organisation_name"),
            Component.Statuses.BETA: ("repository_url", "organisation_name",),
            Component.Statuses.BRUIKBAAR: ("repository_url", "organisation_name",),
            Component.Statuses.UITGEFASEERD: ("repository_url", "organisation_name",),
        }

        try:
            status = Component.Statuses(self.initial_data["status"])
            fields = required_fields_mapping[status]
            self._set_required_on_fields(fields)
        except ValueError:
            # Ignore an invalid value for status. It will be handled  by the parent is_valid().
            pass

        return super().is_valid(raise_exception)

    def _set_required_on_fields(self, field_names):
        for name in field_names:
            field = self.fields[name]  # type: Field
            field.required = True


class ProductComponentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True)

    class Meta:
        model = Component
        fields = ['id']


class ProductSerializer(serializers.ModelSerializer):
    owner = OwnerSerializer(read_only=True)
    components = ComponentSerializer(many=True, required=False, read_only=True)

    class Meta:
        model = Product
        fields = [
            'id',
            'name',
            'short_description',
            'description',
            'detail_page_image_url',
            'documentation_url',
            'demo_url',
            'bpmn_process_url',
            'is_published',
            'owner',
            'components'
        ]
