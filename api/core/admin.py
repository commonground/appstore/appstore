from django.contrib import admin
from .models import User, Product, Component, Tag, Chart
from django.utils.translation import gettext_lazy as _


class UserAdmin(admin.ModelAdmin):
    ordering = ['full_name']
    list_display = ['full_name', 'email', 'is_admin', 'is_product_owner', 'last_login']
    search_fields = ['full_name', 'email']
    readonly_fields = ['full_name', 'email', 'last_login']
    fieldsets = (
        (None, {'fields': ['full_name', 'email', 'last_login']}),
        (_('Permissions'), {'fields': ['is_active', 'is_admin', 'is_product_owner']})
    )


admin.site.register(User, UserAdmin)
admin.site.register(Product)
admin.site.register(Component)
admin.site.register(Tag)
admin.site.register(Chart)
