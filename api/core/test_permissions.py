from django.test import TestCase
from django.contrib.auth.models import AnonymousUser
from django.test.client import RequestFactory
from rest_framework import permissions

from .permissions import ProductOwnerOrReadOnly
from .models import User, Product


class ProductOwnerOrReadOnlyTests(TestCase):
    def setUp(self):
        self.product_owner = User.objects.create(full_name='Product owner', email='product-owner@appstore.test', is_product_owner=True)
        self.normal_user = User.objects.create(full_name='Normal user', email='normal-user@appstore.test')

        self.product_of_product_owner = Product.objects.create(name='Product 42', owner=self.product_owner)
        self.product_not_of_product_owner = Product.objects.create(name='Product 42a', owner=None)

        self.permission_class = ProductOwnerOrReadOnly()

        self.factory = RequestFactory()

    def test_has_read_permissions(self):
        request = self.factory.get('/')
        request.user = AnonymousUser()

        result = self.permission_class.has_permission(request, None)
        self.assertEqual(result, True)

    def test_has_create_permissions(self):
        request = self.factory.post('/')

        request.user = self.product_owner
        result = self.permission_class.has_permission(request, None)
        self.assertEqual(result, True)

        request.user = self.normal_user
        result = self.permission_class.has_permission(request, None)
        self.assertEqual(result, False)

    def test_has_object_permissions(self):
        request = self.factory.put('/')
        request.user = AnonymousUser()

        result = self.permission_class.has_object_permission(request, None, self.product_of_product_owner)
        self.assertEqual(result, False)

        request.user = self.product_owner

        result = self.permission_class.has_object_permission(request, None, self.product_of_product_owner)
        self.assertEqual(result, True)

        result = self.permission_class.has_object_permission(request, None, self.product_not_of_product_owner)
        self.assertEqual(result, False)
