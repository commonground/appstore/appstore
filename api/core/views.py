from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend
from django.db.models import Q
from rest_framework import viewsets, filters, status
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from .models import Product, Component
from .permissions import ProductOwnerOrReadOnly
from .serializers import ProductSerializer, ComponentSerializer, UserSerializer, ProductComponentSerializer


class UserViewSet(viewsets.ViewSet):
    def retrieve(self, request, pk=None):
        if pk != 'me':
            raise NotFound(detail='Could not find this user', code=404)

        if request.user.is_authenticated:
            serializer = UserSerializer(request.user)
            return Response(serializer.data)

        return Response()


class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = [ProductOwnerOrReadOnly]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [filters.SearchFilter]

    search_fields = ['name']
    ordering_fields = ['created_at']
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]

    def get_queryset(self):
        query = Q(is_published=True)
        if self.request.user.is_authenticated:
            query.add(Q(owner=self.request.user), Q.OR)

        return Product.objects.filter(query)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    #pylint: disable=unused-argument
    @action(detail=True, methods=['post'], name='Add Components')
    def add_component(self, request, pk=None):
        product = self.get_object()
        serializer = ProductComponentSerializer(many=True, data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        component_ids = [cdata['id'] for cdata in serializer.validated_data]
        for component in Component.objects.filter(id__in=component_ids):
            product.components.add(component)

        product.save()
        return Response(ProductSerializer(product).data)


class ComponentViewSet(viewsets.ModelViewSet):
    permission_classes = [ProductOwnerOrReadOnly]
    queryset = Component.objects.all()
    serializer_class = ComponentSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]

    search_fields = ['name']
    filterset_fields = ['status']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
