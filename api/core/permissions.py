from rest_framework import permissions


class ProductOwnerOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        # everyone can do read actions
        if request.method in permissions.SAFE_METHODS:
            return True

        # for other actions it is required to be logged in
        # and to be a product owner
        if request.user.is_authenticated and request.user.is_product_owner:
            return True

        return False

    def has_object_permission(self, request, view, obj):
        # everyone can do read actions
        if request.method in permissions.SAFE_METHODS:
            return True

        # when an action is performed on an object, you need to be owner
        return obj.owner == request.user
