// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import { Selector } from 'testcafe'

const getBaseUrl = require('../getBaseUrl')
const baseUrl = getBaseUrl();

fixture `Homepage`
  .page `${baseUrl}`

test('Page title is present', async t => {
  // the title contains a soft-hyphen (&shy;) character between Componenten and catalogus
  // https://en.wikipedia.org/wiki/Soft_hyphen
  await t
    .expect(Selector('h1').innerText).eql('Common Ground Componenten­catalogus');
});
