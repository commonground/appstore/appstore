// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { renderWithProviders, act } from './test-utils'

import App from './App'

describe('App', () => {
  beforeEach(() => {
    // the App component includes the UserContext, which will attempt to fetch the authenticated
    // user. We mock that fetch so we don't get errors from a failing XHR-request in our output.
    jest.spyOn(global, 'fetch').mockImplementation(() =>
      Promise.resolve({
        ok: true,
        status: 200,
        json: () =>
          Promise.resolve({
            id: 'id',
            externalId: 'external id',
            fullName: 'full name',
            email: 'email',
            pictureUrl: 'picture url',
            roles: [],
          }),
      }),
    )
  })

  it('exists', async () => {
    let container

    await act(async () => {
      const renderResult = renderWithProviders(<App />)
      container = renderResult.container
    })

    expect(container).toBeTruthy()
  })
})
