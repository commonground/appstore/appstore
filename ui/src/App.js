// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'
import { GlobalStyles, DomainNavigation } from '@commonground/design-system'
import { UserContextProvider } from './user-context'
import ComponentencatalogusGlobalStyles from './components/GlobalStyles'
import theme from './theme'
import AboutPage from './pages/AboutPage'
import ContactPage from './pages/ContactPage'
import FiveLayersModelPage from './pages/FiveLayersModelPage'
import HomePage from './pages/HomePage'
import ProductPage from './pages/ProductPage'
import ProductsPage from './pages/ProductsPage'
import AddProductPage from './pages/AddProductPage'
import ComponentsPage from './pages/ComponentsPage'
import PortfolioPage from './pages/PortfolioPage'
import PortfolioDetailPage from './pages/PortfolioDetailPage'
import SolutionPage from './pages/SolutionPage'
import EditProductPage from './pages/EditProductPage'
import PageTemplate from './components/design-system-candidates/PageTemplate'
import PrivacyPage from './pages/PrivacyPage'

const App = () => (
  <UserContextProvider>
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <ComponentencatalogusGlobalStyles />
      <DomainNavigation
        activeDomain="Componenten­catalogus"
        gitLabLink="https://gitlab.com/commonground/appstore/appstore"
      />
      <Router>
        <PageTemplate>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/over" component={AboutPage} />
            <Route exact path="/portfolio" component={PortfolioPage} />
            <Route path="/portfolio/oplossing/:id" component={SolutionPage} />
            <Route path="/portfolio/:id" component={PortfolioDetailPage} />
            <Route path="/contact" component={ContactPage} />
            <Route path="/5-lagen-model" component={FiveLayersModelPage} />
            <Route exact path="/producten" component={ProductsPage} />
            <Route path="/producten/nieuw" component={AddProductPage} />
            <Route path="/producten/:id/bewerk" component={EditProductPage} />
            <Route path="/producten/:id/" component={ProductPage} />
            <Route path="/componenten" component={ComponentsPage} />
            <Route path="/privacyverklaring" component={PrivacyPage} />
          </Switch>
        </PageTemplate>
      </Router>
    </ThemeProvider>
  </UserContextProvider>
)

export default App
