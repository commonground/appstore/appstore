// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { act, render, waitFor } from '@testing-library/react'
import UserContext, { UserContextProvider } from './index'

describe('UserContext', () => {
  describe('Provider', () => {
    describe('on initialisation', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve({ id: '42' }),
          }),
        )
      })

      afterEach(() => global.fetch.mockRestore())

      it('should fetch the current user', async () => {
        render(<UserContextProvider />)

        await act(async () => {
          expect(global.fetch).toHaveBeenCalledWith('/api/users/me/')
        })
      })

      it('should make the user available to the context consumers', async () => {
        const { getByText, findByTestId } = render(
          <UserContextProvider>
            <UserContext.Consumer>
              {({ user }) => (
                <div data-testid="child">{user ? user.id : ''}</div>
              )}
            </UserContext.Consumer>
          </UserContextProvider>,
        )

        await waitFor(async () => findByTestId('child'))
        expect(getByText('42')).toBeTruthy()
      })
    })

    describe('when passing a default user', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch')
      })

      afterEach(() => global.fetch.mockRestore())

      it('should not fetch the current user', async () => {
        render(<UserContextProvider user={{ id: '42' }} />)

        await act(async () => {
          expect(global.fetch).not.toHaveBeenCalled()
        })
      })
    })
  })
})
