// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

const { createProxyMiddleware } = require('http-proxy-middleware')

const backendSettings = {
  target: process.env.PROXY || 'http://localhost:8000',
  headers: { 'X-Forwarded-Port': process.env.PORT || '3000' },
}

module.exports = function (app) {
  app.use('/api', createProxyMiddleware(backendSettings))
  app.use('/oidc', createProxyMiddleware(backendSettings))
  app.use('/admin', createProxyMiddleware(backendSettings))
}
