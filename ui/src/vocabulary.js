// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import theme from './theme'

export const LAYER_TYPE_INTERACTIE = 'interactie'
export const LAYER_TYPE_PROCES = 'proces'
export const LAYER_TYPE_INTEGRATIE = 'integratie'
export const LAYER_TYPE_SERVICES = 'services'
export const LAYER_TYPE_DATA = 'data'
export const LAYER_TYPES = [
  LAYER_TYPE_INTERACTIE,
  LAYER_TYPE_PROCES,
  LAYER_TYPE_INTEGRATIE,
  LAYER_TYPE_SERVICES,
  LAYER_TYPE_DATA,
]
export const STATUS_GEWENST = 'gewenst'
export const STATUS_GEPLAND = 'gepland'
export const STATUS_BETA = 'beta'
export const STATUS_BRUIKBAAR = 'bruikbaar'
export const STATUS_UITGEFASEERD = 'uitgefaseerd'
export const STATUSES = [
  STATUS_GEWENST,
  STATUS_GEPLAND,
  STATUS_BETA,
  STATUS_BRUIKBAAR,
  STATUS_UITGEFASEERD,
]

export const DEVELOPED_BY_SELF = 'self'
export const DEVELOPED_BY_THIRDPARTY = 'thirdparty'
export const DEVELOPED_BY_UNKNOWN = 'unknown'
export const DEVELOPED_BY_TYPES = [
  DEVELOPED_BY_SELF,
  DEVELOPED_BY_THIRDPARTY,
  DEVELOPED_BY_UNKNOWN,
]

export const REUSE_TYPE_ZELF_INSTALLEREN = 'zelf installeren'
export const REUSE_TYPE_LANDELIJKE_VOORZIENING = 'landelijke voorziening'
export const REUSE_TYPES = [
  REUSE_TYPE_ZELF_INSTALLEREN,
  REUSE_TYPE_LANDELIJKE_VOORZIENING,
]

export const componentStatus = {
  [STATUS_GEWENST]: {
    color: theme.tokens.colors.colorPaletteBlue400,
    textColor: theme.tokens.colors.colorPaletteGray900,
  },
  [STATUS_GEPLAND]: {
    color: theme.tokens.colors.colorPaletteBlue900,
    textColor: theme.tokens.colors.colorTextWhite,
  },
  [STATUS_BETA]: {
    color: theme.tokens.colors.brandPrimary,
    textColor: theme.tokens.colors.colorPaletteGray900,
  },
  [STATUS_BRUIKBAAR]: null,
  [STATUS_UITGEFASEERD]: {
    color: theme.tokens.colors.colorAlertError,
    textColor: theme.tokens.colors.colorTextWhite,
  },
}
