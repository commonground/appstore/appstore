// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'

import { Row, Col } from '../../components/design-system-candidates/Grid'
import ProductRepository from '../../domain/product-repository'
import ProductList from '../../components/ProductList'
import theme from '../../theme'
import UserContext from '../../user-context'
import { userIsProductOwner } from '../../permissions'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import { AddButton, StyledIconAdd, ProductListWrapper } from './index.styles'

const ProductsPage = () => {
  const [products, setProducts] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      try {
        const products = await ProductRepository.getAll()
        setProducts(products)
      } catch (e) {
        console.error(e)
      }
    }

    fetchData()
  }, [])

  return (
    <ThemeProvider theme={theme}>
      <PageTemplate.ContentContainer>
        <PageTemplate.LayoutCenteredSingleColumn>
          <Row>
            <Col width={[1, 1, 3 / 4]}>
              <h1>Software producten</h1>
              <p>
                Deeloplossingen op basis van een set componenten. Het gaat om
                werkende software die een oplossing biedt voor een bepaald
                onderdeel.
              </p>
            </Col>
            <Col width={[1, 1, 1 / 4]}>
              <UserContext.Consumer>
                {
                  (userContext) =>
                    userContext && userIsProductOwner(userContext.user) ? (
                      <AddButton
                        as={Link}
                        variant="secondary"
                        to="/producten/nieuw"
                        data-testid="add-product-button"
                      >
                        <StyledIconAdd />
                        Product toevoegen
                      </AddButton>
                    ) : null
                  /* eslint-disable-next-line react/jsx-curly-newline */
                }
              </UserContext.Consumer>
            </Col>
          </Row>

          <ProductListWrapper>
            <ProductList products={products} />
          </ProductListWrapper>
        </PageTemplate.LayoutCenteredSingleColumn>
      </PageTemplate.ContentContainer>
    </ThemeProvider>
  )
}

export default ProductsPage
