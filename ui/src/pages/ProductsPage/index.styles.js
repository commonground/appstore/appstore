// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Button, mediaQueries } from '@commonground/design-system'

import { IconAdd } from '../../icons'

export const AddButton = styled(Button)`
  ${mediaQueries.mdUp`
    float: right;
  `}
`

export const StyledIconAdd = styled(IconAdd)`
  margin-right: ${(p) => p.theme.tokens.spacing03};
`

export const ProductListWrapper = styled.section`
  margin-top: ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.smUp`
    margin-top: ${(p) => p.theme.tokens.spacing10};
  `}
`
