// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'

import UserContext from '../../user-context'
import ProductsPage from './index'

test('ProductsPage', () => {
  const { container } = render(
    <Router>
      <ProductsPage />
    </Router>,
  )
  expect(container).toBeTruthy()
})

test('the Add component button', () => {
  const userContext = {
    user: {
      id: '0000-0000-0000-0001',
      isProductOwner: true,
    },
  }

  const { getByTestId, queryByTestId, rerender } = render(
    <Router>
      <UserContext.Provider value={userContext}>
        <ProductsPage />
      </UserContext.Provider>
    </Router>,
  )

  const addProductButton = getByTestId('add-product-button')
  expect(addProductButton).toBeTruthy()
  expect(addProductButton.getAttribute('href')).toEqual('/producten/nieuw')

  rerender(
    <Router>
      <UserContext.Provider value={null}>
        <ProductsPage />
      </UserContext.Provider>
    </Router>,
  )

  expect(queryByTestId('add-product-button')).toBeNull()
})
