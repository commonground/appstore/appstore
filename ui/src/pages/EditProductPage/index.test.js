// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render, fireEvent, waitFor } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'

import theme from '../../theme'
import EditProductPage from './index'

// eslint-disable-next-line react/prop-types
jest.mock('../../components/ProductForm', () => ({ onSubmitHandler }) => (
  <form
    onSubmit={() => onSubmitHandler({ foo: 'bar' })}
    data-testid="product-form"
  >
    <label htmlFor="name">Naam</label>
    <input type="text" id="name" />
    <button type="submit" />
  </form>
))

describe('the EditProductPage', () => {
  it('on initialization', async () => {
    const historyPushSpy = jest.fn()
    const updateHandlerSpy = jest.fn().mockResolvedValue()
    const fetchProductByIdSpy = jest.fn().mockResolvedValue({
      id: 32,
      name: 'My First Product',
    })

    const { findByTestId, queryByTestId, findByLabelText } = render(
      <Router>
        <ThemeProvider theme={theme}>
          <EditProductPage
            match={{ params: { id: 42 } }}
            fetchProductById={fetchProductByIdSpy}
            updateHandler={updateHandlerSpy}
            history={{ push: historyPushSpy }}
          />
        </ThemeProvider>
      </Router>,
    )

    expect(await findByTestId('product-form')).toBeTruthy()
    expect(queryByTestId('success-message')).toBeNull()
    expect(fetchProductByIdSpy).toHaveBeenCalledWith(42)

    const productForm = await findByTestId('product-form')
    expect(productForm).toBeTruthy()

    const nameInput = await findByLabelText('Naam')
    expect(nameInput).toBeTruthy()
  })

  it('successfully submitting the form', async () => {
    const historyPushSpy = jest.fn()
    const dummyProduct = {
      id: 42,
      name: 'My First Product',
    }

    const { findByTestId, getByTestId } = render(
      <ThemeProvider theme={theme}>
        <Router>
          <EditProductPage
            match={{ params: { id: dummyProduct.id } }}
            fetchProductById={() => Promise.resolve(dummyProduct)}
            updateHandler={() => Promise.resolve(dummyProduct)}
            history={{ push: historyPushSpy }}
          />
        </Router>
      </ThemeProvider>,
    )

    await waitFor(async () => findByTestId('product-form'))

    const form = getByTestId('product-form')
    await fireEvent.submit(form)

    expect(historyPushSpy).toHaveBeenCalledWith('/producten/42')
  })

  it('re-submitting the form when the previous submission went wrong', async () => {
    const historyPushSpy = jest.fn()
    const dummyProduct = {
      id: 42,
      name: 'My First Product',
    }
    const updateHandler = jest
      .fn(() => Promise.resolve(dummyProduct))
      .mockImplementationOnce(() => Promise.reject(new Error('error')))

    const { findByTestId } = render(
      <ThemeProvider theme={theme}>
        <Router>
          <EditProductPage
            match={{ params: { id: dummyProduct.id } }}
            fetchProductById={() => Promise.resolve(dummyProduct)}
            updateHandler={updateHandler}
            history={{ push: historyPushSpy }}
          />
        </Router>
      </ThemeProvider>,
    )

    const form = await findByTestId('product-form')

    await fireEvent.submit(form)
    await fireEvent.submit(form)

    expect(historyPushSpy).toHaveBeenCalledWith('/producten/42')
  })

  it('submitting the form with invalid data which has not been caught by client-side validation', async () => {
    const dummyProduct = {
      id: 42,
      name: 'My First Product',
    }
    const { findByTestId } = render(
      <ThemeProvider theme={theme}>
        <Router>
          <EditProductPage
            match={{ params: { id: dummyProduct.id } }}
            fetchProductById={() => Promise.resolve(dummyProduct)}
            updateHandler={() => {
              return Promise.reject(new Error('invalid user input'))
            }}
          />
        </Router>
      </ThemeProvider>,
    )

    const form = await findByTestId('product-form')
    await fireEvent.submit(form)

    const errorMessage = await findByTestId('error-message')
    expect(errorMessage).toBeTruthy()
    expect(errorMessage.textContent).toEqual(
      'Ongeldige invoerDe opgegeven waarden zijn ongeldig.',
    )
  })

  it('when the server is unable to handle the request', async () => {
    const dummyProduct = {
      id: 42,
      name: 'My First Product',
    }
    const { findByTestId } = render(
      <ThemeProvider theme={theme}>
        <Router>
          <EditProductPage
            match={{ params: { id: dummyProduct.id } }}
            fetchProductById={() => Promise.resolve(dummyProduct)}
            updateHandler={() => Promise.reject(new Error('arbitrary error'))}
          />
        </Router>
      </ThemeProvider>,
    )

    const form = await findByTestId('product-form')
    await fireEvent.submit(form)

    const errorMessage = await findByTestId('error-message')
    expect(errorMessage).toBeTruthy()
    expect(errorMessage.textContent).toEqual(
      'Oeps, er ging iets foutEr ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
    )
  })
})
