// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { Component } from 'react'
import { func, object } from 'prop-types'
import { Flex, Box } from 'reflexbox/styled-components'
import ProductForm from '../../components/ProductForm'
import ProductRepository from '../../domain/product-repository'
import Alert from '../../components/design-system-candidates/Alert'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'

class EditProductPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isSubmitted: false,
      errorMessage: null,
    }

    this.handleFormSubmit = this.handleFormSubmit.bind(this)
    this.loadProductDetails = this.loadProductDetails.bind(this)
  }

  componentDidMount() {
    const {
      match: {
        params: { id },
      },
    } = this.props
    this.loadProductDetails(id)
  }

  loadProductDetails(productId) {
    const that = this
    const { fetchProductById } = this.props
    return fetchProductById(productId).then((product) => {
      that.setState({
        product,
      })
    })
  }

  handleFormSubmit(values) {
    const { match, history, updateHandler } = this.props
    updateHandler(match.params.id, values)
      .then((product) => {
        history.push(`/producten/${product.id}`)
      })
      .catch((error) => {
        this.setState({
          isSubmitted: true,
          errorMessage: error.message,
        })
      })
  }

  render() {
    const { isSubmitted, errorMessage, product } = this.state

    return (
      <PageTemplate.ContentContainer>
        <PageTemplate.LayoutCenteredSingleColumn>
          <Flex>
            <Box width={[1, 2 / 3]}>
              <h1>Product aanpassen</h1>

              {isSubmitted ? (
                errorMessage === 'invalid user input' ? (
                  <Alert
                    type="error"
                    data-testid="error-message"
                    title="Ongeldige invoer"
                  >
                    De opgegeven waarden zijn ongeldig.
                  </Alert>
                ) : errorMessage === 'forbidden' ? (
                  <Alert
                    type="error"
                    data-testid="error-message"
                    title="Niet toegestaan"
                  >
                    Je hebt geen rechten om dit product aan te passen.
                  </Alert>
                ) : (
                  <Alert
                    type="error"
                    data-testid="error-message"
                    title="Oeps, er ging iets fout"
                  >
                    Er ging onverwachts iets fout bij ons. Probeer het later
                    opnieuw.
                  </Alert>
                )
              ) : null}
              {product ? (
                <ProductForm
                  initialValues={product}
                  onSubmitHandler={this.handleFormSubmit}
                  submitButtonText="Opslaan"
                  data-testid="product-form"
                />
              ) : null}
            </Box>
          </Flex>
        </PageTemplate.LayoutCenteredSingleColumn>
      </PageTemplate.ContentContainer>
    )
  }
}

EditProductPage.propTypes = {
  updateHandler: func,
  match: object,
  fetchProductById: func,
  history: object,
}

EditProductPage.defaultProps = {
  updateHandler: ProductRepository.update,
  match: {
    params: {},
  },
  fetchProductById: ProductRepository.getById,
}

export default EditProductPage
