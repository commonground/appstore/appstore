// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { func, object, shape } from 'prop-types'
import { Drawer } from '@commonground/design-system'
import AddComponentForm from '../../components/AddComponentForm'
import ComponentRepository from '../../domain/component-repository'
import Alert from '../../components/design-system-candidates/Alert'
import errorToUserFriendlyMessage from './error-to-user-friendly-message'

class EditComponentPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isSubmitted: false,
      success: null,
      errorMessage: null,
    }

    this.handleFormSubmit = this.handleFormSubmit.bind(this)
    this.loadComponentDetails = this.loadComponentDetails.bind(this)
    this.close = this.close.bind(this)
  }

  componentDidMount() {
    const {
      match: {
        params: { id },
      },
    } = this.props
    this.loadComponentDetails(id)
  }

  loadComponentDetails(componentId) {
    const that = this
    const { fetchComponentById } = this.props
    return fetchComponentById(componentId)
      .then((component) => {
        that.setState({
          component,
          errorMessage: null,
        })
      })
      .catch((error) => {
        that.setState({
          component: null,
          errorMessage: errorToUserFriendlyMessage(error.message),
        })
      })
  }

  handleFormSubmit(values) {
    this.props
      .updateHandler(values)
      .then(() => {
        this.setState({
          isSubmitted: true,
          success: true,
          errorMessage: null,
        })
      })
      .catch((error) => {
        this.setState({
          isSubmitted: true,
          success: false,
          errorMessage: errorToUserFriendlyMessage(error.message),
        })
      })
  }

  close() {
    this.props.history.push('/componenten')
  }

  render() {
    const { isSubmitted, success, errorMessage, component } = this.state

    return (
      <Drawer closeHandler={this.close}>
        <h1>Component bewerken</h1>
        {errorMessage != null ? (
          <Alert
            type="error"
            data-testid="error-message"
            title={errorMessage.title}
          >
            {errorMessage.content}
          </Alert>
        ) : component ? (
          <>
            {isSubmitted && success ? (
              <Alert
                type="success"
                data-testid="success-message"
                title="Component aangepast"
              >
                Ga terug naar het{' '}
                <Link to="/componenten">componentenoverzicht</Link>.
              </Alert>
            ) : null}
            {component && (!isSubmitted || (isSubmitted && !success)) ? (
              <AddComponentForm
                initialValues={component}
                onSubmitHandler={this.handleFormSubmit}
                submitButtonText="Opslaan"
                data-testid="component-form"
              />
            ) : null}
          </>
        ) : null}
      </Drawer>
    )
  }
}

EditComponentPage.propTypes = {
  updateHandler: func,
  match: shape({ params: object.isRequired }).isRequired,
  history: shape({ push: func.isRequired }).isRequired,
  fetchComponentById: func,
}

EditComponentPage.defaultProps = {
  updateHandler: ComponentRepository.update,
  match: {
    params: {},
  },
  history: {
    push: () => {},
  },
  fetchComponentById: ComponentRepository.getById,
}

export default EditComponentPage
