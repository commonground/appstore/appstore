// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
const errorToUserFriendlyMessage = (error) => {
  const errorMessages = {
    'invalid user input': {
      title: 'Ongeldige invoer',
      content: 'De opgegeven waarden zijn ongeldig.',
    },
    unauthorized: {
      title: 'Niet toegestaan',
      content: 'Je bent niet ingelogd.',
    },
    forbidden: {
      title: 'Niet toegestaan',
      content: 'Je hebt niet de rechten om dit component aan te passen.',
    },
    unexpected: {
      title: 'Oeps, er ging iets fout',
      content:
        'Er ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
    },
  }

  /* eslint-disable security/detect-object-injection */
  if (errorMessages[error] !== undefined) {
    return errorMessages[error]
  }
  /* eslint-enable security/detect-object-injection */

  return errorMessages.unexpected
}

export default errorToUserFriendlyMessage
