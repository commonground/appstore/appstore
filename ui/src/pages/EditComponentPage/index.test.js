// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'

import UserContext from '../../user-context'
import { renderWithProviders, fireEvent, waitFor } from '../../test-utils'
import EditComponentPage from './index'

// eslint-disable-next-line react/prop-types
jest.mock('../../components/AddComponentForm', () => ({ onSubmitHandler }) => (
  <form
    onSubmit={() => onSubmitHandler({ foo: 'bar' })}
    data-testid="component-form"
  >
    <label htmlFor="name">Naam</label>
    <input type="text" id="name" />
    <button type="submit" />
  </form>
))

describe('the EditComponentPage', () => {
  it('before the component info has been fetched', async () => {
    let fetchComponentByIdPromiseResolve
    const fetchComponentByIdPromise = new Promise((resolve) => {
      fetchComponentByIdPromiseResolve = resolve
    })
    const fetchComponentByIdSpy = jest.fn(() => fetchComponentByIdPromise)
    const userContext = {
      user: {
        id: '0000-0000-0000-0001',
        isProductOwner: true,
      },
    }
    const { queryByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <EditComponentPage
            match={{ params: { id: '00000000-0000-0000-0000-000000000000' } }}
            fetchComponentById={fetchComponentByIdSpy}
            updateHandler={jest.fn()}
          />
        </UserContext.Provider>
      </Router>,
    )

    expect(queryByTestId('component-form')).toBeNull()
    expect(queryByTestId('error-message')).toBeNull()
    expect(queryByTestId('success-message')).toBeNull()

    fetchComponentByIdPromiseResolve({
      id: '00000000-0000-0000-0000-000000000000',
      name: 'My First Component',
      owner: {
        id: '0000-0000-0000-0001',
      },
    })

    await waitFor(() => queryByTestId('component-form'))

    expect(queryByTestId('component-form')).toBeTruthy()
    expect(queryByTestId('error-message')).toBeNull()
    expect(queryByTestId('success-message')).toBeNull()
  })

  it('on initialization', async () => {
    const updateHandlerSpy = jest.fn().mockResolvedValue()
    const fetchComponentByIdSpy = jest.fn().mockResolvedValue({
      id: '00000000-0000-0000-0000-000000000000',
      name: 'My First Component',
      owner: {
        id: '0000-0000-0000-0001',
      },
    })
    const userContext = {
      user: {
        id: '0000-0000-0000-0001',
        isProductOwner: true,
      },
    }
    const { findByTestId, queryByTestId, findByLabelText } =
      renderWithProviders(
        <Router>
          <UserContext.Provider value={userContext}>
            <EditComponentPage
              match={{ params: { id: '00000000-0000-0000-0000-000000000000' } }}
              fetchComponentById={fetchComponentByIdSpy}
              updateHandler={updateHandlerSpy}
            />
          </UserContext.Provider>
        </Router>,
      )

    expect(await findByTestId('component-form')).toBeTruthy()
    expect(queryByTestId('success-message')).toBeNull()
    expect(fetchComponentByIdSpy).toHaveBeenCalledWith(
      '00000000-0000-0000-0000-000000000000',
    )

    const componentForm = await findByTestId('component-form')
    expect(componentForm).toBeTruthy()

    const nameInput = await findByLabelText('Naam')
    expect(nameInput).toBeTruthy()
  })

  it('successfully submitting the form', async () => {
    const userContext = {
      user: {
        id: '0000-0000-0000-0001',
        isProductOwner: true,
      },
    }
    const dummyComponent = {
      id: '00000000-0000-0000-0000-000000000000',
      name: 'My First Component',
      owner: userContext.user,
    }

    const { findByTestId, queryByTestId, getByTestId, findByText } =
      renderWithProviders(
        <Router>
          <UserContext.Provider value={userContext}>
            <EditComponentPage
              match={{ params: { id: dummyComponent.id } }}
              fetchComponentById={() => Promise.resolve(dummyComponent)}
              updateHandler={() => Promise.resolve()}
            />
          </UserContext.Provider>
        </Router>,
      )

    await waitFor(async () => findByTestId('component-form'))

    const form = getByTestId('component-form')
    fireEvent.submit(form)

    const successMessage = await findByTestId('success-message')
    expect(successMessage).toBeTruthy()

    expect(queryByTestId('component-form')).toBeNull()

    const link = await findByText('componentenoverzicht')
    expect(link.getAttribute('href')).toBe('/componenten')
  })

  it('re-submitting the form when the previous submission went wrong', async () => {
    const userContext = {
      user: {
        id: '0000-0000-0000-0001',
        isProductOwner: true,
      },
    }

    const dummyComponent = {
      id: '00000000-0000-0000-0000-000000000000',
      name: 'My First Component',
      owner: userContext.user,
    }

    const updateHandler = jest
      .fn(() => Promise.resolve())
      .mockImplementationOnce(() => Promise.reject(new Error('error')))

    const { findByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <EditComponentPage
            match={{ params: { id: dummyComponent.id } }}
            fetchComponentById={() => Promise.resolve(dummyComponent)}
            updateHandler={updateHandler}
          />
        </UserContext.Provider>
      </Router>,
    )

    const form = await findByTestId('component-form')

    await fireEvent.submit(form)
    await fireEvent.submit(form)

    const successMessage = await findByTestId('success-message')
    expect(successMessage).toBeTruthy()
  })

  it('submitting the form with invalid data which has not been caught by client-side validation', async () => {
    const dummyComponent = {
      id: '00000000-0000-0000-0000-000000000000',
      name: 'My First Component',
      owner: {
        id: '0000-0000-0000-0001',
      },
    }
    const userContext = {
      user: {
        id: '0000-0000-0000-0001',
        isProductOwner: true,
      },
    }
    const { findByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <EditComponentPage
            match={{ params: { id: dummyComponent.id } }}
            fetchComponentById={() => Promise.resolve(dummyComponent)}
            updateHandler={() => {
              return Promise.reject(new Error('invalid user input'))
            }}
          />
        </UserContext.Provider>
      </Router>,
    )

    const form = await findByTestId('component-form')
    await fireEvent.submit(form)

    const errorMessage = await findByTestId('error-message')
    expect(errorMessage).toBeTruthy()
    expect(errorMessage.textContent).toEqual(
      'Ongeldige invoerDe opgegeven waarden zijn ongeldig.',
    )
  })

  it('when the server is unable to handle the request', async () => {
    const dummyComponent = {
      id: '00000000-0000-0000-0000-000000000000',
      name: 'My First Component',
      owner: {
        id: '0000-0000-0000-0001',
      },
    }
    const userContext = {
      user: {
        id: '0000-0000-0000-0001',
        isProductOwner: true,
      },
    }
    const { findByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <EditComponentPage
            match={{ params: { id: dummyComponent.id } }}
            fetchComponentById={() => Promise.resolve(dummyComponent)}
            updateHandler={() => Promise.reject(new Error('arbitrary error'))}
          />
        </UserContext.Provider>
      </Router>,
    )

    const form = await findByTestId('component-form')
    await fireEvent.submit(form)

    const errorMessage = await findByTestId('error-message')
    expect(errorMessage).toBeTruthy()
    expect(errorMessage.textContent).toEqual(
      'Oeps, er ging iets foutEr ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
    )
  })

  it('failed to fetch the component details', async () => {
    const userContext = {
      user: {
        id: '0000-0000-0000-0001',
        isProductOwner: true,
      },
    }
    const { findByTestId, getByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <EditComponentPage
            match={{ params: { id: '00000000-0000-0000-0000-000000000000' } }}
            fetchComponentById={() => {
              return Promise.reject(new Error('arbitrary error'))
            }}
            updateHandler={jest.fn()}
          />
        </UserContext.Provider>
      </Router>,
    )

    await waitFor(() => findByTestId('error-message'))

    expect(getByTestId('error-message')).toBeTruthy()
    expect(getByTestId('error-message').textContent).toBe(
      'Oeps, er ging iets foutEr ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
    )
  })

  it('submitting when the user is unauthenticated according to the server', async () => {
    const dummyComponent = {
      id: '00000000-0000-0000-0000-000000000000',
      name: 'My First Component',
      owner: {
        id: '0000-0000-0000-0001',
      },
    }
    const userContext = {
      user: {
        id: '0000-0000-0000-0001',
        isProductOwner: true,
      },
    }
    const { findByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <EditComponentPage
            match={{ params: { id: dummyComponent.id } }}
            fetchComponentById={() => Promise.resolve(dummyComponent)}
            updateHandler={() => Promise.reject(new Error('unauthorized'))}
          />
        </UserContext.Provider>
      </Router>,
    )

    const addComponentForm = await findByTestId('component-form')
    await fireEvent.submit(addComponentForm)

    const message = await findByTestId('error-message')
    expect(message).toBeTruthy()
    expect(message.textContent).toBe('Niet toegestaanJe bent niet ingelogd.')
  })

  it('submitting when the user is unauthorized according to the server', async () => {
    const dummyComponent = {
      id: '00000000-0000-0000-0000-000000000000',
      name: 'My First Component',
      owner: {
        id: '0000-0000-0000-0001',
      },
    }
    const userContext = {
      user: {
        id: '0000-0000-0000-0001',
        isProductOwner: true,
      },
    }
    const { findByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <EditComponentPage
            match={{ params: { id: dummyComponent.id } }}
            fetchComponentById={() => Promise.resolve(dummyComponent)}
            updateHandler={() => Promise.reject(new Error('forbidden'))}
          />
        </UserContext.Provider>
      </Router>,
    )

    const form = await findByTestId('component-form')
    await fireEvent.submit(form)

    const errorMessage = await findByTestId('error-message')
    expect(errorMessage).toBeTruthy()
    expect(errorMessage.textContent).toEqual(
      'Niet toegestaanJe hebt niet de rechten om dit component aan te passen.',
    )
  })
})
