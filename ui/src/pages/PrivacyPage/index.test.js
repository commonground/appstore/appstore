// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { ThemeProvider } from 'styled-components/macro'
import { render } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'

import theme from '../../theme'
import PrivacyPage from './index'

test('exists', () => {
  const { container } = render(
    <ThemeProvider theme={theme}>
      <Router>
        <PrivacyPage />
      </Router>
    </ThemeProvider>,
  )
  expect(container).toBeTruthy()
})
