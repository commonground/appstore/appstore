// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Subtitle = styled.p`
  font-size: ${(p) => p.theme.tokens.fontSizeXLarge};
  line-height: 125%;
  margin: 1.5rem 0 4rem;
`

export const CollapsibleWrapper = styled.div`
  margin-top: ${(p) => p.theme.tokens.spacing06};
  border-bottom: 1px solid
    ${(p) => p.theme.tokens.colors.colorCollapsibleBorder};

  > * {
    border-top: 1px solid ${(p) => p.theme.tokens.colors.colorCollapsibleBorder};
    padding: ${(p) =>
      `${p.theme.tokens.spacing05} ${p.theme.tokens.spacing03}`};
  }
`

export const CollapsibleTitle = styled.p`
  margin-bottom: 0;
`

export const CollapsibleBody = styled.p`
  margin: 0;
  padding-top: ${(p) => p.theme.tokens.spacing05};
`
