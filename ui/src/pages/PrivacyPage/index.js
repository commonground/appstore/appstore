// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Collapsible } from '@commonground/design-system'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import {
  Subtitle,
  CollapsibleBody,
  CollapsibleTitle,
  CollapsibleWrapper,
} from './index.styles'

const PrivacyPage = () => (
  <PageTemplate.ContentContainer>
    <PageTemplate.LayoutCenteredSingleColumn>
      <article>
        <h1>Privacyverklaring</h1>
        <Subtitle>
          Privacy & security wordt integraal meegenomen in het ontwerp van onze
          applicaties. Het is een van de kernprincipes van Common Ground.
        </Subtitle>
        <p>
          Deze privacyverklaring moet worden gelezen in aanvulling op en in
          samenhang met{' '}
          <a href="https://vng.nl/privacyverklaring-vereniging-van-nederlandse-gemeenten">
            de algemene privacyverklaring van de Vereniging van Nederlandse
            Gemeenten
          </a>
          .
        </p>
        <h2>1. Algemeen</h2>
        <p>
          VNG Realisatie BV verwerkt persoonsgegevens op een zorgvuldige en
          veilige manier in overeenstemming met de geldende wet- en regelgeving.
          In de algemene privacyverklaring zijn de uitgangspunten beschreven ten
          aanzien van de verwerking van persoonsgegevens door de VNG Realisatie
          BV en is informatie te vinden over hoe u uw rechten kunt uitoefenen.
          In deze meer specifieke verklaring geven we nadere informatie over de
          doelen en grondslagen, alsmede andere belangrijke informatie.
        </p>
        <p>
          Vanwege nieuwe wetgeving of andere ontwikkelingen, past VNG Realisatie
          BV regelmatig haar processen aan. Dit kan ook wijzigingen inhouden ten
          aanzien van het verwerken van persoonsgegevens. Het is daarom raadzaam
          om regelmatig de algemene en deze specifieke privacyverklaring te
          raadplegen. Aan het einde van de verklaring staat aangegeven wanneer
          deze voor het laatst is aangepast.
        </p>
        <h2>2. De verantwoordelijke</h2>
        <p>
          VNG Realisatie BV is de verwerkingsverantwoordelijke voor de volgende
          verwerkingen:
        </p>
        <h2>3. Verwerken van persoonsgegevens</h2>
        <p>
          Hieronder wordt meer uitleg gegeven over de verwerking, inclusief het
          doel, de grondslag en andere belangrijke informatie.
        </p>
        <h3>Doel Componentencatalogus</h3>
        <p>
          Het doel van het verwerken van persoonsgegevens voor de
          Componentencatalogus: Het verstrekken van contactgegevens bij
          producten in de Componentencatalogus. De Componentencatalogus biedt
          een overzicht (catalogus) van herbruikbare software binnen Nederlandse
          gemeenten. Hiermee wordt zichtbaar welke componenten herbruikbaar
          zijn, en wordt het makkelijker om software daadwerkelijk te
          hergebruiken.
        </p>

        <CollapsibleWrapper>
          <Collapsible
            title={<CollapsibleTitle>Grondslag</CollapsibleTitle>}
            aria-label="Grondslag"
          >
            <CollapsibleBody>
              VNG Realisatie BV verwerkt deze persoonsgegevens op basis van
              gerechtvaardigd belang.
            </CollapsibleBody>
          </Collapsible>
          <Collapsible
            title={
              <CollapsibleTitle>Categorieën Persoonsgegevens</CollapsibleTitle>
            }
            aria-label="Categorieën Persoonsgegevens"
          >
            <CollapsibleBody as="ul">
              <li>Naam product owner component</li>
              <li>E-mailadres</li>
              <li>Telefoonnummer</li>
            </CollapsibleBody>
          </Collapsible>
          <Collapsible
            title={
              <CollapsibleTitle>
                Geen ontvangers, geen doorgifte, geen profielen geen
                geautomatiseerde besluitvorming
              </CollapsibleTitle>
            }
            aria-label="Ontvangers van persoonsgegevens"
          >
            <CollapsibleBody>
              Bij de Componentencatalogus is geen sprake van ontvangers van
              persoonsgegevens. De persoonsgegevens van Componentencatalogus
              worden niet buiten de Europese Economische Ruimte (EER) verwerkt.
              Er worden geen profielen opgesteld. Er vindt geen geautomatiseerde
              besluitvorming plaats.
            </CollapsibleBody>
          </Collapsible>
          <Collapsible
            title={<CollapsibleTitle>Bewaartermijnen</CollapsibleTitle>}
            aria-label="Bewaartermijnen"
          >
            <CollapsibleBody>
              De gegevens worden bewaard zolang noodzakelijk voor het doel van
              de verwerking. Dit betekent dat de naam van de product owner wordt
              verwijderd en vernietigd zodra een component een nieuwe product
              owner krijgt.
            </CollapsibleBody>
          </Collapsible>
        </CollapsibleWrapper>

        <h2>4. Contact</h2>
        <p>
          Mocht u nog vragen hebben, die niet in dit document, noch in het
          document met algemene informatie, worden beantwoord, of als u om een
          andere reden contact wil opnemen met de gemeente in het kader van
          privacy of gegevensbescherming, neem dan contact op via{' '}
          <a href="mailto:realisatie@vng.nl">realisatie@vng.nl</a>.
        </p>
        <h2>5. Uw rechten uitoefenen en klachten</h2>
        <p>
          Als burger of belanghebbende kunt u uw rechten bij de VNG Realisatie
          BV uitoefenen. Voor meer informatie over het uitoefenen van uw
          rechten, kunt u contact met ons opnemen via{' '}
          <a href="mailto:realisatie@vng.nl">realisatie@vng.nl</a>. Wanneer u
          een klacht heeft over het handelen van de VNG Realisatie BV, dan heeft
          u het recht om klacht in te dienen bij de privacy toezichthouder, de
          Autoriteit Persoonsgegevens.
        </p>
        <h2>6. Wijzigingen</h2>
        <p>Deze privacyverklaring is opgesteld op: 18 november 2020.</p>
      </article>
    </PageTemplate.LayoutCenteredSingleColumn>
  </PageTemplate.ContentContainer>
)

export default PrivacyPage
