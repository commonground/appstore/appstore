// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'

import theme from '../../../theme'
import { IconUser } from '../../../icons'
import UniqueSellingPoint from './index'

test('Renders without crashing', () => {
  expect(() =>
    render(
      <ThemeProvider theme={theme}>
        <UniqueSellingPoint title="title" Icon={IconUser}>
          <p>Some content</p>
        </UniqueSellingPoint>
      </ThemeProvider>,
    ),
  ).not.toThrow()
})
