// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { string, elementType, node } from 'prop-types'

import { USP, StyledIcon, Title } from './index.style'

const UniqueSellingPoint = ({ title, Icon, children }) => (
  <USP>
    <StyledIcon as={Icon} />
    <Title>{title}</Title>
    {children}
  </USP>
)

UniqueSellingPoint.propTypes = {
  title: string.isRequired,
  Icon: elementType.isRequired,
  children: node.isRequired,
}

export default UniqueSellingPoint
