// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { mediaQueries } from '@commonground/design-system'

export const USP = styled.div`
  margin-bottom: ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.sm`
    max-width: 80%;
    margin: 0 auto ${(p) => p.theme.tokens.spacing07};
  `}

  ${mediaQueries.mdUp`
    margin-bottom: 0;
  `}
`

export const StyledIcon = styled.svg`
  display: block;
  width: 32px;
  height: 32px;
  margin: 0 auto;
  fill: ${(p) => p.theme.tokens.colors.colorPaletteBlue800};

  ${mediaQueries.mdUp`
    width: 56px;
    height: 56px;
  `}
`

export const Title = styled.h3`
  text-align: center;

  ${mediaQueries.sm`
    margin-top: ${(p) => p.theme.tokens.spacing05};
  `}
`
