// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { StaticRouter as Router } from 'react-router-dom'
import { render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components/macro'

import theme from '../../theme'
import HomePage from './index'

test('Renders without crashing', () => {
  expect(() =>
    render(
      <Router>
        <ThemeProvider theme={theme}>
          <HomePage />
        </ThemeProvider>
      </Router>,
    ),
  ).not.toThrow()
})
