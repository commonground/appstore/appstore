// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { mediaQueries, Icon } from '@commonground/design-system'

export const Card = styled.div`
  padding: ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.smUp`
    padding: ${(p) =>
      `${p.theme.tokens.spacing07} ${p.theme.tokens.spacing07} ${p.theme.tokens.spacing07} ${p.theme.tokens.spacing11}`};
  `}

  background-color: ${(p) => p.theme.tokens.colorBackground};

  & a {
    margin-top: 1rem;
  }
`

export const Title = styled.h2`
  margin-top: 0;

  ${mediaQueries.smUp`
    margin-left: -${(p) => p.theme.tokens.spacing09};
  `}
`

export const StyledIcon = styled(Icon)`
  margin-right: ${(p) => p.theme.tokens.spacing05};
  fill: ${(p) => p.theme.tokens.colorPaletteGray500};

  ${mediaQueries.xs`
    width: 24px;
    height: 24px;
    margin-right: ${(p) => p.theme.tokens.spacing04};
  `}
`
