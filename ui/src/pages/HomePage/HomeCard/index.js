// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { string, elementType, node } from 'prop-types'

import { Card, Title, StyledIcon } from './index.styles'

const HomeCard = ({ title, Icon, children }) => (
  <Card>
    <Title>
      <StyledIcon as={Icon} size="x-large" inline />
      {title}
    </Title>
    {children}
  </Card>
)

HomeCard.propTypes = {
  Icon: elementType.isRequired,
  title: string.isRequired,
  children: node.isRequired,
}

export default HomeCard
