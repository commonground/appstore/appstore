// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { mediaQueries, Button } from '@commonground/design-system'

import PageTemplate from '../../components/design-system-candidates/PageTemplate'

const backgroundHeight = '25rem'

export const HomeContainer = styled(PageTemplate.ContentContainer)`
  &:before {
    height: ${backgroundHeight};
  }

  ${mediaQueries.xs`
    background: linear-gradient(
      ${(p) => {
        const { colorBackground, colorPaletteGray300 } = p.theme.tokens
        return `180deg,
          ${colorBackground} 0%,
          ${colorPaletteGray300} ${backgroundHeight},
          ${colorBackground} ${backgroundHeight}
        `
      }}
    );
  `}
`

export const Intro = styled.section`
  margin-bottom: ${(p) => p.theme.tokens.spacing08};

  ${mediaQueries.mdUp`
    margin-bottom: ${(p) => p.theme.tokens.spacing12};
  `}

  & a {
    margin-top: 1rem;
  }
`

export const IntroTitle = styled.h1`
  hyphens: none;
  /* mediaQueries.xs is too large */
  @media (max-width: 320px) {
    hyphens: manual;
  }
`

export const IntroButton = styled(Button)`
  width: 100%;
  text-align: center;

  ${mediaQueries.smUp`
    margin-top: ${(p) => p.theme.tokens.spacing05};
    width: auto;
    text-align: left;
  `}
`

export const UniqueSellingPoints = styled.section`
  text-align: center;
  margin-top: ${(p) => p.theme.tokens.spacing08};

  ${mediaQueries.mdUp`
    margin-top: ${(p) => p.theme.tokens.spacing12};
  `}
`

export const USPTitle = styled.h2`
  text-align: center;
  margin-bottom: ${(p) => p.theme.tokens.spacing07};

  ${mediaQueries.mdUp`
    margin-bottom: ${(p) => p.theme.tokens.spacing10};
  `}
`
