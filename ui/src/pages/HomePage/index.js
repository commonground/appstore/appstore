// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from '@commonground/design-system'

import { Row, Col } from '../../components/design-system-candidates/Grid'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import {
  IconUser,
  IconCodeSlash,
  IconRecycle,
  IconEarth,
  IconFileList,
} from '../../icons'
import HomeCard from './HomeCard'
import UniqueSellingPoint from './UniqueSellingPoint'
import bg from './componentencatalogus-home-hero.png'
import {
  HomeContainer,
  Intro,
  IntroTitle,
  IntroButton,
  UniqueSellingPoints,
  USPTitle,
} from './index.styles'

const HomePage = () => {
  return (
    <HomeContainer heroImages={{ smUp: bg }}>
      <PageTemplate.LayoutCenteredSingleColumn>
        <Intro>
          <IntroTitle>Common Ground Componenten&shy;catalogus</IntroTitle>
          <p>
            Binnen Common Ground werken gemeenten en VNG-Realisatie samen aan
            gemeentelijke informatievoorziening en maatschappelijke opgaven. De
            componenten die onder de vlag van Common Ground zijn gebouwd, worden
            in deze catalogus beschikbaar gesteld voor alle gemeenten.
          </p>
          <IntroButton as={Link} to="/over" variant="secondary">
            Lees meer over de Componentencatalogus
          </IntroButton>
        </Intro>

        <Row as="section">
          <Col width={[1, 1, 1 / 2]}>
            <HomeCard title="Business overzicht" Icon={IconUser}>
              <p>Hier vindt u beschikbare totaaloplossingen voor gemeenten.</p>
              <Button as={Link} to="/portfolio">
                Bekijk portfolio
              </Button>
            </HomeCard>
          </Col>

          <Col width={[1, 1, 1 / 2]}>
            <HomeCard title="Technisch overzicht" Icon={IconCodeSlash}>
              <p>
                De software producten en technische componenten vindt u in dit
                overzicht.
              </p>
              <Button as={Link} to="/producten">
                Bekijk software producten
              </Button>
            </HomeCard>
          </Col>
        </Row>

        <UniqueSellingPoints>
          <USPTitle>Waarom de Componentencatalogus?</USPTitle>

          <Row>
            <Col width={[1, 1, 1 / 3]}>
              <UniqueSellingPoint
                title="Herbruikbaarheid stimuleren"
                Icon={IconRecycle}
              >
                De ontwikkelde oplossingen van Common Ground zijn beschikbaar
                voor alle gemeenten.
              </UniqueSellingPoint>
            </Col>

            <Col width={[1, 1, 1 / 3]}>
              <UniqueSellingPoint title="Publiek overzicht" Icon={IconEarth}>
                U vindt hier een overzicht van de Common Ground oplossingen
                inclusief de status.
              </UniqueSellingPoint>
            </Col>

            <Col width={[1, 1, 1 / 3]}>
              <UniqueSellingPoint
                title="Common Ground principes"
                Icon={IconFileList}
              >
                Alle oplossingen in deze componenten&shy;catalogus zijn
                ontwikkeld conform de principes van Common Ground.
              </UniqueSellingPoint>
            </Col>
          </Row>
        </UniqueSellingPoints>
      </PageTemplate.LayoutCenteredSingleColumn>
    </HomeContainer>
  )
}

export default HomePage
