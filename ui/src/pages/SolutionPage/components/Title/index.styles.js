// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
`

export const StyledH1 = styled.h1`
  margin: 0 ${(p) => p.theme.tokens.spacing05}
    ${(p) => p.theme.tokens.spacing02} 0;
`
