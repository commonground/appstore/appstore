// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'

import Status from '../../../../components/ComponentStatus/index'
import { TitleWrapper, StyledH1 } from './index.styles'

const Title = ({ title, status }) => {
  return (
    <TitleWrapper>
      <StyledH1>{title}</StyledH1>
      {status && <Status status={status} />}
    </TitleWrapper>
  )
}

Title.propTypes = {
  title: string.isRequired,
  status: string,
}

export default Title
