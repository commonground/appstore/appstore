// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { renderWithProviders } from '../../../../test-utils'

import { STATUS_BETA } from '../../../../vocabulary'
import Title from './index'

test('renders expected result', () => {
  const { getByText, queryByText, rerender } = renderWithProviders(
    <Title title="My title" />,
  )

  expect(getByText('My title')).toBeInTheDocument()
  expect(queryByText('Beta')).not.toBeInTheDocument()

  rerender(<Title title="My title" status={STATUS_BETA} />)

  expect(getByText('beta')).toBeInTheDocument()
})
