// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'
import { Icon } from '@commonground/design-system'

import { Row, Col } from '../../../../components/design-system-candidates/Grid'
import nl2br from '../../../../utils/nl2br'
import { IconPlay } from '../../../../icons'
import {
  Screenshot,
  StyledButton,
  StyledIconExternalLink,
  AsideItem,
  AsideItemTitle,
} from './index.styles'

const SolutionDescription = ({ solution }) => {
  const { description, screenshots, supplier, features, demoUrl } = solution

  return (
    <Row>
      <Col width={[1, 2 / 3]}>
        <p>{nl2br(description)}</p>

        {features && features.length ? (
          <>
            <h2>Wat kan het</h2>
            <ul>
              {features.map((feature, i) => (
                <li key={i}>{feature}</li>
              ))}
            </ul>
          </>
        ) : null}

        <h2>Screenshot{screenshots.length > 1 ? 's' : ''}</h2>
        {screenshots.map(({ src, alt }) => (
          <Screenshot src={src} key={src} alt={alt} />
        ))}
      </Col>

      <Col as="aside" width={[1, 1 / 3]}>
        <StyledButton
          as="a"
          variant="secondary"
          href={demoUrl || ''}
          disabled={!demoUrl}
        >
          <Icon as={IconPlay} inline /> Demo{' '}
          <StyledIconExternalLink disabled={!demoUrl} />
        </StyledButton>

        {supplier && (
          <AsideItem>
            <AsideItemTitle>Leverancier</AsideItemTitle>
            <p>{supplier}</p>
          </AsideItem>
        )}
      </Col>
    </Row>
  )
}

SolutionDescription.propTypes = {
  solution: object,
}

SolutionDescription.defaultProps = {}

export default SolutionDescription
