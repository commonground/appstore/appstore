// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Button } from '@commonground/design-system'

import { IconExternalLink } from '../../../../icons'

export const Screenshot = styled.img`
  display: block;
  width: 100%;
  border: 1px solid ${(p) => p.theme.tokens.colorPaletteGray400};
  margin-bottom: ${(p) => p.theme.tokens.spacing05};
`

export const StyledButton = styled(Button)`
  width: 100%;
`

export const StyledIconExternalLink = styled(IconExternalLink)`
  margin-left: ${(p) => p.theme.tokens.spacing05};
  margin-right: 0;

  ${(p) => p.disabled && `fill: currentcolor;`}
`

export const AsideItem = styled.div`
  margin-top: ${(p) => p.theme.tokens.spacing07};
`

export const AsideItemTitle = styled.small`
  display: block;
`

export const Contact = styled.div`
  display: flex;
  align-items: center;
`

export const ContactData = styled.div`
  flex: 1 1 auto;

  & * {
    display: block;
  }
`

export const Avatar = styled.img`
  width: 37px;
  height: 37px;
  margin-right: ${(p) => p.theme.tokens.spacing04};
`
