// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState, useEffect } from 'react'
import { oneOfType, func, object } from 'prop-types'
import { useParams, useRouteMatch, Switch, Route } from 'react-router-dom'
import { Icon } from '@commonground/design-system'

import ProductRepository from '../../domain/product-repository'
import { solutions, getPortfolioItemBySolutionId } from '../../domain/in-memory'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import BackButton from '../../components/design-system-candidates/BackButton'
import BreadCrumbs from '../../components/BreadCrumbs'
import TabbedRouteContent from '../../components/TabbedRouteContent'
import ProductComponents from '../../components/ProductComponents'
import Title from './components/Title'
import Description from './components/Description'

const SolutionPage = ({ productRepository }) => {
  const [components, setComponents] = useState([])
  const { id } = useParams()
  const match = useRouteMatch()
  const { url } = match

  const solution = solutions.find((s) => s.id === id)

  useEffect(() => {
    let isCancelled = false

    const getProduct = async () => {
      try {
        const products = await productRepository.getAll()
        if (isCancelled) return

        const match = products.find((p) => p.name === name)
        if (!match || !match.id) {
          throw new Error(`Product not found by name: ${name}`)
        }

        const product = await productRepository.getById(match.id)
        if (isCancelled) return

        setComponents(product.components)
      } catch (e) {
        if (isCancelled) return

        setComponents(null)
        console.error(e)
      }
    }

    if (solution && solution.name) getProduct(name)

    return () => {
      isCancelled = true
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  if (!solution) return null

  const { name, status, categoryPath } = solution
  const portfolio = getPortfolioItemBySolutionId(id)

  const tabs = [
    { to: `${url}`, title: 'Omschrijving', exact: true },
    {
      to: `${url}/componenten`,
      title: 'Componenten',
    },
  ]

  return (
    <PageTemplate.ContentContainer>
      <PageTemplate.LayoutCenteredSingleColumn>
        <BackButton to={`/portfolio/${portfolio.id}`} title="Portfolio">
          {portfolio.name}
        </BackButton>

        <Title title={name} status={status} />
        <BreadCrumbs>
          <Icon size="small" as={portfolio.categoryIcon} inline />
          <BreadCrumbs.Path path={categoryPath} />
        </BreadCrumbs>

        <TabbedRouteContent tabs={tabs}>
          <Switch>
            <Route path={`${url}`} exact>
              <Description solution={solution} />
            </Route>
            <Route path={`${url}/componenten`}>
              {components === null ? (
                <p>Het componentenoverzicht kon niet worden opgehaald.</p>
              ) : (
                <ProductComponents components={components} match={match} />
              )}
            </Route>
          </Switch>
        </TabbedRouteContent>
      </PageTemplate.LayoutCenteredSingleColumn>
    </PageTemplate.ContentContainer>
  )
}

SolutionPage.propTypes = {
  productRepository: oneOfType([func, object]).isRequired,
}

SolutionPage.defaultProps = {
  productRepository: ProductRepository,
}

export default SolutionPage
