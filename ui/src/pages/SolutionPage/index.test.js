// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { MemoryRouter as Router, Route } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'

import theme from '../../theme'
import SolutionPage from './index'

test('SolutionPage', async () => {
  const mockProductRepository = {
    getAll: jest.fn().mockResolvedValue([
      {
        id: 42,
        name: 'Huishoudboekje',
      },
    ]),

    getById: jest.fn().mockResolvedValue({
      components: [],
    }),
  }

  const { container } = render(
    <ThemeProvider theme={theme}>
      <Router
        initialEntries={['/portfolio/oplossing/huishoudboekje']}
        initialIndex={0}
      >
        <Route path="/portfolio/oplossing/:id">
          <SolutionPage productRepository={mockProductRepository} />
        </Route>
      </Router>
    </ThemeProvider>,
  )

  expect(container).toBeTruthy()
})
