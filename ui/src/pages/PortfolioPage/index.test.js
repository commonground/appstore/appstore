// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import theme from '../../theme'
import PortfolioPage from './index'

test('PortfolioPage', () => {
  const { container } = render(
    <Router>
      <ThemeProvider theme={theme}>
        <PortfolioPage />
      </ThemeProvider>
    </Router>,
  )
  expect(container).toBeTruthy()
})
