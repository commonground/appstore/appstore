// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Flex, Box } from 'reflexbox/styled-components'
import { mediaQueries } from '@commonground/design-system'

export const StyledContainer = styled(Flex)`
  justify-content: space-between;
  flex-wrap: wrap;
`

export const StyledContent = styled(Box)`
  ${mediaQueries.smUp`
    padding-right: ${(p) => p.theme.tokens.spacing10};
  `}
`

export const StyledPortfolioListWrapper = styled.section`
  margin-top: ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.smUp`
    margin-top: ${(p) => p.theme.tokens.spacing10};
  `}
`
