// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Box } from 'reflexbox/styled-components'

export const CardList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  list-style-type: none;
  margin-left: -${(p) => p.theme.tokens.spacing04};
  margin-right: -${(p) => p.theme.tokens.spacing04};
  padding: 0;
`

export const Card = styled(Box)`
  padding: ${(p) => p.theme.tokens.spacing04};
`
