// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { shape, string, arrayOf, object } from 'prop-types'
import PortfolioCard from '../PortfolioCard'
import { Card, CardList } from './index.styles'

const PortfolioList = ({ items, ...props }) => (
  <CardList data-test="portfolio-list" {...props}>
    {items.map((item) => (
      <Card as="li" width={[1, 1 / 2]} key={item.id}>
        <PortfolioCard
          id={item.id}
          name={item.name}
          categoryPath={item.categoryPath}
          categoryIcon={item.categoryIcon}
          shortDescription={item.shortDescription}
          imgSrc={item.imgThumbSrc}
        />
      </Card>
    ))}
  </CardList>
)

PortfolioList.propTypes = {
  items: arrayOf(
    shape({
      id: string,
      name: string,
      categoryPath: arrayOf(string),
      categoryIcon: object,
      imgThumbSrc: string,
      shortDescription: string,
    }),
  ),
}

PortfolioList.defaultProps = {
  items: [],
}

export default PortfolioList
