// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render, screen } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'

import theme from '../../../../theme'
import { IconBriefcaseLine } from '../../../../icons'

import PortfolioList from './index'

const dummyItems = [
  {
    id: 'budgetbeheer-1',
    name: 'Budgetbeheer',
    imgSrc: '',
    categoryPath: ['Werk en inkomen'],
    categoryIcon: IconBriefcaseLine,
    shortDescription: 'Omschrijving',
  },
  {
    id: 'budgetbeheer-2',
    name: 'Budgetbeheer',
    imgSrc: '',
    categoryPath: ['Werk en inkomen'],
    categoryIcon: IconBriefcaseLine,
    shortDescription: 'Omschrijving',
  },
]

test('PortfolioList', async () => {
  render(
    <ThemeProvider theme={theme}>
      <Router>
        <PortfolioList items={dummyItems} />
      </Router>
    </ThemeProvider>,
  )

  const cards = await screen.findAllByText('Budgetbeheer')
  expect(cards).toHaveLength(2)
})
