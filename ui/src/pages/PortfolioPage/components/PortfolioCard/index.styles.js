// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Link } from 'react-router-dom'
import Color from 'color'
import { mediaQueries } from '@commonground/design-system'

export const Card = styled(Link)`
  position: relative;

  display: flex;
  width: 100%;
  height: 100%;

  text-decoration: none;
  background-color: ${(p) => p.theme.tokens.colors.colorPaletteGray100};
  box-shadow: inset 0 -3px 0 ${(p) => Color(p.theme.tokens.colors.colorPaletteGray900).alpha(0.25).hsl().string()};

  &:hover {
    background: ${(p) => p.theme.tokens.colors.colorPaletteGray200};
  }
`

Card.Header = styled.div``

Card.Title = styled.h2`
  margin: 0;
  margin-bottom: ${(p) => p.theme.tokens.spacing02};

  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
  color: ${(p) => p.theme.tokens.colors.colorText};
`

Card.Subtitle = styled.h3`
  margin: 0;
  margin-bottom: ${(p) => p.theme.tokens.spacing04};

  color: ${(p) => p.theme.tokens.colorPaletteGray700};
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
`

Card.Icon = styled.img`
  margin: 5px;

  max-height: 16px;
`

Card.Body = styled.div`
  padding: ${(p) => p.theme.tokens.spacing05};

  color: ${(p) => p.theme.tokens.colors.colorText};

  ${mediaQueries.smUp`
    padding: ${(p) => p.theme.tokens.spacing06};
  `}

  p {
    margin-bottom: 0;
  }
`

Card.Thumbnail = styled.div`
  flex: 40px 0 0;

  background-image: linear-gradient(
      to bottom,
      ${(p) => Color(p.theme.tokens.colorBrand2).alpha(0.3).hsl().string()},
      ${(p) => Color(p.theme.tokens.colorBrand2).alpha(0.3).hsl().string()}
    ),
    url(${(p) => p.src});
  background-size: cover;
  box-shadow: inset 0 -3px 0 ${(p) => Color(p.theme.tokens.colors.colorPaletteGray900).alpha(0.25).hsl().string()};

  ${mediaQueries.smUp`
    flex: 80px 0 0;
  `}

  ${mediaQueries.mdUp`
    flex: 160px 0 0;
  `}
`
