// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render, screen } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'

import theme from '../../../../theme'
import { IconBriefcaseLine } from '../../../../icons'
import PortfolioCard from './index'

test('Portfolio card', () => {
  const item = {
    id: 'budgetbeheer',
    name: 'Budgetbeheer',
    imgSrc: '',
    categoryPath: ['Werk en inkomen'],
    categoryIcon: IconBriefcaseLine,
    shortDescription: 'Omschrijving',
  }

  render(
    <ThemeProvider theme={theme}>
      <Router>
        <PortfolioCard {...item} data-testid="item" />
      </Router>
    </ThemeProvider>,
  )

  const product = screen.getByRole('link')
  expect(product.getAttribute('href')).toEqual('/portfolio/budgetbeheer')

  const title = screen.getByText('Budgetbeheer')
  expect(title).toBeInTheDocument()

  const subtitle = screen.getByText(/Werk en inkomen/g)
  expect(subtitle).toBeInTheDocument()

  const shortDescription = screen.getByText('Omschrijving')
  expect(shortDescription).toBeInTheDocument()
})
