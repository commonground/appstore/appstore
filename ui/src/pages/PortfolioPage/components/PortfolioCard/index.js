// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { string, object, arrayOf } from 'prop-types'
import { Icon } from '@commonground/design-system'

import BreadCrumbs from '../../../../components/BreadCrumbs'
import { Card } from './index.styles'

const PortfolioCard = ({
  id,
  name,
  categoryPath,
  categoryIcon,
  shortDescription,
  imgSrc,
  ...props
}) => {
  return (
    <Card to={`/portfolio/${id}`} title={name} id={id} {...props}>
      <Card.Thumbnail src={imgSrc} />
      <Card.Body>
        <Card.Header>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>
            <BreadCrumbs>
              <Icon size="small" as={categoryIcon} inline />
              <BreadCrumbs.Path path={categoryPath.slice(0, 1)} />
            </BreadCrumbs>
          </Card.Subtitle>
        </Card.Header>
        <p>{shortDescription}</p>
      </Card.Body>
    </Card>
  )
}

PortfolioCard.propTypes = {
  id: string.isRequired,
  name: string.isRequired,
  categoryPath: arrayOf(string).isRequired,
  categoryIcon: object.isRequired,
  shortDescription: string.isRequired,
  imgSrc: string.isRequired,
}

export default PortfolioCard
