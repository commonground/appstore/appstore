// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import { portfolioItems } from '../../domain/in-memory'
import {
  StyledPortfolioListWrapper,
  StyledContainer,
  StyledContent,
} from './index.styles'
import PortfolioList from './components/PortfolioList'

const PortfolioPage = () => {
  return (
    <PageTemplate.ContentContainer>
      <PageTemplate.LayoutCenteredSingleColumn>
        <StyledContainer>
          <StyledContent>
            <h1>Portfolio</h1>
            <p>
              Voor onderstaande gemeentelijke producten en diensten zijn Common
              Ground oplossingen beschikbaar.
            </p>
          </StyledContent>
        </StyledContainer>
        <StyledPortfolioListWrapper>
          <PortfolioList items={portfolioItems} />
        </StyledPortfolioListWrapper>
      </PageTemplate.LayoutCenteredSingleColumn>
    </PageTemplate.ContentContainer>
  )
}

export default PortfolioPage
