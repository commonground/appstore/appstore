// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { useParams } from 'react-router-dom'
import { Icon } from '@commonground/design-system'
import { Box } from 'reflexbox'

import {
  portfolioItems,
  solutions as allSolutions,
} from '../../domain/in-memory'
import BackButton from '../../components/design-system-candidates/BackButton'
import BreadCrumbs from '../../components/BreadCrumbs'
import SolutionsList from './components/SolutionsList'
import {
  StyledContainer,
  StyledContentContainer,
  StyledH1,
  StyledLayoutCenteredSingleColumn,
} from './index.styles'

const PortfolioDetailPage = () => {
  const { id } = useParams()

  const portfolioItem = portfolioItems.find((item) => item.id === id)
  const solutions = allSolutions.filter(
    (solution) =>
      portfolioItem && portfolioItem.solutions.includes(solution.id),
  )

  return (
    <StyledContentContainer heroImages={{ xsUp: portfolioItem.imgSrc }}>
      <StyledLayoutCenteredSingleColumn>
        <StyledContainer>
          <BackButton to="/portfolio" title="Portfolio">
            Portfolio
          </BackButton>

          <Box>
            <StyledH1>{portfolioItem.name}</StyledH1>

            <BreadCrumbs>
              <Icon size="small" as={portfolioItem.categoryIcon} inline />
              <BreadCrumbs.Path path={portfolioItem.categoryPath} />
            </BreadCrumbs>

            <p>{portfolioItem.description}</p>
            <h2>Oplossingen ({solutions.length})</h2>

            <SolutionsList items={solutions} />
          </Box>
        </StyledContainer>
      </StyledLayoutCenteredSingleColumn>
    </StyledContentContainer>
  )
}

export default PortfolioDetailPage
