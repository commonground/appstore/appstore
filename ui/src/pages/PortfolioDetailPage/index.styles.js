// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import Color from 'color'
import { mediaQueries } from '@commonground/design-system'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'

export const StyledLayoutCenteredSingleColumn = styled(
  PageTemplate.LayoutCenteredSingleColumn,
)`
  padding: 0;

  ${mediaQueries.smUp`
    padding: 0 ${(p) => p.theme.tokens.spacing05};
`}
`

export const StyledContainer = styled.div`
  margin-top: 180px;
  background-color: #ffffff;
  max-width: 45rem;
  padding: 0 ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.smUp`
    padding: ${(p) => p.theme.tokens.spacing05} ${(p) =>
    p.theme.tokens.spacing07}
    ${(p) => p.theme.tokens.spacing07} ${(p) => p.theme.tokens.spacing07};
`}
`

export const StyledH1 = styled.h1`
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
`

export const StyledContentContainer = styled(PageTemplate.ContentContainer)`
  &:before {
    background-size: cover;
    background-position: center;
  }

  &:after {
    content: '';
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 20rem;
    z-index: -1;
    background-size: 100%;
    background: linear-gradient(
      ${(p) => {
        const { colorBrand1 } = p.theme.tokens
        return `170deg,
          ${Color(colorBrand1).alpha(0.1).hsl().string()} 0%,
          ${Color(colorBrand1).alpha(0.3).hsl().string()} 20rem
        `
      }}
    );
  }
`
