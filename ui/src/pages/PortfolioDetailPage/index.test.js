// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { MemoryRouter as Router, Route } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'

import theme from '../../theme'
import PortfolioDetailPage from './index'

test('PortfolioDetailPage', () => {
  const { container } = render(
    <ThemeProvider theme={theme}>
      <Router
        initialEntries={['/portfolio/financiele-stabiliteit']}
        initialIndex={0}
      >
        <Route path="/portfolio/:id" component={PortfolioDetailPage} />
      </Router>
    </ThemeProvider>,
  )
  expect(container).toBeTruthy()
})
