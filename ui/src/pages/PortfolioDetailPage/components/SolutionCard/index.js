// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { Card, StyledBlock, StyledBlocks } from './index.styles'

const SolutionCard = ({ id, name, shortDescription, audience, ...props }) => {
  return (
    <Card to={`/portfolio/oplossing/${id}`} title={name} id={id} {...props}>
      <Card.Body>
        <Card.Header>
          <Card.Title>{name}</Card.Title>
        </Card.Header>

        <p>{shortDescription}</p>

        <StyledBlocks>
          <StyledBlock>
            <StyledBlock.Title>Doelgroep</StyledBlock.Title>
            <StyledBlock.Content>{audience}</StyledBlock.Content>
          </StyledBlock>
        </StyledBlocks>
      </Card.Body>
    </Card>
  )
}

SolutionCard.propTypes = {
  id: string.isRequired,
  name: string.isRequired,
  shortDescription: string.isRequired,
  audience: string.isRequired,
}

export default SolutionCard
