// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render, screen } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'

import theme from '../../../../theme'
import SolutionCard from './index'

test('Solution card', () => {
  const item = {
    id: 'budgetbeheer',
    name: 'Budgetbeheer',
    shortDescription: 'Omschrijving',
    audience: 'Burgers',
  }

  render(
    <ThemeProvider theme={theme}>
      <Router>
        <SolutionCard {...item} />
      </Router>
    </ThemeProvider>,
  )

  const product = screen.getByRole('link')
  expect(product.getAttribute('href')).toEqual(
    '/portfolio/oplossing/budgetbeheer',
  )

  const title = screen.getByText('Budgetbeheer')
  expect(title).toBeInTheDocument()

  const shortDescription = screen.getByText('Omschrijving')
  expect(shortDescription).toBeInTheDocument()
})
