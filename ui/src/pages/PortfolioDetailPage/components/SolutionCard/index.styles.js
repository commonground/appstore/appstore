// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Link } from 'react-router-dom'
import { mediaQueries } from '@commonground/design-system'
import iconChevronRight from '../../../../icons/chevron-right.svg'

export const Card = styled(Link)`
  position: relative;
  display: flex;
  width: 100%;
  height: 100%;
  text-decoration: none;
  border-bottom: 1px solid ${(p) => p.theme.tokens.colorPaletteGray300};

  &:hover {
    background-color: ${(p) => p.theme.tokens.colors.colorPaletteGray100};
  }
`

Card.Header = styled.div``

Card.Title = styled.h2`
  margin: 0;
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
  color: ${(p) => p.theme.tokens.colors.colorText};
`

Card.Body = styled.div`
  padding: ${(p) => p.theme.tokens.spacing03};
  flex: 1;
  background: url(${iconChevronRight}) no-repeat center right 15px;
  color: ${(p) => p.theme.tokens.colors.colorText};

  ${mediaQueries.smUp`
    padding: ${(p) => p.theme.tokens.spacing06};
  `}
`

export const StyledBlocks = styled.div`
  display: flex;
`

export const StyledBlock = styled.div`
  margin-right: ${(p) => p.theme.tokens.spacing10};
`

StyledBlock.Title = styled.p`
  margin-top: 0;
  margin-bottom: ${(p) => p.theme.tokens.spacing01};
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray700};
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
`

StyledBlock.Content = styled.p`
  margin-top: ${(p) => p.theme.tokens.spacing01};
  margin-bottom: 0;
`
