// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render, screen } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'
import theme from '../../../../theme'

import SolutionsList from './index'

const dummyItems = [
  {
    id: 'budgetbeheer',
    name: 'Budgetbeheer',
    shortDescription: 'Omschrijving',
    audience: 'Burgers',
  },
  {
    id: 'budgetbeheer-2',
    name: 'Budgetbeheer',
    shortDescription: 'Omschrijving',
    audience: 'Burgers',
  },
]

test('SolutionsList', async () => {
  render(
    <ThemeProvider theme={theme}>
      <Router>
        <SolutionsList items={dummyItems} />
      </Router>
    </ThemeProvider>,
  )

  const cards = await screen.findAllByText('Budgetbeheer')
  expect(cards).toHaveLength(2)
})
