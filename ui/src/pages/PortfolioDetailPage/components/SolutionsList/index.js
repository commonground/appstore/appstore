// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { shape, string, arrayOf } from 'prop-types'

import SolutionCard from '../SolutionCard'
import { Card, CardList } from './index.styles'

const SolutionsList = ({ items, ...props }) => (
  <CardList {...props}>
    {items.map((item) => (
      <Card as="li" key={item.id}>
        <SolutionCard
          id={item.id}
          name={item.name}
          shortDescription={item.shortDescription}
          audience={item.audience}
        />
      </Card>
    ))}
  </CardList>
)

SolutionsList.propTypes = {
  items: arrayOf(
    shape({
      id: string,
      name: string,
      shortDescription: string,
      audience: string,
    }),
  ),
}

SolutionsList.defaultProps = {
  items: [],
}

export default SolutionsList
