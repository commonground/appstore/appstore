// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState, useEffect, createContext, useContext } from 'react'
import { Link, Switch, Route, Redirect } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { string, shape } from 'prop-types'
import { Button } from '@commonground/design-system'
import ProductInfo from '../../components/ProductInfo'
import ProductVisibility from '../../components/ProductVisibility'
import ProductStatusToggleButton from '../../components/ProductStatusToggleButton'
import ProductComponents from '../../components/ProductComponents'
import TabbedRouteContent from '../../components/TabbedRouteContent'
import ProductRepository from '../../domain/product-repository'
import ProductProces from '../../components/ProductProces'
import theme from '../../theme'
import { userOwnsItem } from '../../permissions'
import UserContext from '../../user-context'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import {
  StyledIconPencil,
  StyledPageTemplate,
  StyledPageHeader,
  StyledPageTitle,
  StyledPageActions,
} from './index.styles'

export const ProductContext = createContext({
  product: null,
  setProduct: () => {},
})

const ProductPage = ({ match }) => {
  const [product, setProduct] = useState(null)
  const { user } = useContext(UserContext)

  useEffect(() => {
    if (!match.params.id) {
      return
    }

    const fetchData = async () => {
      try {
        const product = await ProductRepository.getById(match.params.id)
        setProduct(product)
      } catch (e) {
        console.error(e)
      }
    }

    fetchData()
  }, [match.params.id])

  const tabs = product
    ? [
        { to: `/producten/${product.id}/omschrijving`, title: 'Omschrijving' },
        { to: `/producten/${product.id}/proces`, title: 'Proces' },
        { to: `/producten/${product.id}/componenten`, title: 'Componenten' },
      ]
    : []

  return (
    product && (
      <PageTemplate.ContentContainer>
        <PageTemplate.LayoutCenteredSingleColumn>
          <ProductContext.Provider value={{ product, setProduct }}>
            <ThemeProvider theme={theme}>
              <StyledPageTemplate>
                <StyledPageHeader>
                  <StyledPageTitle>
                    <h1>{product.name}</h1>
                    {userOwnsItem(user, product) && (
                      <ProductVisibility product={product} />
                    )}
                  </StyledPageTitle>
                  {userOwnsItem(user, product) && (
                    <StyledPageActions>
                      <Button
                        as={Link}
                        variant="secondary"
                        to={`/producten/${product.id}/bewerk`}
                        data-testid="edit-product-button"
                      >
                        <StyledIconPencil />
                        Product aanpassen
                      </Button>
                      <ProductStatusToggleButton
                        product={product}
                        setProduct={setProduct}
                      />
                    </StyledPageActions>
                  )}
                </StyledPageHeader>

                <TabbedRouteContent tabs={tabs}>
                  <Switch>
                    <Redirect
                      exact
                      path={`${match.path}`}
                      to={`${match.url}/omschrijving`}
                    />
                    <Route path={`${match.url}/omschrijving`}>
                      <ProductInfo product={product} />
                    </Route>
                    <Route path={`${match.url}/proces`}>
                      <ProductProces product={product} />
                    </Route>
                    <Route path={`${match.url}/componenten`}>
                      <ProductComponents components={product.components} />
                    </Route>
                  </Switch>
                </TabbedRouteContent>
              </StyledPageTemplate>
            </ThemeProvider>
          </ProductContext.Provider>
        </PageTemplate.LayoutCenteredSingleColumn>
      </PageTemplate.ContentContainer>
    )
  )
}

ProductPage.propTypes = {
  match: shape({
    url: string,
    params: shape({
      id: string.isRequired,
    }),
  }),
}

export default ProductPage
