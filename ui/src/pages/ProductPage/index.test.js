// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'

import { UserContextProvider } from '../../user-context'
import ProductPage from './index'

test('exists', () => {
  const match = { params: { id: '42' } }
  const { container } = render(
    <UserContextProvider>
      <ProductPage match={match} />
    </UserContextProvider>,
  )
  expect(container).toBeTruthy()
})
