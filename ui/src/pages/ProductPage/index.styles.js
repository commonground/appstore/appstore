// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Flex, Box } from 'reflexbox/styled-components'

import { IconPencil } from '../../icons'

export const StyledPageTemplate = styled.div``

export const StyledPageHeader = styled(Flex)`
  justify-content: space-between;
  flex-wrap: wrap;
`

export const StyledPageTitle = styled(Box)``

export const StyledPageActions = styled(Box)`
  display: flex;
  align-items: flex-start;
`

export const StyledIconPencil = styled(IconPencil)`
  margin-right: ${(p) => p.theme.tokens.spacing03};
`
