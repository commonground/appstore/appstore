// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Link } from 'react-router-dom'

import {
  IconAgile40,
  IconArrowDown,
  IconBox,
  IconCheckCircle,
  IconCodeSlash,
  IconCopy,
  IconEye,
  IconKey,
  IconLock,
  IconNLX40,
  IconPointer,
  IconStack,
  IconTeam,
} from '../../icons'

import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import {
  StyledPrinciples,
  StyledPrinciple,
  AboutPageIcon,
} from './index.styles'

const AboutPage = () => (
  <PageTemplate.ContentContainer>
    <PageTemplate.LayoutCenteredSingleColumn>
      <article>
        <h1>Over de Componentencatalogus</h1>
        <p>
          De componentencatalogus is een initiatief van de community Common
          Ground. Binnen Common Ground werken gemeenten en VNG samen aan
          gemeentelijke informatievoorziening en maatschappelijke opgaven. Wat
          binnen Common Ground wordt gerealiseerd, is opgenomen in deze
          componentencatalogus.
        </p>
        <p>
          U vindt in deze componentencatalogus businessoplossingen, zoals
          applicaties en de nodige, technische componenten om deze oplossingen
          te kunnen implementeren. U heeft hiermee een compleet overzicht van
          wat er binnen Common Ground wordt gerealiseerd inclusief de status.
        </p>
        <p>
          Inmiddels werkt een flinke groep gemeenten, ketenpartners en
          leveranciers mee aan een nieuwe inrichting van de gemeentelijke
          informatievoorziening, gebaseerd op de principes van de
          informatiekundige visie Common Ground. Gegevens worden losgekoppeld
          van processen, en in plaats van data heen en weer te kopiëren tussen
          applicatie-silo’s worden deze door middel van NLX bevraagd bij de
          bron.
        </p>
        <p>
          Dit nieuwe gegevenslandschap wordt gekenmerkt door een
          componentgebaseerde architectuur in vijf lagen.{' '}
          <Link to="/producten">Producten</Link> bestaan uit meerdere{' '}
          <Link to="/componenten">componenten</Link> wat maakt dat ze
          wendbaarder zijn, omdat onderdelen (componenten) eenvoudiger kunnen
          worden vervangen door andere. Componenten worden meestal in samenhang
          met andere componenten ontwikkeld. Zo’n setje componenten noemen we
          een product. Het werken met componenten heeft bovendien als voordeel
          om risico’s te verspreiden en beter te voorkomen dat een component
          meer doet dan waar het voor bedoeld is.
        </p>
        <p>
          Ontwikkelteams die meewerken in de community van{' '}
          <a href="https://commonground.nl/cms/view/54476259/wat-is-common-ground">
            Common Ground
          </a>{' '}
          publiceren een overzicht van de componenten in de Common Ground
          Componentencatalogus, zodat we van elkaar goed weten waar we aan
          werken, waar we kunnen samenwerken om herbruikbaarheid te bevorderen
          en hoe de voortgang verloopt.
        </p>

        <h2>Principes van Common Ground</h2>
        <h3>Realisatieprincipes</h3>
        <p>
          Dit zijn praktische uitgangspunten die beschrijven hoe we werken en
          welke keuzes we daarbij maken.{' '}
          <a
            href="/20190130_-_Common_Ground_-_Realisatieprincipes.pdf"
            target="_blank"
            rel="noopener noreferrer"
          >
            Lees meer over onze principes.
          </a>
        </p>

        <StyledPrinciples>
          <StyledPrinciple>
            <AboutPageIcon as={IconTeam} />
            <StyledPrinciple.Title>Community</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              Gemeenten, ketenpartners, marktpartijen en de VNG werken samen in
              het realiseren van Common Ground.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <IconAgile40 />
            <StyledPrinciple.Title>Agile</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              We ontwikkelen incrementeel en iteratief.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <AboutPageIcon as={IconCopy} />
            <StyledPrinciple.Title>Nieuw naast oud</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              De nieuwe architectuur vervangt geleidelijk de bestaande
              architectuur.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <AboutPageIcon as={IconPointer} />
            <StyledPrinciple.Title>Moderne IT</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              Kansen voor nieuwe technologie moeten benut worden, mits dit
              meerwaarde heeft.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <AboutPageIcon as={IconCodeSlash} />
            <StyledPrinciple.Title>Open Source</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              Stimuleren van open source software om samenwerking en hergebruik
              te bevorderen.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <AboutPageIcon as={IconStack} />
            <StyledPrinciple.Title>Lagen Architectuur</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              Verantwoordelijkheden worden gescheiden in lagen en componenten.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <IconNLX40 />
            <StyledPrinciple.Title>NLX als gateway</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              Zorgen voor veilige, betrouwbare, transparante en snelle
              uitwisseling van gegevens Met de NLX laten we zien hoe dit kan
              werken.
            </StyledPrinciple.Content>
          </StyledPrinciple>
        </StyledPrinciples>

        <h3>Informatiearchitectuurprincipes</h3>
        <p>
          Onze fundamentele keuzes over hoe er met gegevens en applicaties moet
          worden omgegaan.{' '}
          <a
            href="/20190130_-_Common_Ground_-_Informatiearchitectuurprincipes.pdf"
            target="_blank"
            rel="noopener noreferrer"
          >
            Lees meer over onze principes.
          </a>
        </p>

        <StyledPrinciples>
          <StyledPrinciple>
            <AboutPageIcon as={IconBox} />
            <StyledPrinciple.Title>Componentgebaseerd</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              We gebruiken ontkoppelde componenten met afgebakende
              functionaliteit en gestandaardiseerde interfaces.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <AboutPageIcon as={IconEye} />
            <StyledPrinciple.Title>Open</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              We werken transparant en controleerbaar met duurzaam toegankelijke
              gegevens.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <AboutPageIcon as={IconLock} />
            <StyledPrinciple.Title>Vertrouwd</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              We zorgen dat informatiebeveiliging en privacy op orde zijn.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <AboutPageIcon as={IconArrowDown} />
            <StyledPrinciple.Title>Eenmalige vastlegging</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              We leggen gegevens eenmalig vast en vragen op bij de bron.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <AboutPageIcon as={IconKey} />
            <StyledPrinciple.Title>Regie op gegevens</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              We faciliteren regie op gegevens.
            </StyledPrinciple.Content>
          </StyledPrinciple>

          <StyledPrinciple>
            <AboutPageIcon as={IconCheckCircle} />
            <StyledPrinciple.Title>Standaarden</StyledPrinciple.Title>
            <StyledPrinciple.Content>
              We standaardiseren maximaal.
            </StyledPrinciple.Content>
          </StyledPrinciple>
        </StyledPrinciples>
      </article>
    </PageTemplate.LayoutCenteredSingleColumn>
  </PageTemplate.ContentContainer>
)

export default AboutPage
