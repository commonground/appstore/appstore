// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledPrinciples = styled.div`
  display: flex;
  margin: 0 -${(p) => p.theme.tokens.spacing04} ${(p) =>
      p.theme.tokens.spacing05} -${(p) => p.theme.tokens.spacing04};
  flex-wrap: wrap;
`

export const StyledPrinciple = styled.div`
  background-color: ${(p) => p.theme.tokens.colors.brandSecondary1};
  flex: 0 1 300px;
  margin: 0 ${(p) => p.theme.tokens.spacing04}
    ${(p) => p.theme.tokens.spacing06} ${(p) => p.theme.tokens.spacing04};
  padding: ${(p) => p.theme.tokens.spacing06};
`

StyledPrinciple.Title = styled.h3`
  margin-top: ${(p) => p.theme.tokens.spacing05};
`

StyledPrinciple.Icon = styled.img`
  display: block;
`

StyledPrinciple.Content = styled.p`
  margin-bottom: 0;
`

// To be used with `as={IconX}` - results in less css
export const AboutPageIcon = styled.figure`
  width: 40px;
  height: 40px;
  fill: ${(p) => p.theme.tokens.colorPaletteGray500};
`
