// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { func, oneOf, string } from 'prop-types'
import React, { useContext, useState } from 'react'
import { withRouter } from 'react-router'
import { Link, useHistory } from 'react-router-dom'
import { Drawer } from '@commonground/design-system'
import AddComponentForm from '../../components/AddComponentForm'
import ComponentRepository from '../../domain/component-repository'
import Alert from '../../components/design-system-candidates/Alert'
import { LAYER_TYPES } from '../../vocabulary'
import ProductRepository from '../../domain/product-repository'
import { ProductContext } from '../ProductPage'
import UserContext from '../../user-context'
import { userIsProductOwner } from '../../permissions'
import errorToUserFriendlyMessage from './error-to-user-friendly-message'
import { StyledTitle } from './index.styles'

const AddComponentPage = ({
  createHandler,
  addComponentHandler,
  parentUrl,
  layerType,
  ...props
}) => {
  const [isSubmitted, setIsSubmitted] = useState(false)
  const [success, setSuccess] = useState(null)
  const [errorMessage, setErrorMessage] = useState(null)

  const { product, setProduct } = useContext(ProductContext)
  const history = useHistory()
  const userContext = useContext(UserContext)
  const user = userContext && userContext.user ? userContext.user : null

  const close = () => history.push(parentUrl)

  if (!errorMessage && !userIsProductOwner(user)) {
    setErrorMessage(errorToUserFriendlyMessage('forbidden'))
  }

  const onFormSubmit = async (values) => {
    try {
      setIsSubmitted(true)
      const component = await createHandler(values)
      if (product) {
        const updatedProduct = await addComponentHandler(product.id, [
          component,
        ])
        setTimeout(() => setProduct(updatedProduct), 150)
        setTimeout(close, 150)
      }
      setSuccess(true)
      setErrorMessage(null)
    } catch (error) {
      setErrorMessage(errorToUserFriendlyMessage(error.message))
      setSuccess(false)
    }
  }

  return (
    <Drawer closeHandler={close} {...props}>
      <StyledTitle>Component toevoegen</StyledTitle>
      {errorMessage != null && !success ? (
        <Alert
          type="error"
          data-testid="error-message"
          title={errorMessage.title}
        >
          {errorMessage.content}
        </Alert>
      ) : null}

      {userIsProductOwner(user) && isSubmitted && success ? (
        <Alert
          type="success"
          data-testid="success-message"
          title="Component toegevoegd"
        >
          Ga terug naar het <Link to="/componenten">componentenoverzicht</Link>.
        </Alert>
      ) : null}

      {userIsProductOwner(user) &&
      (!isSubmitted || (isSubmitted && !success)) ? (
        <AddComponentForm
          onSubmitHandler={onFormSubmit}
          data-testid="form"
          initialValues={{ layerType }}
        />
      ) : null}
    </Drawer>
  )
}

AddComponentPage.propTypes = {
  createHandler: func,
  addComponentHandler: func,
  parentUrl: string.isRequired,
  layerType: oneOf(LAYER_TYPES),
}

AddComponentPage.defaultProps = {
  createHandler: ComponentRepository.create,
  addComponentHandler: ProductRepository.addComponent,
  parentUrl: '/componenten',
}

export default withRouter(AddComponentPage)
