// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'

export const StyledTitle = styled.h1`
  margin: 0 0 ${(p) => p.theme.tokens.spacing02} 0;
`
