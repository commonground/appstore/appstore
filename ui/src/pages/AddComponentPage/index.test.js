// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'

import UserContext from '../../user-context'
import { renderWithProviders, fireEvent } from '../../test-utils'
import AddComponentPage from './index'

// eslint-disable-next-line react/prop-types
jest.mock('../../components/AddComponentForm', () => ({ onSubmitHandler }) => (
  <form onSubmit={() => onSubmitHandler({ foo: 'bar' })} data-testid="form">
    <button type="submit" />
  </form>
))

describe('the AddComponentPage', () => {
  afterEach(() => {
    jest.resetModules()
  })

  it('on initialization', () => {
    const userContext = {
      user: {
        isProductOwner: true,
      },
    }
    const { getByTestId, queryByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <AddComponentPage />
        </UserContext.Provider>
      </Router>,
    )

    expect(getByTestId('form')).toBeTruthy()
    expect(queryByTestId('success-message')).toBeNull()
  })

  it('user is not allowed to create a component', () => {
    const userContext = {
      user: {
        isProductOwner: false,
      },
    }
    const { getByTestId, queryByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <AddComponentPage />
        </UserContext.Provider>
      </Router>,
    )

    expect(queryByTestId('form')).toBeNull()
    const errorMessage = getByTestId('error-message')
    expect(errorMessage).toBeTruthy()
    expect(errorMessage.textContent).toEqual(
      'Niet toegestaanJe hebt geen rechten om een component toe te voegen.',
    )
  })

  it('successfully submitting the form', async () => {
    const createHandler = jest.fn().mockResolvedValue()
    const userContext = {
      user: {
        isProductOwner: true,
      },
    }
    const { findByTestId, queryByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <AddComponentPage createHandler={createHandler} />
        </UserContext.Provider>
      </Router>,
    )

    const addComponentForm = await findByTestId('form')
    await fireEvent.submit(addComponentForm)

    expect(await findByTestId('success-message')).toBeTruthy()
    expect(queryByTestId('form')).toBeNull()

    const successMessage = await findByTestId('success-message')
    const link = successMessage.querySelector('a')

    expect(link.getAttribute('href')).toEqual('/componenten')
  })

  it('re-submitting the form when the previous submission went wrong', async () => {
    const createHandler = jest
      .fn()
      .mockResolvedValue({})
      .mockRejectedValueOnce(new Error('error'))
    const userContext = {
      user: {
        isProductOwner: true,
      },
    }

    const { findByTestId, queryByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <AddComponentPage createHandler={createHandler} />
        </UserContext.Provider>
      </Router>,
    )

    const addComponentForm = await findByTestId('form')
    await fireEvent.submit(addComponentForm)
    expect(createHandler).toHaveBeenCalledTimes(1)
    expect(await findByTestId('error-message')).toBeTruthy()
    expect(queryByTestId('success-message')).toBeNull()

    await fireEvent.submit(addComponentForm)
    expect(createHandler).toHaveBeenCalledTimes(2)
    expect(await findByTestId('success-message')).toBeTruthy()
    expect(queryByTestId('error-message')).toBeNull()
  })

  it('submitting when the user is unauthenticated according to the server', async () => {
    const createHandler = jest.fn().mockRejectedValue(new Error('unauthorized'))
    const userContext = {
      user: {
        isProductOwner: true,
      },
    }
    const { findByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <AddComponentPage createHandler={createHandler} />
        </UserContext.Provider>
      </Router>,
    )

    const addComponentForm = await findByTestId('form')
    await fireEvent.submit(addComponentForm)

    const message = await findByTestId('error-message')
    expect(message).toBeTruthy()
    expect(message.textContent).toBe('Niet toegestaanJe bent niet ingelogd.')
  })

  it('submitting when the user is unauthorized according to the server', async () => {
    const createHandler = jest.fn().mockRejectedValue(new Error('forbidden'))
    const userContext = {
      user: {
        isProductOwner: true,
      },
    }
    const { findByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <AddComponentPage createHandler={createHandler} />
        </UserContext.Provider>
      </Router>,
    )

    const addComponentForm = await findByTestId('form')
    await fireEvent.submit(addComponentForm)

    const message = await findByTestId('error-message')
    expect(message).toBeTruthy()
    expect(message.textContent).toBe(
      'Niet toegestaanJe hebt geen rechten om een component toe te voegen.',
    )
  })

  it('submitting the form with invalid data which has not been caught by client-side validation', async () => {
    const createHandler = jest
      .fn()
      .mockRejectedValue(new Error('invalid user input'))
    const userContext = {
      user: {
        isProductOwner: true,
      },
    }
    const { findByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <AddComponentPage createHandler={createHandler} />
        </UserContext.Provider>
      </Router>,
    )

    const addComponentForm = await findByTestId('form')
    fireEvent.submit(addComponentForm)

    const message = await findByTestId('error-message')
    expect(message).toBeTruthy()
    expect(message.textContent).toBe(
      'Ongeldige invoerDe opgegeven waarden zijn ongeldig.',
    )
  })

  it('when the server is unable to handle the request', async () => {
    const createHandler = jest
      .fn()
      .mockRejectedValue(new Error('arbitrary reason'))
    const userContext = {
      user: {
        isProductOwner: true,
      },
    }
    const { findByTestId } = renderWithProviders(
      <Router>
        <UserContext.Provider value={userContext}>
          <AddComponentPage createHandler={createHandler} />
        </UserContext.Provider>
      </Router>,
    )

    const addComponentForm = await findByTestId('form')
    await fireEvent.submit(addComponentForm)

    const message = await findByTestId('error-message')
    expect(message).toBeTruthy()
    expect(message.textContent).toBe(
      'Oeps, er ging iets foutEr ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
    )
  })
})
