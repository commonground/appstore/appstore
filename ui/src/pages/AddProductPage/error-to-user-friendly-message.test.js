// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import errorToUserFriendlyMessage from './error-to-user-friendly-message'

describe('convert error to user friendly message', () => {
  describe('for a known error message', () => {
    it('should return the error message for invalid user input', () => {
      expect(errorToUserFriendlyMessage('invalid user input')).toEqual({
        title: 'Ongeldige invoer',
        content: 'De opgegeven waarden zijn ongeldig.',
      })
    })

    it('should return the error message for unauthorized', () => {
      expect(errorToUserFriendlyMessage('unauthorized')).toEqual({
        title: 'Niet toegestaan',
        content: 'Je bent niet ingelogd.',
      })
    })

    it('should return the error message for forbidden', () => {
      expect(errorToUserFriendlyMessage('forbidden')).toEqual({
        title: 'Niet toegestaan',
        content: 'Je hebt geen rechten om een product toe te voegen.',
      })
    })
  })

  describe('with an unknown error message', () => {
    it('should return the unexpected error message', () => {
      expect(errorToUserFriendlyMessage('somerandomerrorcode1337')).toEqual({
        title: 'Oeps, er ging iets fout',
        content:
          'Er ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
      })
    })
  })
})
