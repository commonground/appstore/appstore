// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { Component } from 'react'
import { func, object } from 'prop-types'
import { Flex, Box } from 'reflexbox/styled-components'
import ProductRepository from '../../domain/product-repository'
import ProductForm from '../../components/ProductForm'
import Alert from '../../components/design-system-candidates/Alert'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import errorToUserFriendlyMessage from './error-to-user-friendly-message'

class AddProductPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isSubmitted: false,
      errorMessage: null,
    }

    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }

  handleFormSubmit(values) {
    const { createHandler, history } = this.props

    createHandler(values)
      .then((product) => {
        history.push(`/producten/${product.id}/componenten`)
      })
      .catch((error) => {
        this.setState({
          isSubmitted: true,
          errorMessage: errorToUserFriendlyMessage(error.message),
        })
      })
  }

  render() {
    const { isSubmitted, errorMessage } = this.state

    return (
      <PageTemplate.ContentContainer>
        <PageTemplate.LayoutCenteredSingleColumn>
          <Flex>
            <Box width={[1, 2 / 3]}>
              <h1>Product toevoegen</h1>
              <p>Je kunt tussentijds opslaan en later verder gaan.</p>
              {isSubmitted ? (
                <Alert
                  type="error"
                  data-testid="error-message"
                  title={errorMessage.title}
                >
                  {errorMessage.content}
                </Alert>
              ) : (
                <ProductForm
                  onSubmitHandler={this.handleFormSubmit}
                  submitButtonText="Concept opslaan"
                  buttonInfoText="Er wordt nog niets gepubliceerd"
                  data-testid="form"
                />
              )}
            </Box>
          </Flex>
        </PageTemplate.LayoutCenteredSingleColumn>
      </PageTemplate.ContentContainer>
    )
  }
}

AddProductPage.propTypes = {
  createHandler: func,
  history: object,
}

AddProductPage.defaultProps = {
  createHandler: ProductRepository.create,
}

export default AddProductPage
