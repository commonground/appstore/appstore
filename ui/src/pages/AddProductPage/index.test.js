// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render, fireEvent } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'

import theme from '../../theme'
import AddProductPage from './index'

// eslint-disable-next-line react/prop-types
jest.mock('../../components/ProductForm', () => ({ onSubmitHandler }) => (
  <form onSubmit={() => onSubmitHandler({ foo: 'bar' })} data-testid="form">
    <button type="submit" />
  </form>
))

describe('the AddProductPage', () => {
  it('on initialization', () => {
    const { getByTestId, queryByTestId } = render(
      <ThemeProvider theme={theme}>
        <Router>
          <AddProductPage />
        </Router>
      </ThemeProvider>,
    )

    expect(getByTestId('form')).toBeTruthy()
    expect(queryByTestId('success-message')).toBeNull()
  })

  it('successfully submitting the form', async () => {
    const createHandler = jest.fn().mockResolvedValue({ id: 43 })
    const historyPush = jest.fn()

    const { findByTestId } = render(
      <ThemeProvider theme={theme}>
        <Router>
          <AddProductPage
            createHandler={createHandler}
            history={{ push: historyPush }}
          />
        </Router>
      </ThemeProvider>,
    )

    const addComponentForm = await findByTestId('form')
    await fireEvent.submit(addComponentForm)

    expect(historyPush).toHaveBeenCalledWith('/producten/43/componenten')
  })

  it('submitting the form with invalid data which has not been caught by client-side validation', async () => {
    const createHandler = jest
      .fn()
      .mockRejectedValue(new Error('invalid user input'))

    const { findByTestId } = render(
      <ThemeProvider theme={theme}>
        <Router>
          <AddProductPage createHandler={createHandler} />
        </Router>
      </ThemeProvider>,
    )

    const addComponentForm = await findByTestId('form')
    await fireEvent.submit(addComponentForm)

    const message = await findByTestId('error-message')
    expect(message).toBeTruthy()
    expect(message.textContent).toBe(
      'Ongeldige invoerDe opgegeven waarden zijn ongeldig.',
    )
  })

  it('when the server is unable to handle the request', async () => {
    const createHandler = jest
      .fn()
      .mockRejectedValue(new Error('arbitrary reason'))

    const { findByTestId } = render(
      <ThemeProvider theme={theme}>
        <Router>
          <AddProductPage createHandler={createHandler} />
        </Router>
      </ThemeProvider>,
    )

    const addComponentForm = await findByTestId('form')
    await fireEvent.submit(addComponentForm)

    const message = await findByTestId('error-message')
    expect(message).toBeTruthy()
    expect(message.textContent).toBe(
      'Oeps, er ging iets foutEr ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
    )
  })
})
