// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Label = styled.label`
  display: block;
`

export const Fieldset = styled.fieldset`
  border: 0 none;
  padding: 0 0 1rem 0;
`
