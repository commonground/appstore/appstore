// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Button, mediaQueries } from '@commonground/design-system'

import { Col } from '../../components/design-system-candidates/Grid'
import Alert from '../../components/design-system-candidates/Alert'
import { IconAdd } from '../../icons'

export const AddButton = styled(Button)`
  ${mediaQueries.mdUp`
    float: left; 
  `}
`

export const StyledIconAdd = styled(IconAdd)`
  margin-right: ${(p) => p.theme.tokens.spacing03};
`

export const StyledNoResults = styled.div`
  h3 {
    font-weight: 500;
  }

  color: ${(p) => p.theme.tokens.colors.colorPaletteGray700};
  font-style: normal;
`

export const StyledResultSummary = styled.p`
  text-transform: uppercase;
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray700};
`

export const ActionsCol = styled(Col)`
  flex-grow: 1;
  min-width: 170px; /* Required for Button */
`

export const StyledAlert = styled(Alert)`
  ${mediaQueries.smUp`
    width: 75%;
  `}
`
