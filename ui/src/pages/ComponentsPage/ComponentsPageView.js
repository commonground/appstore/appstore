// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { func, bool, array, object } from 'prop-types'
import { Link } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'

import UserContext from '../../user-context'
import { Row, Col } from '../../components/design-system-candidates/Grid'
import theme from '../../theme'
import ComponentSearch from '../../components/ComponentSearch'
import ComponentsGroupedByLayer from '../../components/ComponentsGroupedByLayer'
import { userIsProductOwner } from '../../permissions'
import {
  AddButton,
  StyledIconAdd,
  StyledNoResults,
  ActionsCol,
  StyledResultSummary,
  StyledAlert,
} from './index.styles'

const hasFiltersFactory = (filters) => () =>
  Object.values(filters).some((filter) => filter.length > 0)

const ComponentsPageView = ({
  components,
  filters,
  onQueryChanged,
  onStatusChanged,
  loading,
  error,
}) => {
  const hasFilters = hasFiltersFactory(filters)

  return (
    <ThemeProvider theme={theme}>
      <Row>
        <Col width={[1, 1, 3 / 4]}>
          <h1 data-testid="heading">Componenten</h1>
          <p>
            Kleine brokjes herbruikbare code. Deze zijn ingedeeld volgens{' '}
            <Link to="/5-lagen-model">het 5-lagen model</Link>. Hiermee creëren
            we meer flexibiliteit, spreiden we risico’s en voorkomen we dat een
            component meer doet dan waar het voor bedoeld is.
          </p>
        </Col>
        <ActionsCol width={[1, 1, 1 / 4]}>
          <UserContext.Consumer>
            {
              (userContext) =>
                userContext && userIsProductOwner(userContext.user) ? (
                  <AddButton
                    as={Link}
                    to="/componenten/nieuw"
                    role="button"
                    variant="secondary"
                    data-testid="add-component-button"
                  >
                    <StyledIconAdd />
                    Component toevoegen
                  </AddButton>
                ) : null
              /* eslint-disable-next-line react/jsx-curly-newline */
            }
          </UserContext.Consumer>
        </ActionsCol>
      </Row>

      <ComponentSearch
        filters={filters}
        onQueryChanged={onQueryChanged}
        onStatusChanged={onStatusChanged}
        searchTestId="search-box"
      />

      {loading && components.length < 1 ? null : error ? (
        <StyledAlert
          type="error"
          data-testid="unexpected-error"
          title="Er ging iets fout"
        >
          Er ging onverwachts iets fout. Gelieve opnieuw te zoeken.
        </StyledAlert>
      ) : components && components.length >= 1 ? (
        <>
          <StyledResultSummary data-testid="result-summary">
            {hasFilters()
              ? `${components.length} componenten gevonden`
              : // We want to maintain the height of the element, even without content.
                // Because using &nbsp; doesn't work using this ternary operator, we use the following .replace().
                // Copied from https://stackoverflow.com/a/24437562
                ' '.replace(/ /g, '\u00a0')}
          </StyledResultSummary>

          <ComponentsGroupedByLayer
            components={components}
            data-testid="components-overview"
          />
        </>
      ) : (
        <StyledNoResults data-testid="no-results">
          <h3>Geen componenten gevonden</h3>
          <p>Probeer een andere zoekterm of filter.</p>

          <p>
            Kan je niet vinden wat je zoekt?
            <br />
            <Link to="/componenten/nieuw">Voeg een gewenst component toe</Link>
          </p>
        </StyledNoResults>
      )}
    </ThemeProvider>
  )
}

ComponentsPageView.propTypes = {
  components: array.isRequired, // Not using it locally, for shape see ComponentsGroupedByLayer
  filters: object,
  onQueryChanged: func.isRequired,
  onStatusChanged: func.isRequired,
  loading: bool.isRequired,
  error: bool,
}
ComponentsPageView.defaultProps = {}

export default ComponentsPageView
