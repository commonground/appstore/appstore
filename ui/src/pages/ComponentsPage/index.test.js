// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { fireEvent, act, render, waitFor } from '@testing-library/react'
import { StaticRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import theme from '../../theme'
import ComponentsPage from './index'

jest.mock('../../components/ComponentsGroupedByLayer', () => {
  return {
    __esModule: true,
    default: () => {
      return <div data-testid="components-overview" />
    },
  }
})

jest.mock('../../components/ComponentDetailPane', () => {
  return {
    __esModule: true,
    default: () => {
      return <div data-testid="component-details-page" />
    },
  }
})

jest.mock('../AddComponentPage', () => {
  return {
    __esModule: true,
    default: () => {
      return <div data-testid="add-component-page" />
    },
  }
})

describe('ComponentsPage', () => {
  it('hides the search results initially', async () => {
    const nonResolvingPromise = new Promise(jest.fn())
    const searchComponentsHandlerSpy = jest
      .fn()
      .mockImplementation(() => nonResolvingPromise)

    const { queryByTestId } = render(
      <Router location="/componenten?query=foo&status=bruikbaar">
        <ThemeProvider theme={theme}>
          <ComponentsPage
            searchComponentsHandler={searchComponentsHandlerSpy}
          />
        </ThemeProvider>
      </Router>,
    )

    expect(searchComponentsHandlerSpy).toHaveBeenCalledWith({
      query: 'foo',
      status: 'bruikbaar',
    })

    expect(queryByTestId('result-summary')).toBeNull()
    expect(queryByTestId('heading')).toBeTruthy()
    expect(queryByTestId('search-box')).toBeTruthy()
  })

  it('requests on pageload if only query is set', async () => {
    const nonResolvingPromise = new Promise(jest.fn())
    const searchComponentsHandlerSpy = jest
      .fn()
      .mockImplementation(() => nonResolvingPromise)

    await act(async () => {
      render(
        <ThemeProvider theme={theme}>
          <Router location="/componenten?query=foo">
            <ComponentsPage
              searchComponentsHandler={searchComponentsHandlerSpy}
            />
          </Router>
        </ThemeProvider>,
      )
    })

    expect(searchComponentsHandlerSpy).toHaveBeenCalledWith({
      query: 'foo',
      status: '',
    })
  })

  it('requests on pageload if only status is set', async () => {
    const nonResolvingPromise = new Promise(jest.fn())
    const searchComponentsHandlerSpy = jest
      .fn()
      .mockImplementation(() => nonResolvingPromise)

    await act(async () => {
      render(
        <Router location="/componenten?status=bruikbaar">
          <ThemeProvider theme={theme}>
            <ComponentsPage
              searchComponentsHandler={searchComponentsHandlerSpy}
            />
          </ThemeProvider>
        </Router>,
      )
    })

    expect(searchComponentsHandlerSpy).toHaveBeenCalledWith({
      query: '',
      status: 'bruikbaar',
    })
  })

  describe('unexpected error occurs while fetching the components', () => {
    it('should show an error message', async () => {
      const { queryAllByTestId, getByTestId, queryByTestId, findByTestId } =
        render(
          <Router location="/componenten">
            <ThemeProvider theme={theme}>
              <ComponentsPage
                searchComponentsHandler={() => {
                  return Promise.reject(new Error('arbitrary error'))
                }}
              />
            </ThemeProvider>
          </Router>,
        )

      await waitFor(async () => findByTestId('heading'))

      expect(getByTestId('unexpected-error')).toBeTruthy()
      expect(queryAllByTestId('components-overview')).toHaveLength(0)
      expect(queryByTestId('no-results')).toBeNull()
    })
  })

  describe('no results', () => {
    it('should show a no-results message', async () => {
      const { queryAllByTestId, getByTestId, findByTestId } = render(
        <Router location="/componenten">
          <ThemeProvider theme={theme}>
            <ComponentsPage
              searchComponentsHandler={() => Promise.resolve([])}
            />
          </ThemeProvider>
        </Router>,
      )

      await waitFor(async () => findByTestId('heading'))

      expect(queryAllByTestId('components-overview')).toHaveLength(0)
      expect(getByTestId('no-results')).toBeTruthy()
    })
  })

  describe('searching for a component', () => {
    it('should show the components overview ', async () => {
      const ESCAPE_KEY_CODE = 27

      const { getByTestId, queryAllByTestId, findByTestId } = render(
        <Router location="/componenten">
          <ThemeProvider theme={theme}>
            <ComponentsPage
              searchComponentsHandler={() => Promise.resolve([{}])}
            />
          </ThemeProvider>
        </Router>,
      )

      await waitFor(async () => findByTestId('heading'))
      expect(queryAllByTestId('components-overview')).toHaveLength(1)

      const input = getByTestId('search-box').querySelector('input')
      await act(async () => {
        fireEvent.change(input, { target: { value: 'abc' } })
      })

      expect(input.value).toEqual('abc')
      expect(queryAllByTestId('no-results')).toHaveLength(0)

      await waitFor(() => expect(window.location.search).toBe('?query=abc'))

      expect(getByTestId('result-summary')).toHaveTextContent(
        '1 componenten gevonden',
      )

      fireEvent.keyDown(global.document, {
        keyCode: ESCAPE_KEY_CODE,
      })

      expect(input.value).toEqual('')
    })
  })

  describe('the nested route /:id', () => {
    it('should display the ComponentDetail pane', async () => {
      const promise = Promise.resolve([{}])
      const mockHandler = jest.fn(() => promise)

      const { getByTestId } = render(
        <Router location="/componenten/42">
          <ThemeProvider theme={theme}>
            <ComponentsPage searchComponentsHandler={mockHandler} />
          </ThemeProvider>
        </Router>,
      )

      expect(getByTestId('component-details-page')).toBeTruthy()
      await act(() => promise) // prevent `update not wrapped in act()` warning
    })
  })

  describe('the nested route /nieuw', () => {
    it('should display the AddComponentDetail pane', async () => {
      const promise = Promise.resolve([{}])
      const mockHandler = jest.fn(() => promise)

      const { getByTestId, queryByTestId } = render(
        <Router location="/componenten/nieuw">
          <ThemeProvider theme={theme}>
            <ComponentsPage searchComponentsHandler={mockHandler} />
          </ThemeProvider>
        </Router>,
      )

      expect(getByTestId('add-component-page')).toBeTruthy()
      expect(queryByTestId('component-details-page')).toBeNull()
      await act(() => promise)
    })
  })
})
