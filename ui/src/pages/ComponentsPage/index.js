// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useEffect, useState } from 'react'
import { useRouteMatch } from 'react-router'
import { Route, useLocation } from 'react-router-dom'
import { func } from 'prop-types'
import { useDebouncedCallback } from 'use-debounce'
import ComponentRepository from '../../domain/component-repository'
import ComponentDetailPane from '../../components/ComponentDetailPane'
import AddComponentPage from '../AddComponentPage'
import EditComponentPage from '../EditComponentPage'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import ComponentsPageView from './ComponentsPageView'

const ESCAPE_KEY_CODE = 27
const isEscapeKey = (keyCode) => keyCode === ESCAPE_KEY_CODE

export const ComponentsPage = ({ searchComponentsHandler }) => {
  const match = useRouteMatch('/componenten')
  const location = useLocation()
  const urlParams = new URLSearchParams(location.search)

  // Make this smarter when we get more search filters
  const [query, setQuery] = useState(urlParams.get('query') || '')
  const [status, setStatus] = useState(urlParams.get('status') || '')

  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(null)
  const [components, setComponents] = useState([])

  const onKeydownHandler = (event) => {
    if (isEscapeKey(event.keyCode)) {
      handleQueryChange('')
    }
  }

  const resolveUrlParams = (params) => {
    Object.entries(params).forEach(([param, value]) => {
      if (value) {
        urlParams.set(param, encodeURIComponent(value))
      } else {
        urlParams.delete(param)
      }
    })

    return urlParams.toString()
  }

  const fetchComponents = () => {
    setLoading(true)
    setError(false)

    const urlParams = resolveUrlParams({ query, status })
    window.history.pushState({}, null, `?${urlParams}`)

    return searchComponentsHandler({ query, status })
      .then((components) => {
        setLoading(false)
        setError(false)
        setComponents(components)
      })
      .catch(() => {
        setLoading(false)
        setError(true)
      })
  }

  const fetchComponentsDebounced = useDebouncedCallback(fetchComponents, 150)

  const handleQueryChange = (value) => {
    setQuery(value)
  }

  const handleStatusChange = (status) => {
    setStatus(status ? status.value : '')
  }

  // Watch for search state changes and call debounced fetch function
  useEffect(() => {
    fetchComponentsDebounced()
  }, [query, status, fetchComponentsDebounced])

  useEffect(() => {
    const fetchData = async () => {
      await fetchComponents(query)
    }

    fetchData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname]) // we want to reload the components if we navigate from a sub-page to the overview page

  useEffect(() => {
    document.addEventListener('keydown', onKeydownHandler, false)

    return () => {
      document.removeEventListener('keydown', onKeydownHandler, false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <PageTemplate.ContentContainer>
        <PageTemplate.LayoutCenteredSingleColumn>
          <ComponentsPageView
            components={components}
            filters={{ query, status }}
            onQueryChanged={handleQueryChange}
            onStatusChanged={handleStatusChange}
            loading={loading}
            error={error}
          />

          <Route path={`${match.url}/nieuw`} component={AddComponentPage} />
          <Route
            exact
            path={`${match.url}/:id(\\d+)`}
            component={ComponentDetailPane}
          />
          <Route
            path={`${match.url}/:id/bewerk`}
            component={EditComponentPage}
          />
        </PageTemplate.LayoutCenteredSingleColumn>
      </PageTemplate.ContentContainer>
    </>
  )
}

ComponentsPage.propTypes = {
  searchComponentsHandler: func.isRequired,
}

ComponentsPage.defaultProps = {
  searchComponentsHandler: ComponentRepository.filterComponents,
}

export default ComponentsPage
