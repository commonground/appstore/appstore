// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'
import { render } from '@testing-library/react'
import UserContext from '../../user-context'
import ComponentsPageView from './ComponentsPageView'

test('The Add component button', () => {
  const userContext = {
    user: {
      id: 42,
      isProductOwner: true,
    },
  }

  const { getByTestId, queryByTestId, rerender } = render(
    <Router>
      <UserContext.Provider value={userContext}>
        <ComponentsPageView
          components={[]}
          onQueryChanged={() => {}}
          onStatusChanged={() => {}}
          loading={false}
          filters={{ query: '', status: '' }}
        />
      </UserContext.Provider>
    </Router>,
  )

  const addComponentButton = getByTestId('add-component-button')
  expect(addComponentButton).toBeTruthy()
  expect(addComponentButton.getAttribute('href')).toEqual('/componenten/nieuw')

  rerender(
    <Router>
      <UserContext.Provider value={null}>
        <ComponentsPageView
          components={[]}
          onQueryChanged={() => {}}
          onStatusChanged={() => {}}
          loading={false}
          filters={{ query: '', status: '' }}
        />
      </UserContext.Provider>
    </Router>,
  )

  expect(queryByTestId('add-component-button')).toBeNull()
})
