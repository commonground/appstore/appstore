// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import { IconMail, IconPhone } from '../../icons'

export const StyledContactList = styled.ul`
  background-color: ${(p) => p.theme.tokens.colors.brandSecondary1};
  min-width: 400px;
  padding: ${(p) => p.theme.tokens.spacing07};
  list-style-type: none;
  display: inline-block;
`

StyledContactList.Item = styled.li`
  &:not(:last-child) {
    margin: 0 0 0.75rem 0;
  }
`

export const StyledIconMail = styled(IconMail)`
  float: left;
  margin: 0 1rem 0 0;
`

export const StyledIconPhone = styled(IconPhone)`
  float: left;
  margin: 0 1rem 0 0;
`
