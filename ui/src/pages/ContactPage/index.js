// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import {
  StyledContactList,
  StyledIconMail,
  StyledIconPhone,
} from './index.styles'

const ContactPage = () => (
  <PageTemplate.ContentContainer>
    <PageTemplate.LayoutCenteredSingleColumn>
      <article>
        <h1>Contact</h1>
        <p>
          De Common Ground Componentencatalogus wordt ontwikkeld door{' '}
          <a href="https://www.vngrealisatie.nl">VNG Realisatie</a>.
        </p>

        <h2>Vragen over hergebruik?</h2>
        <p>
          Neem contact op met de contactpersoon die vermeld staat bij de
          applicatie. Kom je er niet uit? Neem dan contact op met VNG
          Realisatie.
        </p>

        <h2>Vragen over het toevoegen van een product of component?</h2>
        <p>
          Voor het aanhaken met een nieuw product of component neem je contact
          op met VNG Realisatie.
        </p>

        <StyledContactList>
          <StyledContactList.Item>
            <StyledIconPhone />
            <a href="tel:0703738008">070-373 8008</a>
          </StyledContactList.Item>
          <StyledContactList.Item>
            <StyledIconMail />
            <a href="mailto:realisatie@vng.nl">realisatie@vng.nl</a>
          </StyledContactList.Item>
        </StyledContactList>
      </article>
    </PageTemplate.LayoutCenteredSingleColumn>
  </PageTemplate.ContentContainer>
)

export default ContactPage
