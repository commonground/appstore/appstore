// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'
import { render } from '@testing-library/react'

import theme from '../../theme'
import FiveLayersModelPage from './index'

test('exists', () => {
  const { container } = render(
    <ThemeProvider theme={theme}>
      <Router>
        <FiveLayersModelPage />
      </Router>
    </ThemeProvider>,
  )
  expect(container).toBeTruthy()
})
