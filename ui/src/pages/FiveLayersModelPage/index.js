// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Link } from 'react-router-dom'
import PageTemplate from '../../components/design-system-candidates/PageTemplate'
import { StyledFiveLayersImage } from './index.styles'
import fiveLayersModelImage from './5-lagenmodel.png'

const FiveLayersModelPage = () => (
  <PageTemplate.ContentContainer>
    <PageTemplate.LayoutCenteredSingleColumn>
      <article>
        <h1>5-lagenmodel</h1>
        <p>
          Common Ground software is opgebouwd uit componenten in een
          architectuur op basis van het 5-lagen model. Hiermee creëren we meer
          flexibiliteit, spreiden we risico’s en wordt bovendien voorkomen dat
          een component meer doet dan waar het voor bedoeld is.
        </p>
        <p>
          De onderste drie lagen van het model omvatten de standaardisatie van
          semantiek en syntax van de ontsluiting van de gegevensbronnen, de
          daarbij behorende gestandaardiseerde diensten (APIs) en de
          gemeenschappelijke integratiefunctionaliteit NLX. De bovenste twee
          lagen van het model omvatten de inrichting en ondersteuning van
          processen en interactie met die processen voor burgers, bedrijven en
          medewerkers.
        </p>
        <StyledFiveLayersImage src={fiveLayersModelImage} />

        <h2>De 5 lagen</h2>
        <h3>Interactie-laag</h3>
        <p>Websites en apps</p>
        <p>
          De interactielaag bestaat uit gebruikersinterfaces voor de
          eindgebruiker. Deze eindgebruiker kan een medewerker van de gemeente
          zijn, maar ook een inwoner of ondernemer. Voorbeelden van componenten
          zijn webformulieren (frontends), (mobiele apps) of afhandelomgevingen
        </p>

        <h3>Proces-laag</h3>
        <p>Bedrijfsprocessen</p>
        <p>
          Binnen de proceslaag vallen de dienstverlenings- en bedrijfsprocessen.
          Een proces is een reeks opeenvolgende handelingen, die vanuit een
          bepaalde input tot een beoogde output leiden.
        </p>
        <p>
          Processen zijn te beschrijven met behulp van modelleertalen zoals
          BPMN, en een procescomponenten zijn dan ook implementaties in software
          van (onderdelen van) deze processen.
        </p>
        <p>
          Processen kunnen geïmplementeerd zijn in vele verschillende
          technologieën en programmeertalen, en bestaan over het algemeen uit
          het verwerken van gebruikersinput (interactie-laag), het raadplegen,
          controleren en opslaan van gegevens (lokaal of via NLX bij een andere
          organisatie), en het bijhouden van processtatus (in het werkgeheugen
          op de proces-laag of in persistente gegevensopslag in de onderste
          lagen).
        </p>

        <h3>Integratie-laag</h3>
        <p>Landelijke faciliteit voor uitwisseling van data</p>
        <p>
          Één van de speerpunten van Common Ground is dat we stoppen met het
          kopiëren van gegevens, en in plaats daarvan gegevens raadplegen en
          muteren bij de bron. Dit vereist dat deze bronnen, zowel lokaal als
          landelijk, zeer eenvoudig gebruikt kunnen worden. Om dat mogelijk te
          maken ontwikkelen we de NLX, de landelijke integratiefunctionaliteit
          voor het Common Ground gegevenslandschap.
        </p>
        <p>
          NLX zorgt voor gemakkelijke, veilige verbindingen tussen organisaties.
          Zo kunnen procescomponenten heel gemakkelijk gegevens gebruiken bij
          service-componenten, ook als deze bij een andere organisatie staan.
          Omdat verbinding maken altijd op dezelfde manier plaatsvindt, hoeft
          niet voor iedere koppeling een nieuwe integratie te worden gebouwd,
          maar kan een gegevensbron die eenmaal is ontsloten, volgens open
          standaarden heel gemakkelijk worden hergebruikt.
        </p>
        <p>
          Voorbeelden van componenten in deze laag zijn de NLX Inway en NLX
          Outway, die respectievelijk gebruikt worden om gegevens aan te bieden
          en af te nemen op het NLX netwerk. Ook regelt NLX generieke
          authenticatie en autorisatie, en logging zodat gegevensuitwisseling
          met hele lage beheerslast mogelijk is, en er tegelijk compliancy is
          t.a.v. wet- en regelgeving zoals de AVG.
        </p>
        <p>
          <a href="https://nlx.io">Meer informatie over NLX</a>
        </p>

        <h3>Service-laag</h3>
        <p>Toegang tot data (API’s)</p>
        <p>
          In de servicelaag bevinden zich componenten die API’s aanbieden
          waarmee gegevens uit de data-laag ontsloten worden. Op deze laag
          worden bovendien API-gerelateerde zaken geregeld zoals filtering van
          gegevens (dataminimalisatie), en (technische) validatie van gegevens
          die gemuteerd worden (functionele validatie is over het algemeen juist
          onderdeel van een proces).
        </p>
        <p>
          API’s worden bij voorkeur gestandaardiseerd in de vorm van OpenAPI 3
          specificaties, zodat afnemende componenten garanties hebben over de
          inhoud van API-calls.
        </p>

        <h3>Data-laag</h3>
        <p>Opslag en archivering</p>
        <p>
          De data-laag bevat componenten die gegevens opslaan of archiveren. Het
          kan hierbij gaan om gegevens van landelijke basisregistraties,
          sectorale registraties, gemeentelijke kernregistraties,
          processpecifieke gegevensopslag, et cetera.
        </p>
        <p>
          Uitgangspunt van het gegevenslandschap is dat deze bronnen qua syntax
          en samenhang gestandaardiseerd zijn door de bronhouder. Voor
          gemeentelijke gegevens vindt deze standaardisatie plaats onder regie
          van gemeenten en voor andere registraties door de betreffende
          bronhouder.
        </p>
        <p>
          Voorbeelden van data-componenten zijn databasesystemen zoals Postgres
          en CouchDB, maar ook bestandsopslagsystemen zoals buckets of flat file
          storage.
        </p>

        <h2>Vragen?</h2>
        <p>
          <Link to="/contact">Neem contact op met VNG Realisatie.</Link>
        </p>
      </article>
    </PageTemplate.LayoutCenteredSingleColumn>
  </PageTemplate.ContentContainer>
)

export default FiveLayersModelPage
