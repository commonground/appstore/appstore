// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledFiveLayersImage = styled.img`
  display: block;
  width: 480px;
  max-width: 100%;
`
