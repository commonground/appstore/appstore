// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { LAYER_TYPE_DATA } from '../vocabulary'
import ComponentRepository, {
  componentsByLayerType,
} from './component-repository'

describe('the ComponentRepository', () => {
  describe('creating a component', () => {
    describe('when the creation is successful', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            status: 201,
            json: () => Promise.resolve({}),
          }),
        )
      })

      afterEach(() => global.fetch.mockRestore())

      it('should return an empty object', async () => {
        const result = await ComponentRepository.create()
        await expect(result).toEqual({})
        expect(global.fetch).toHaveBeenCalledWith('/api/components/', {
          body: undefined,
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': undefined,
          },
          method: 'POST',
        })
      })
    })

    describe('with invalid user input', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            status: 400,
          }),
        )
      })

      afterEach(() => global.fetch.mockRestore())

      it('should throw an error', async () => {
        const create = ComponentRepository.create()
        await expect(create).rejects.toEqual(new Error('invalid user input'))

        expect(global.fetch).toHaveBeenCalledWith('/api/components/', {
          body: undefined,
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': undefined,
          },
          method: 'POST',
        })
      })
    })

    describe('when an unexpected error happens', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: false,
          }),
        )
      })

      afterEach(() => global.fetch.mockRestore())

      it('should throw an error', async () => {
        const create = ComponentRepository.create()

        await expect(create).rejects.toEqual(
          new Error('unable to handle the request'),
        )

        expect(global.fetch).toHaveBeenCalledWith('/api/components/', {
          body: undefined,
          credentials: 'same-origin',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': undefined,
          },
          method: 'POST',
        })
      })
    })
  })

  describe('updating a component', () => {
    describe('when the update is successful', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            status: 204,
          }),
        )
      })

      afterEach(() => global.fetch.mockRestore())

      it('should return an empty object', async () => {
        const result = await ComponentRepository.update({
          id: '00000000-0000-0000-0000-000000000000',
        })
        expect(result).toEqual({})
      })
    })

    describe('with invalid user input', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            status: 400,
          }),
        )
      })

      afterEach(() => global.fetch.mockRestore())

      it('should throw an error', async () => {
        const update = ComponentRepository.update({
          id: '00000000-0000-0000-0000-000000000000',
        })
        await expect(update).rejects.toEqual(new Error('invalid user input'))

        expect(global.fetch).toHaveBeenCalledWith(
          '/api/components/00000000-0000-0000-0000-000000000000/',
          {
            body: '{"id":"00000000-0000-0000-0000-000000000000"}',
            credentials: 'same-origin',
            headers: {
              'Content-Type': 'application/json',
              'X-CSRFToken': undefined,
            },
            method: 'PATCH',
          },
        )
      })
    })

    describe('when the authenticated user is not authorized to update the component', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            status: 403,
          }),
        )
      })

      it('should throw an error', async () => {
        const update = ComponentRepository.update({
          id: '00000000-0000-0000-0000-000000000000',
        })
        await expect(update).rejects.toEqual(new Error('forbidden'))

        expect(global.fetch).toHaveBeenCalledWith(
          '/api/components/00000000-0000-0000-0000-000000000000/',
          {
            body: '{"id":"00000000-0000-0000-0000-000000000000"}',
            credentials: 'same-origin',
            headers: {
              'Content-Type': 'application/json',
              'X-CSRFToken': undefined,
            },
            method: 'PATCH',
          },
        )
      })
    })

    describe('when an unexpected error happens', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: false,
          }),
        )
      })

      afterEach(() => global.fetch.mockRestore())

      it('should throw an error', async () => {
        const update = ComponentRepository.update({
          id: '00000000-0000-0000-0000-000000000000',
        })
        await expect(update).rejects.toEqual(
          new Error('unable to handle the request'),
        )

        expect(global.fetch).toHaveBeenCalledWith(
          '/api/components/00000000-0000-0000-0000-000000000000/',
          {
            body: '{"id":"00000000-0000-0000-0000-000000000000"}',
            credentials: 'same-origin',
            headers: {
              'Content-Type': 'application/json',
              'X-CSRFToken': undefined,
            },
            method: 'PATCH',
          },
        )
      })
    })
  })

  describe('getting a component by id', () => {
    describe('when the component exists', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockImplementation(() =>
          Promise.resolve({
            ok: true,
            status: 200,
            json: () =>
              Promise.resolve({
                id: '00000000-0000-0000-0000-000000000000',
                name: 'My First Component',
                description: 'Omschrijving',
                layerType: LAYER_TYPE_DATA,
                productOwnerName: 'Product Owner naam',
                organisationName: 'Organisatie',
                contact: 'Contact informatie',
                repositoryUrl: 'http://www.duck.com',
                reuseType: 'zelf installeren',
                createdAt: '2020-01-31T08:01:50.801000Z',
              }),
          }),
        )
      })

      afterEach(() => global.fetch.mockRestore())

      it('should return the component', async () => {
        const result = await ComponentRepository.getById(
          '00000000-0000-0000-0000-000000000000',
        )

        expect(result).toEqual({
          id: '00000000-0000-0000-0000-000000000000',
          name: 'My First Component',
          description: 'Omschrijving',
          layerType: LAYER_TYPE_DATA,
          productOwnerName: 'Product Owner naam',
          organisationName: 'Organisatie',
          contact: 'Contact informatie',
          repositoryUrl: 'http://www.duck.com',
          reuseType: 'zelf installeren',
          createdAt: '2020-01-31T08:01:50.801000Z',
        })

        expect(global.fetch).toHaveBeenCalledWith(
          '/api/components/00000000-0000-0000-0000-000000000000',
        )
      })
    })
  })
})

describe('filter components by layer type', () => {
  const { arrayContaining, objectContaining } = expect
  const components = [
    { key: 1, layerType: 'test' },
    { key: 2, layerType: 'something' },
    { key: 3 },
  ]

  it('filter with a known layerType', () => {
    const result = componentsByLayerType('test', components)
    expect(result).toHaveLength(1)
    expect(result).toEqual(arrayContaining([objectContaining({ key: 1 })]))
  })

  it('filter with an unknown layerType', () => {
    const result = componentsByLayerType('else', components)
    expect(result).toHaveLength(0)
  })

  it('filter without a layerType', () => {
    const result = componentsByLayerType(null, components)
    expect(result).toHaveLength(0)
  })
})
