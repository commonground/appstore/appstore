// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import Cookies from 'js-cookie'

class ProductRepository {
  static async getAll() {
    const result = await fetch(`/api/products/`)

    if (!result.ok) {
      throw new Error('failed to get all products')
    }

    return result.json()
  }

  static async getLatest() {
    const result = await fetch(`/api/products/?ordering=created_at&limit=6`)

    if (!result.ok) {
      throw new Error('failed to get the most recent products')
    }

    return result.json()
  }

  static async getById(id) {
    const result = await fetch(`/api/products/${id}/`)

    if (!result.ok) {
      throw new Error('failed to get product by id')
    }

    return result.json()
  }

  static async create(product) {
    const result = await fetch('/api/products/', {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': Cookies.get('csrftoken'),
      },
      body: JSON.stringify(product),
    })

    if (result.status === 400) {
      throw new Error('invalid user input')
    }

    if (result.status === 401) {
      throw new Error('unauthorized')
    }

    if (result.status === 403) {
      throw new Error('forbidden')
    }

    if (!result.ok) {
      throw new Error('unable to handle the request')
    }

    return result.json()
  }

  static async update(id, product) {
    const result = await fetch(`/api/products/${id}/`, {
      method: 'PATCH',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': Cookies.get('csrftoken'),
      },
      body: JSON.stringify(product),
    })

    if (result.status === 400) {
      throw new Error('invalid user input')
    }

    if (result.status === 401) {
      throw new Error('unauthorized')
    }

    if (result.status === 403) {
      throw new Error('forbidden')
    }

    if (!result.ok) {
      throw new Error('unable to handle the request')
    }

    return result.json()
  }

  static async addComponent(id, components) {
    const result = await fetch(`/api/products/${id}/add_component/`, {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': Cookies.get('csrftoken'),
      },
      body: JSON.stringify(components.map(({ id }) => ({ id }))),
    })

    if (result.status === 400) {
      throw new Error('invalid user input')
    }

    if (result.status === 401) {
      throw new Error('unauthorized')
    }

    if (result.status === 403) {
      throw new Error('forbidden')
    }

    if (!result.ok) {
      throw new Error('failed to update product')
    }

    return result.json()
  }
}

export default ProductRepository
