// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import huishoudboekjeImage01 from '../pages/SolutionPage/assets/huishoudboekje/S1_huishoudboekje.png'
import huishoudboekjeImage02 from '../pages/SolutionPage/assets/huishoudboekje/S2_huishoudboekje.png'
import huishoudboekjeImage03 from '../pages/SolutionPage/assets/huishoudboekje/S3_huishoudboekje.png'
import huishoudboekjeImage04 from '../pages/SolutionPage/assets/huishoudboekje/S4_huishoudboekje.png'
import huishoudboekjeImage05 from '../pages/SolutionPage/assets/huishoudboekje/S5_huishoudboekje.png'
import signalenImage01 from '../pages/SolutionPage/assets/signalen/S1_signalen.png'
import budgetbeheerImage from '../pages/PortfolioPage/assets/budgetbeheer.jpg'
import budgetbeheerThumbnailImage from '../pages/PortfolioPage/assets/budgetbeheer_thumbnail.jpg'
import meldingenImage from '../pages/PortfolioPage/assets/meldingen.jpg'
import meldingenThumbnailImage from '../pages/PortfolioPage/assets/meldingen_thumbnail.jpg'
import vakantieverhuurImage from '../pages/PortfolioPage/assets/vakantieverhuur.jpg'
import vakantieverhuurThumbnailImage from '../pages/PortfolioPage/assets/vakantieverhuur_thumbnail.jpg'
import {
  IconBouwenEnWonen,
  IconBriefcaseLine,
  IconOpenbareOrdeEnVeiligheid,
} from '../icons'

import { STATUS_GEPLAND, STATUS_BETA } from '../vocabulary'

export const solutions = [
  {
    id: 'huishoudboekje',
    name: 'Huishoudboekje',
    status: STATUS_BETA,
    categoryPath: ['Werk en inkomen', 'Overzicht in Inkomsten en Uitgaven'],
    supplier: 'VNG Realisatie',
    audience: 'Inwoners, Gemeentemedewerkers, incasserende organisaties',
    shortDescription:
      'Een applicatie die inwoners van gemeenten financiële ondersteuning biedt',
    description: `Financiële stabiliteit met één druk op de knop. Dat is de visie van het huishoudboekje. Met het huishoudboekje kunnen inwoners van een gemeente eenvoudiger inkomsten en vaste lasten balanceren. Het helpt deelnemers om periodieke betalingen te structureren en daardoor een buffer op te bouwen. Met deze buffer is het mogelijk om leveranciers garantie op betalingen te geven. Door die garantie kan een leverancier vervolgens een korting aan de deelnemende inwoner bieden.
De applicatie huishoudboekje handelt alle inkomende en uitgaande betalingen af en keert periodiek vast bedrag aan leefgeld aan de deelnemende inwoner uit. Op deze manier krijgen deelnemende inwoners weer grip op hun financiële situatie.
 `,
    features: [
      'De gemeente zorgt dat inkomsten (loon, uitkering, toeslagen) van deelnemers binnenkomen op een derdengeldenrekening.',
      'De gemeente zorgt dat het automatisch betalen van de vaste lasten (huur, energie, water en zorgverzekering) vanaf de derdengeldenrekening gaat lopen.',
      'Deelnemers ontvangen het restant op de eigen rekening. Zij mogen zelf weten hoe zij dit bedrag uitgeven.',
      'Deelnemers kunnen hun financiële gegevens in het Huishoudboekje online inzien met een inlog.',
    ],
    screenshots: [
      {
        src: huishoudboekjeImage01,
        alt: 'Schermafbeelding van Huishoudboekje',
      },
      {
        src: huishoudboekjeImage02,
        alt: 'Schermafbeelding van Huishoudboekje',
      },
      {
        src: huishoudboekjeImage03,
        alt: 'Schermafbeelding van Huishoudboekje',
      },
      {
        src: huishoudboekjeImage04,
        alt: 'Schermafbeelding van Huishoudboekje',
      },
      {
        src: huishoudboekjeImage05,
        alt: 'Schermafbeelding van Huishoudboekje',
      },
    ],
    demoUrl: '',
  },
  {
    id: 'signalen',
    name: 'Signalen',
    status: STATUS_BETA,
    categoryPath: ['Openbare ruimte en veiligheid', 'Melding openbare ruimte'],
    supplier: 'VNG Realisatie',
    audience: 'Inwoners, Gemeentemedewerkers',
    shortDescription:
      'Een applicatie waarmee inwoners zelf een digitale melding maken. Deze meldingen komen automatisch bij de juiste behandelaar terecht.',
    description: `Signalen helpt gemeenten om de tijd tussen het doen van een melding door een inwoner en oplossing van het gemelde probleem zo kort mogelijk te laten zijn.
    `,
    features: [
      'Binnenkomende Meldingen (Openbare Ruimte) worden automatisch gecategoriseerd en gerouteerd naar de juiste behandelaar.',
      'Meldingen komen dus in één keer op de juiste plek terecht. Signalen maakt hierbij gebruik van Machine Learning technologie gecombineerd met historische data.',
      'Signalen is dus het open source proces- en taaksysteem van en voor overheden, dat meldingen over de openbare ruimte automatisch categoriseert en routeert voor afhandeling door de juiste behandelaar',
    ],
    screenshots: [
      {
        src: signalenImage01,
        alt: 'Schermafbeelding van Huishoudboekje',
      },
    ],
    demoUrl: 'https://www.youtube.com/watch?v=AdBNyYwbf8A',
  },
  {
    id: 'vakantieverhuur',
    name: 'Registratie toeristische verhuur woonruimte',
    status: STATUS_GEPLAND,
    categoryPath: [
      'Bouwen en wonen',
      'Registratie toeristische verhuur woonruimte',
    ],
    supplier: 'Gemeente Amsterdam',
    audience: 'Inwoners, Gemeentemedewerkers',
    shortDescription:
      'Een applicatie die verhuurders ondersteunt bij het aanmelden van de woning t.b.v. toeristische verhuur en gemeenten helpt bij de afhandeling en uitvoering van de registratieplicht.',
    description: `Verhuurders kunnen een registratienummer aanvragen als ze hun woning tijdelijk willen verhuren aan toeristen.`,
    features: [
      'Via de applicatie "Registratie Toeristische Verhuur Woonruimte" kan een inwoner een registratienummer aanvragen voor het verhuren van de woning. De verhuurder zal het nummer per direct ontvangen en in zijn advertentie kunnen zetten.',
    ],
    screenshots: [],
    demoUrl: '',
  },
]

export const portfolioItems = [
  {
    id: 'financiele-stabiliteit',
    name: 'Financiële stabiliteit',
    categoryPath: ['Werk en Inkomen'],
    categoryIcon: IconBriefcaseLine,
    imgSrc: budgetbeheerImage,
    imgThumbSrc: budgetbeheerThumbnailImage,
    shortDescription:
      'De gemeente helpt inwoners met geldzorgen om grip te krijgen op hun financiële situatie.',
    description:
      'Inwoners ervaren schommelingen in inkomsten en uitgaven. Inkomsten en uitgaven worden op verschillende momenten gestort en geïncasseerd. Inwoners maken zich zorgen over op het juiste moment genoeg saldo hebben om rekeningen te betalen, zodat geen achterstand in betaling ontstaat.\n' +
      'Een gemeente zorgt ervoor dat inkomsten en uitgaven aan vaste lasten van een inwoner gestabiliseerd worden.',
    solutions: ['huishoudboekje'],
  },
  {
    id: 'melding-openbare-ruimte',
    name: 'Melding openbare ruimte',
    categoryPath: ['Openbare ruimte en veiligheid'],
    categoryIcon: IconOpenbareOrdeEnVeiligheid,
    imgSrc: meldingenImage,
    imgThumbSrc: meldingenThumbnailImage,
    shortDescription:
      'Inwoners kunnen digitaal meldingen doen bij de gemeente.',
    description:
      'Wanneer de openbare ruimte aandacht van de gemeente nodig heeft, kunnen inwoners hiervan een digitale melding maken. Dat kan bijvoorbeeld gaan over bestrating, afval, reiniging, groenvoorziening, een verstopt riool, openbare verlichting of ongediertebestrijding.\n' +
      'Binnen de gemeente wordt de melding automatisch naar de juiste behandelaar doorgezet voor verdere afhandeling.',
    solutions: ['signalen'],
  },
  {
    id: 'registratie-toeristische-verhuur-woonruimte',
    name: 'Registratie toeristische verhuur woonruimte',
    categoryPath: ['Bouwen en wonen'],
    categoryIcon: IconBouwenEnWonen,
    imgSrc: vakantieverhuurImage,
    imgThumbSrc: vakantieverhuurThumbnailImage,
    shortDescription:
      'Inwoners kunnen een registratienummer aanvragen om hun woning te kunnen verhuren.',
    description:
      'In 2021 gaat de landelijke registratieplicht van kracht als onderdeel van de Wet toeristische verhuur. Afhankelijk van de gemeentelijke verordening zijn verhuurders van vakantiewoningen dan verplicht om bij de gemeente een registratienummer voor vakantieverhuur aan te vragen. Deze aanvraag kan alleen digitaal. Na eventuele toekenning is de verhuurder vervolgens verplicht om het registratienummer in advertenties en bij mogelijke verplichte verhuurmeldingen te vermelden.\n' +
      'De gemeente kan registratienummer ook ook gebruiken voor de naleving van veiligheids-, gezondheids- en bruikbaarheidsvoorschriften, en bij het  heffen van toeristenbelasting.',
    solutions: ['vakantieverhuur'],
  },
]

export const getPortfolioItemBySolutionId = (solutionId) =>
  portfolioItems.find((item) => item.solutions.includes(solutionId))
