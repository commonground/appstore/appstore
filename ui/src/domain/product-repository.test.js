// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import ProductRepository from './product-repository'

describe('the ProductRepository', () => {
  describe('getting a product by id', () => {
    beforeEach(() => {
      jest.spyOn(global, 'fetch').mockResolvedValue({
        ok: true,
        status: 200,
        json: () =>
          Promise.resolve({
            id: '00000000-0000-0000-0000-000000000000',
            name: 'My First Product',
          }),
      })
    })

    afterEach(() => global.fetch.mockRestore())

    it('should return the component', async () => {
      const result = await ProductRepository.getById(
        '00000000-0000-0000-0000-000000000000',
      )

      expect(result).toEqual({
        id: '00000000-0000-0000-0000-000000000000',
        name: 'My First Product',
      })

      expect(global.fetch).toHaveBeenCalledWith(
        '/api/products/00000000-0000-0000-0000-000000000000/',
      )
    })
  })

  describe('getting all products', () => {
    beforeEach(() => {
      jest.spyOn(global, 'fetch').mockResolvedValue({
        ok: true,
        status: 200,
        json: () =>
          Promise.resolve([
            {
              id: '00000000-0000-0000-0000-000000000000',
              name: 'My First Product',
            },
          ]),
      })
    })

    afterEach(() => global.fetch.mockRestore())

    it('should return the components', async () => {
      const result = await ProductRepository.getAll()

      expect(result).toEqual([
        {
          id: '00000000-0000-0000-0000-000000000000',
          name: 'My First Product',
        },
      ])

      expect(global.fetch).toHaveBeenCalledWith('/api/products/')
    })
  })

  describe('getting the latest products', () => {
    beforeEach(() => {
      jest.spyOn(global, 'fetch').mockResolvedValue({
        ok: true,
        status: 200,
        json: () =>
          Promise.resolve([
            {
              id: '00000000-0000-0000-0000-000000000000',
              name: 'My First Product',
            },
          ]),
      })
    })

    afterEach(() => global.fetch.mockRestore())

    it('should return the components', async () => {
      const result = await ProductRepository.getLatest()

      expect(result).toEqual([
        {
          id: '00000000-0000-0000-0000-000000000000',
          name: 'My First Product',
        },
      ])

      expect(global.fetch).toHaveBeenCalledWith(
        '/api/products/?ordering=created_at&limit=6',
      )
    })
  })

  describe('updating a product', () => {
    describe('when the update is successful', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch').mockResolvedValue({
          ok: true,
          status: 204,
          json: () => Promise.resolve({}),
        })
      })

      afterEach(() => global.fetch.mockRestore())

      it('should return an empty object', async () => {
        const result = await ProductRepository.update(
          '00000000-0000-0000-0000-000000000000',
          {
            name: '00000000-0000-0000-0000-000000000000',
          },
        )
        expect(result).toEqual({})
      })
    })
  })
})
