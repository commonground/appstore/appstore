// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import Cookies from 'js-cookie'

class ComponentRepository {
  static async getAll() {
    const result = await fetch(`/api/components/`)

    if (!result.ok) {
      throw new Error('failed to get all components')
    }

    return result.json()
  }

  static async getById(id) {
    const result = await fetch(`/api/components/${id}`)

    if (result.status === 404) {
      throw new Error('not found')
    }

    if (!result.ok) {
      throw new Error('unable to handle the request')
    }

    return result.json()
  }

  static async filterComponents({ query, status }) {
    const result = await fetch(
      `/api/components/?search=${query}&status=${status}`,
    )

    if (!result.ok) {
      throw new Error('failed to fetch filtered components')
    }

    return result.json()
  }

  static async create(component) {
    const result = await fetch('/api/components/', {
      method: 'POST',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': Cookies.get('csrftoken'),
      },
      body: JSON.stringify(component),
    })

    if (result.status === 400) {
      throw new Error('invalid user input')
    }

    if (result.status === 401) {
      throw new Error('unauthorized')
    }

    if (result.status === 403) {
      throw new Error('forbidden')
    }

    if (!result.ok) {
      throw new Error('unable to handle the request')
    }

    return result.json()
  }

  static async update(component) {
    const result = await fetch(`/api/components/${component.id}/`, {
      method: 'PATCH',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': Cookies.get('csrftoken'),
      },
      body: JSON.stringify(component),
    })

    if (result.status === 400) {
      throw new Error('invalid user input')
    }

    if (result.status === 403) {
      throw new Error('forbidden')
    }

    if (!result.ok) {
      throw new Error('unable to handle the request')
    }

    return {}
  }
}

export const componentsByLayerType = (layerType, components) =>
  components.filter((component) => component.layerType === layerType)

export default ComponentRepository
