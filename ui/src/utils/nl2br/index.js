// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
// Credits: https://github.com/yosuke-furukawa/react-nl2br
//
// When we have upgraded to react@17, consider updating this util like so:
// https://github.com/yosuke-furukawa/react-nl2br/commit/f69aee2f310de7907d523a6c04c05110aba4333f
//
import { createElement } from 'react'

const newlineRegex = /(\r\n|\r|\n)/g

function nl2br(str) {
  if (typeof str !== 'string') return str

  return str
    .split(newlineRegex)
    .map((line, key) =>
      line.match(newlineRegex) ? createElement('br', { key }) : line,
    )
}

export default nl2br
