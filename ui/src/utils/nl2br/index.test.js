// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'

import nl2br from './index'

test('replace new lines with br element', function () {
  const result = nl2br('aaa\nbbb\nccc\nddd')

  expect(result).toEqual([
    'aaa',
    <br key={1} />,
    'bbb',
    <br key={3} />,
    'ccc',
    <br key={5} />,
    'ddd',
  ])
})

test('passing a react component', function () {
  const component = <p>jsx</p>
  const result = nl2br(component)

  expect(result).toEqual(component)
})

test('passing undefined', function () {
  expect(nl2br(undefined)).toBeUndefined()
})
