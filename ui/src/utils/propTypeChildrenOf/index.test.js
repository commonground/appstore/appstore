// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { checkPropTypes } from 'prop-types'

import childrenOf from './index'

const MyComponent = () => <p>√</p>

test('it passes as intended', () => {
  jest.spyOn(global.console, 'error').mockImplementation(() => {})

  const types = {
    test: childrenOf(MyComponent),
  }

  const props = {
    test: <MyComponent />,
  }

  checkPropTypes(types, props, 'tests')
  expect(console.error).not.toHaveBeenCalled()

  global.console.error.mockRestore()
})

test('it fails as intended', () => {
  jest.spyOn(global.console, 'error').mockImplementation(() => {})

  const types = {
    test: childrenOf(MyComponent),
  }

  const props = {
    test: <div />,
  }

  checkPropTypes(types, props, 'test')
  expect(console.error).toHaveBeenCalled()

  global.console.error.mockRestore()
})
