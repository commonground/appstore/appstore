// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { shape, oneOf, oneOfType, arrayOf } from 'prop-types'

export default function childrenOf(...types) {
  const fieldType = shape({
    type: oneOf(types),
  })

  return oneOfType([fieldType, arrayOf(fieldType)])
}
