// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { userIsProductOwner, userOwnsItem } from './permissions'

const userNotProductOwner = { id: 1, isProductOwner: false }
const userProductOwner = { id: 2, isProductOwner: true }

const item = { id: 1, owner: { id: 2 } }
const itemWithoutOwner = { id: 2, owner: null }

test('userIsProductOwner', () => {
  expect(userIsProductOwner(userNotProductOwner)).toEqual(false)
  expect(userIsProductOwner(userProductOwner)).toEqual(true)
})

test('userOwnsItem', () => {
  expect(userOwnsItem(userNotProductOwner, item)).toEqual(false)
  expect(userOwnsItem(userProductOwner, item)).toEqual(true)
  expect(userOwnsItem(userProductOwner, itemWithoutOwner)).toEqual(false)
})
