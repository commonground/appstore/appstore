// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components/macro'

import theme from '../../theme'
import ProductVisibility from './index'

describe('viewing a published component', () => {
  const publishedDummyProduct = {
    id: 43,
    name: 'Verhuizen',
    detailPageImageUrl: 'http://example.com/verhuizen.png',
    shortDescription: 'Verhuis je inwoner',
    isPublished: true,
  }

  it('shows the visiblity text "only for you"', () => {
    const renderResult = render(
      <ThemeProvider theme={theme}>
        <ProductVisibility product={publishedDummyProduct} />
      </ThemeProvider>,
    )

    expect(renderResult.getByTestId('text-visibility-public')).toBeTruthy()
  })
})

describe('viewing an unpublished component', () => {
  const unpublishedDummyProduct = {
    id: 43,
    name: 'Verhuizen',
    detailPageImageUrl: 'http://example.com/verhuizen.png',
    shortDescription: 'Verhuis je inwoner',
    isPublished: false,
  }

  it('shows the visiblity text "public"', () => {
    const renderResult = render(
      <ThemeProvider theme={theme}>
        <ProductVisibility product={unpublishedDummyProduct} />
      </ThemeProvider>,
    )

    expect(
      renderResult.getByTestId('text-visibility-only-for-you'),
    ).toBeTruthy()
  })
})
