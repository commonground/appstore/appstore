// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'

export const StyledVisibilitySection = styled.p`
  display: flex;
  align-items: center;
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray700};
`

export const StyledVisibilityText = styled.span`
  margin-left: ${(p) => p.theme.tokens.spacing03};
`
