// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'

import { IconEye, IconEyeOff } from '../../icons'

import { StyledVisibilitySection, StyledVisibilityText } from './index.styles'

const ProductVisibility = ({ product }) => {
  return (
    <StyledVisibilitySection>
      {product.isPublished ? (
        <>
          <IconEye />
          <StyledVisibilityText data-testid="text-visibility-public">
            Zichtbaarheid: <b>Publiek</b>
          </StyledVisibilityText>
        </>
      ) : (
        <>
          <IconEyeOff />
          <StyledVisibilityText data-testid="text-visibility-only-for-you">
            Zichtbaarheid: <b>Alleen zichtbaar voor jou</b>
          </StyledVisibilityText>
        </>
      )}
    </StyledVisibilitySection>
  )
}

ProductVisibility.propTypes = {
  product: object.isRequired,
}

export default ProductVisibility
