// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { node, bool } from 'prop-types'
import styled from 'styled-components/macro'

const H1Title = styled.h1`
  margin: 0 0 ${(p) => p.theme.tokens.spacing02} 0;
  .name {
    margin-right: ${(p) => p.theme.tokens.spacing06};
  }
`

const H3Title = styled.h3`
  margin: 0 0 ${(p) => p.theme.tokens.spacing02} 0;
  .name {
    margin-right: ${(p) => p.theme.tokens.spacing06};
  }
`
export const Title = ({ preview, children, ...props }) =>
  preview ? (
    <H3Title {...props}>{children}</H3Title>
  ) : (
    <H1Title {...props}>{children}</H1Title>
  )

Title.propTypes = {
  preview: bool,
  children: node,
}

export const Subtitle = styled.p`
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray700};
  margin: 0 0 ${(p) => p.theme.tokens.spacing06} 0;

  span:first-child {
    padding-right: ${(p) => p.theme.tokens.spacing03};
  }

  span:last-child {
    padding-left: ${(p) => p.theme.tokens.spacing03};
  }
`
