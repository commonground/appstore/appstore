// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'

import nl2br from '../../utils/nl2br'
import {
  StyledProductContainer,
  StyledProductContent,
  StyledProductActions,
  StyledProductDescription,
  StyledProductImage,
  ExternalLink,
  StyledIconExternalLink,
  StyledIconFile,
  StyledIconPlay,
} from './index.styles'

const ProductInfo = ({ product }) => (
  <StyledProductContainer>
    <StyledProductContent width={[1, 2 / 3]}>
      <StyledProductDescription>
        {nl2br(product.description)}
      </StyledProductDescription>

      {product.detailPageImageUrl && (
        <>
          <h2>Screenshot</h2>
          <StyledProductImage
            src={product.detailPageImageUrl}
            alt={`Screenshot van het product ${product.name}`}
          />
        </>
      )}
    </StyledProductContent>

    <StyledProductActions width={[1, 1 / 3]}>
      {product.documentationUrl && (
        <ExternalLink
          href={product.documentationUrl}
          target="_blank"
          rel="noreferrer noopener"
        >
          <span>
            <StyledIconFile />
            Documentatie
          </span>
          <StyledIconExternalLink />
        </ExternalLink>
      )}
      {product.demoUrl && (
        <ExternalLink
          href={product.demoUrl}
          target="_blank"
          rel="noreferrer noopener"
        >
          <span>
            <StyledIconPlay />
            Demo
          </span>
          <StyledIconExternalLink />
        </ExternalLink>
      )}
    </StyledProductActions>
  </StyledProductContainer>
)

ProductInfo.propTypes = {
  product: object.isRequired,
}

ProductInfo.defaultProps = {
  product: {},
}

export default ProductInfo
