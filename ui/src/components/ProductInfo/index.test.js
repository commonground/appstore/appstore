// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'

import { ThemeProvider } from 'styled-components/macro'
import theme from '../../theme'
import ProductInfo from './index'

const dummyProduct = {
  id: '43',
  name: 'Verhuizen',
  detailPageImageUrl: 'http://example.com/verhuizen.png',
  shortDescription: 'Verhuis je inwoner',
  isPublished: true,
}

test('exists', () => {
  const { container } = render(
    <ThemeProvider theme={theme}>
      <ProductInfo product={dummyProduct} />
    </ThemeProvider>,
  )
  expect(container).toBeTruthy()
})
