// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Flex, Box } from 'reflexbox/styled-components'
import { mediaQueries } from '@commonground/design-system'

import { IconFile, IconPlay, IconExternalLink } from '../../icons'

export const StyledProductContainer = styled(Flex)`
  justify-content: space-between;
  flex-wrap: wrap;
`

export const StyledProductContent = styled(Box)`
  ${mediaQueries.smUp`
    padding-right: ${(p) => p.theme.tokens.spacing10};
  `}
`

export const StyledProductActions = styled(Box)``

export const StyledProductDescription = styled.div`
  margin-bottom: ${(p) => p.theme.tokens.spacing08};
`

export const StyledProductImage = styled.img`
  display: block;
  border: 1px solid ${(p) => p.theme.tokens.colorPaletteGray400};
  max-width: 100%;
  max-height: 450px;
  margin: ${(p) => p.theme.tokens.spacing06} 0;
`

export const ExternalLink = styled.a`
  display: flex;
  justify-content: space-between;
  width: 100%;
  font-family: 'Source Sans Pro', sans-serif;
  font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};
  text-decoration: none;
  color: ${(p) => p.theme.tokens.colors.colorPaletteBlue800};
  margin: ${(p) => p.theme.tokens.spacing06} 0;
`

export const StyledIconExternalLink = styled(IconExternalLink)`
  fill: ${(p) => p.theme.tokens.colorPaletteGray600};
`

export const StyledIconFile = styled(IconFile)`
  float: left;
  width: 19px;
  height: 20px;
  margin-top: 1px;
  margin-right: ${(p) => p.theme.tokens.spacing04};
`

export const StyledIconPlay = styled(IconPlay)`
  float: left;
  margin-right: ${(p) => p.theme.tokens.spacing04};
  margin-left: -5px; /* Fix different alignments in file and play svg's */
`
