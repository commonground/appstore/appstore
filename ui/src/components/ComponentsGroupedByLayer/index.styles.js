// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'

export const StyledSeparator = styled.hr`
  display: block;
  height: 1px;
  background: ${(p) => p.theme.tokens.colors.colorPaletteGray300};
  margin: ${(p) => p.theme.tokens.spacing05} 0;
  border: 0 none;
`
