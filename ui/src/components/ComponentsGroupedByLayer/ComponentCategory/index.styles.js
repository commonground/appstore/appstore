// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Link } from 'react-router-dom'
import Color from 'color'

import { mediaQueries } from '@commonground/design-system'
import { componentStatus, STATUS_BRUIKBAAR } from '../../../vocabulary'
import { CardView, ListView } from '../../ProductList/index.styles'
import { Card } from '../../ProductCard/index.styles'

export const StyledComponentCategory = styled.div`
  padding-top: ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.smUp`
    border-left: 20px solid ${(p) => p.theme.tokens.colors.brandSecondary1};
    padding: ${(p) => p.theme.tokens.spacing05} 0
      ${(p) => p.theme.tokens.spacing02} ${(p) => p.theme.tokens.spacing07};
  `}
`

export const StyledTitle = styled.h2`
  margin: 0 0 ${(p) => p.theme.tokens.spacing02} 0;
`

export const StyledInfo = styled.p`
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray700};
  margin: 0 0 ${(p) => p.theme.tokens.spacing07} 0;

  span:first-child {
    padding-right: ${(p) => p.theme.tokens.spacing03};
  }

  span:last-child {
    padding-left: ${(p) => p.theme.tokens.spacing03};
  }
`

export const StyledAddComponentMenu = styled.div`
  display: inline-block;
  float: right;
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray600};
  margin-top: ${(p) => p.theme.tokens.spacing02};
  span {
    margin-right: ${(p) => p.theme.tokens.spacing03};
  }
  a {
    margin-left: ${(p) => p.theme.tokens.spacing05};
  }
  ${mediaQueries.smDown`
    min-width: 100%;
  `}
`

export const StyledAddComponentWrapper = styled.div`
  display: inline-flex;
  align-items: center;
  padding: ${(p) => p.theme.tokens.spacing06};
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray600};
  border: 1px dashed ${(p) => p.theme.tokens.colors.colorPaletteGray400};
  text-align: left;
  line-height: ${(p) => p.theme.tokens.lineHeightText};
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-bottom: ${(p) => p.theme.tokens.spacing04};
`

export const StyledAddComponentPlus = styled.div`
  flex-shink: 0;
  height: 100%;
  padding-right: ${(p) => p.theme.tokens.spacing05};
  font-size: 2.5rem;
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray500};
`

export const StyledAddComponentBody = styled.div`
  flex-grow: 1;
  a + a {
    margin-left: ${(p) => p.theme.tokens.spacing07};
  }
`

export const StyledComponentRow = styled(ListView)`
  justify-content: ${(p) => (p.centered ? 'center' : 'start')};
`

export const StyledCardView = styled(CardView)`
  ${mediaQueries.smDown`
    min-width: 100%;
  `}
`

export const StyledComponentCard = styled(Card)`
  height: calc(
    5rem + ${(p) => p.theme.tokens.spacing04} +
      ${(p) => p.theme.tokens.spacing05}
  );
  padding: ${(p) => p.theme.tokens.spacing04} ${(p) => p.theme.tokens.spacing05};
`

StyledComponentCard.Title = styled.h2`
  margin: 0;
  color: ${(p) => p.theme.tokens.colors.colorTextLink};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
`

StyledComponentCard.Organisation = styled.div`
  min-height: 1.5rem; // this is equivalent for the line-height
  color: ${(p) => p.theme.tokens.colors.colorText};
  margin-bottom: ${(p) => p.theme.tokens.spacing03};
`

export const StyledComponentButton = styled(Link)`
  display: inline-block;
  background-color: ${(p) => p.theme.tokens.colors.colorPaletteGray100};
  box-shadow: inset 0 -3px 0 ${(p) => Color(p.theme.tokens.colors.colorPaletteGray900).alpha(0.25).hsl().string()};
  ${(p) => {
    if (p.status !== STATUS_BRUIKBAAR) {
      return `border-right: ${p.theme.tokens.spacing03} solid ${Color(
        componentStatus[p.status].color,
      )
        .hsl()
        .string()};`
    }
  }}
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  padding: ${(p) => p.theme.tokens.spacing05};
  line-height: ${(p) => p.theme.tokens.fontSizeMedium};
  text-decoration: none;
  color: ${(p) => p.theme.tokens.colors.colorPaletteBlue800};
  font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};
  margin-bottom: ${(p) => p.theme.tokens.spacing04};

  &:not(:last-child) {
    margin-right: ${(p) => p.theme.tokens.spacing05};
  }

  &:hover {
    background: ${(p) => p.theme.tokens.colors.colorPaletteGray200};
  }
`
