// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'
import { render } from '@testing-library/react'
import { LAYER_TYPE_INTERACTIE, STATUS_GEWENST } from '../../../vocabulary'
import { UserContextProvider } from '../../../user-context'
import { ProductContext } from '../../../pages/ProductPage'
import ComponentCategory from './index'

const user = { id: 1 }
const productContext = { product: { owner: { id: 1 } } }

test('the category info and components', () => {
  const { getByTestId, getAllByTestId } = render(
    <UserContextProvider user={{}}>
      <Router>
        <ComponentCategory
          name="Interactie-laag"
          description="Websites & apps"
          layerType={LAYER_TYPE_INTERACTIE}
          components={[
            {
              id: 42,
              name: 'My First Component',
              status: STATUS_GEWENST,
              layerType: LAYER_TYPE_INTERACTIE,
            },
          ]}
        />
      </Router>
    </UserContextProvider>,
  )

  expect(getByTestId('title')).toHaveTextContent('Interactie-laag')
  expect(getByTestId('description')).toHaveTextContent('Websites & apps')
  expect(getByTestId('amount')).toHaveTextContent('1 componenten')

  expect(getAllByTestId('component')).toHaveLength(1)

  const component = getByTestId('component')
  expect(component).toHaveTextContent('My First Component')
  expect(component.getAttribute('href')).toBe('//42')
})

test('authenticated user is the owner of a product with components', () => {
  const { container, queryByTestId } = render(
    <UserContextProvider user={user}>
      <ProductContext.Provider value={productContext}>
        <Router>
          <ComponentCategory
            name="Interactie-laag"
            description="Websites & apps"
            layerType={LAYER_TYPE_INTERACTIE}
            components={[
              {
                id: 42,
                name: 'My First Component',
                status: STATUS_GEWENST,
                layerType: LAYER_TYPE_INTERACTIE,
              },
            ]}
          />
        </Router>
      </ProductContext.Provider>
    </UserContextProvider>,
  )

  expect(container.querySelectorAll('a')).toHaveLength(3)
  expect(queryByTestId('add-component-menu')).toBeTruthy()
  expect(queryByTestId('add-component-button')).toBeFalsy()
})

test('authenticated user is the owner of a product without any components', () => {
  const { container, queryByTestId } = render(
    <UserContextProvider user={user}>
      <ProductContext.Provider value={productContext}>
        <Router>
          <ComponentCategory
            name="Interactie-laag"
            description="Websites & apps"
            layerType={LAYER_TYPE_INTERACTIE}
            components={[]}
          />
        </Router>
      </ProductContext.Provider>
    </UserContextProvider>,
  )

  expect(container.querySelectorAll('a')).toHaveLength(2)
  expect(queryByTestId('add-component-button')).toBeTruthy()
  expect(queryByTestId('add-component-menu')).toBeFalsy()
})

test('show cards in product context', () => {
  const { queryAllByTestId } = render(
    <UserContextProvider user={{}}>
      <ProductContext.Provider value={productContext}>
        <Router>
          <ComponentCategory
            name="Interactie-laag"
            description="Websites & apps"
            layerType={LAYER_TYPE_INTERACTIE}
            components={[
              {
                id: 42,
                name: 'My First Component',
                status: STATUS_GEWENST,
                layerType: LAYER_TYPE_INTERACTIE,
              },
            ]}
          />
        </Router>
      </ProductContext.Provider>
    </UserContextProvider>,
  )

  expect(queryAllByTestId('component-card')).toHaveLength(1)
  expect(queryAllByTestId('component')).toHaveLength(0)
})
