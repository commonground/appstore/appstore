// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useContext } from 'react'
import { withRouter, Route, Link } from 'react-router-dom'
import { string, number, shape, arrayOf, oneOf } from 'prop-types'
import { ThemeProvider } from 'styled-components/macro'

import AddComponentPage from '../../../pages/AddComponentPage'
import ComponentSelectPane from '../../../components/ComponentSelectPane'
import theme from '../../../theme'
import { LAYER_TYPES, STATUSES } from '../../../vocabulary'
import { ProductContext } from '../../../pages/ProductPage'
import ComponentStatus from '../../ComponentStatus'
import { componentsByLayerType } from '../../../domain/component-repository'
import UserContext from '../../../user-context'
import { userOwnsItem } from '../../../permissions'
import {
  StyledAddComponentBody,
  StyledAddComponentMenu,
  StyledAddComponentPlus,
  StyledAddComponentWrapper,
  StyledCardView,
  StyledComponentButton,
  StyledComponentCard,
  StyledComponentCategory,
  StyledComponentRow,
  StyledInfo,
  StyledTitle,
} from './index.styles'

const ComponentCategory = ({
  match,
  name,
  layerType,
  description,
  components,
  ...props
}) => {
  const parentUrl = match.url
  const { product } = useContext(ProductContext)
  const { user } = useContext(UserContext)
  const layerComponents = componentsByLayerType(layerType, components)
  const showAddComponent = userOwnsItem(user, product)

  return (
    <ThemeProvider theme={theme}>
      <StyledComponentCategory {...props}>
        {showAddComponent && layerComponents.length > 0 && (
          <StyledAddComponentMenu data-testid="add-component-menu">
            <span>Component toevoegen:</span>
            <Link to={`${parentUrl}/${layerType}/nieuw`}>Nieuw</Link>
            <Link to={`${parentUrl}/${layerType}/kies`}>Kies bestaand</Link>
          </StyledAddComponentMenu>
        )}

        <StyledTitle data-testid="title">{name}</StyledTitle>
        <StyledInfo>
          <span data-testid="description">{description}</span> ·{' '}
          <span data-testid="amount">{layerComponents.length} componenten</span>
        </StyledInfo>

        <StyledComponentRow centered={!!product}>
          {showAddComponent && layerComponents.length === 0 && (
            <StyledAddComponentWrapper data-testid="add-component-button">
              <StyledAddComponentPlus>+</StyledAddComponentPlus>
              <StyledAddComponentBody>
                <div>Component toevoegen</div>
                <div>
                  <Link to={`${parentUrl}/${layerType}/nieuw`}>Nieuw</Link>
                  <Link to={`${parentUrl}/${layerType}/kies`}>
                    Kies bestaand
                  </Link>
                </div>
              </StyledAddComponentBody>
            </StyledAddComponentWrapper>
          )}

          {layerComponents.map(({ id, name, status, organisationName }) =>
            product ? (
              <StyledCardView key={id}>
                <StyledComponentCard
                  to={`${parentUrl}/${id}`}
                  data-testid="component-card"
                >
                  <StyledComponentCard.Title>{name}</StyledComponentCard.Title>
                  <StyledComponentCard.Organisation>
                    {organisationName}
                  </StyledComponentCard.Organisation>
                  <ComponentStatus status={status} />
                </StyledComponentCard>
              </StyledCardView>
            ) : (
              <StyledComponentButton
                key={id}
                to={`${parentUrl}/${id}`}
                data-testid="component"
                status={status}
              >
                {name}
              </StyledComponentButton>
            ),
          )}
        </StyledComponentRow>
        {showAddComponent && (
          <>
            <Route
              path={`${parentUrl}/${layerType}/kies`}
              component={() => (
                <ComponentSelectPane
                  parentUrl={parentUrl}
                  name={name}
                  layerType={layerType}
                />
              )}
            />
            <Route
              path={`${parentUrl}/${layerType}/nieuw`}
              component={() => (
                <AddComponentPage parentUrl={parentUrl} layerType={layerType} />
              )}
            />
          </>
        )}
      </StyledComponentCategory>
    </ThemeProvider>
  )
}

ComponentCategory.propTypes = {
  match: shape({
    url: string,
    params: shape({
      id: string,
    }),
  }),
  name: string.isRequired,
  layerType: oneOf(LAYER_TYPES).isRequired,
  description: string.isRequired,
  components: arrayOf(
    shape({
      id: number.isRequired,
      name: string.isRequired,
      status: oneOf(STATUSES),
      layerType: oneOf(LAYER_TYPES).isRequired,
    }).isRequired,
  ).isRequired,
}

export default withRouter(ComponentCategory)
