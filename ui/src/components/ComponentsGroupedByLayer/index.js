// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { arrayOf, number, object, oneOf, shape, string } from 'prop-types'
import { ThemeProvider } from 'styled-components/macro'
import {
  LAYER_TYPE_DATA,
  LAYER_TYPE_INTEGRATIE,
  LAYER_TYPE_INTERACTIE,
  LAYER_TYPE_PROCES,
  LAYER_TYPE_SERVICES,
  LAYER_TYPES,
} from '../../vocabulary'
import theme from '../../theme'
import ComponentCategory from './ComponentCategory'
import { StyledSeparator } from './index.styles'

const ComponentsGroupedByLayer = ({ components, ...props }) => (
  <ThemeProvider theme={theme}>
    <div {...props}>
      <ComponentCategory
        data-testid="interactie-category"
        name="Interactie-laag"
        layerType={LAYER_TYPE_INTERACTIE}
        description="Websites & apps"
        components={components}
      />
      <StyledSeparator />

      <ComponentCategory
        data-testid="proces-category"
        name="Proces-laag"
        layerType={LAYER_TYPE_PROCES}
        description="Bedrijfsprocessen"
        components={components}
      />
      <StyledSeparator />

      <ComponentCategory
        data-testid="integratie-category"
        name="Integratie-laag"
        layerType={LAYER_TYPE_INTEGRATIE}
        description="Landelijke faciliteit voor uitwisselen van data"
        components={components}
      />
      <StyledSeparator />

      <ComponentCategory
        data-testid="services-category"
        name="Service-laag"
        layerType={LAYER_TYPE_SERVICES}
        description="Toegang tot data (API's)"
        components={components}
      />
      <StyledSeparator />

      <ComponentCategory
        data-testid="data-category"
        name="Data-laag"
        layerType={LAYER_TYPE_DATA}
        description="Opslag en archivering"
        components={components}
      />
    </div>
  </ThemeProvider>
)

ComponentsGroupedByLayer.propTypes = {
  components: arrayOf(
    shape({
      id: number.isRequired,
      name: string.isRequired,
      layerType: oneOf(LAYER_TYPES).isRequired,
    }).isRequired,
  ).isRequired,
  product: object,
}

export default ComponentsGroupedByLayer
