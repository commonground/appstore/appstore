// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'
import { render } from '@testing-library/react'
import { queryAllByTestId } from '@testing-library/dom'
import {
  LAYER_TYPE_INTERACTIE,
  LAYER_TYPE_PROCES,
  STATUS_GEWENST,
} from '../../vocabulary'
import { UserContextProvider } from '../../user-context'
import ComponentsGroupedByLayer from './index'

test('all layer types with their components', () => {
  const components = [
    {
      id: 1,
      name: 'My First Component',
      status: STATUS_GEWENST,
      layerType: LAYER_TYPE_INTERACTIE,
    },
    {
      id: 2,
      name: 'My Second Component',
      status: STATUS_GEWENST,
      layerType: LAYER_TYPE_PROCES,
    },
  ]

  const { getByTestId } = render(
    <UserContextProvider user={{ id: 42 }}>
      <Router>
        <ComponentsGroupedByLayer components={components} />
      </Router>
    </UserContextProvider>,
  )

  expect(getByTestId('interactie-category')).toBeTruthy()
  expect(getByTestId('proces-category')).toBeTruthy()
  expect(getByTestId('integratie-category')).toBeTruthy()
  expect(getByTestId('services-category')).toBeTruthy()
  expect(getByTestId('data-category')).toBeTruthy()

  expect(
    queryAllByTestId(getByTestId('interactie-category'), 'component'),
  ).toHaveLength(1)
  expect(
    queryAllByTestId(getByTestId('proces-category'), 'component'),
  ).toHaveLength(1)
  expect(
    queryAllByTestId(getByTestId('integratie-category'), 'component'),
  ).toHaveLength(0)
  expect(
    queryAllByTestId(getByTestId('services-category'), 'component'),
  ).toHaveLength(0)
  expect(
    queryAllByTestId(getByTestId('data-category'), 'component'),
  ).toHaveLength(0)
})
