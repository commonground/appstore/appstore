// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { createGlobalStyle } from 'styled-components/macro'
import '@fontsource/source-sans-pro/latin.css'

export default createGlobalStyle`
  body {
    word-wrap: break-word;
    word-break: break-word;
  }

  html,
  body,
  #root {
    height: 100%;
  }

  nav {
    flex-shrink: 0;
  }

  #root {
    display: flex;
    flex-direction: column;
  }
`
