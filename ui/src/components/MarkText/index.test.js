// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'

import MarkText from './index'

test('Marks matched text', () => {
  const { container, getAllByText } = render(
    <MarkText search="da">Yada blaDa</MarkText>,
  )

  const marked = getAllByText(/da/i)

  expect(container.textContent).toBe('Yada blaDa')
  expect(marked).toHaveLength(2)
  expect(marked[0].nodeName.toLowerCase()).toBe('mark')
})
