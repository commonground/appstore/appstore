// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'

// Prevent user from entering regular expressions
// This allows us to disable `security/detect-non-literal-regexp`
function escapeRegEx(string) {
  // eslint-disable-next-line no-useless-escape
  return string.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&')
}

const MarkText = ({ search, children }) => {
  // We escape regular expressions, so:
  // eslint-disable-next-line security/detect-non-literal-regexp
  const regex = new RegExp(escapeRegEx(search), 'gi')

  return (
    <span
      dangerouslySetInnerHTML={{
        __html: children
          .toString()
          .replace(regex, (text) => `<mark>${text}</mark>`),
      }}
    />
  )
}

MarkText.propTypes = {
  search: string.isRequired,
  children: string.isRequired,
}

export default MarkText
