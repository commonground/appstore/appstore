// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { oneOf } from 'prop-types'
import { STATUS_BRUIKBAAR, STATUSES } from '../../vocabulary'
import { StyledStatus } from './index.styles'

const ComponentStatus = ({ status }) =>
  status !== STATUS_BRUIKBAAR && (
    <StyledStatus type={status} data-testid="status">
      {status}
    </StyledStatus>
  )

ComponentStatus.propTypes = {
  status: oneOf(STATUSES).isRequired,
}

export default ComponentStatus
