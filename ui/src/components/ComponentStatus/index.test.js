// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { renderWithProviders } from '../../test-utils'
import { STATUS_BRUIKBAAR, STATUS_GEWENST } from '../../vocabulary'
import ComponentStatus from './index'

test('known status', () => {
  const { queryByTestId } = renderWithProviders(
    <ComponentStatus status={STATUS_GEWENST} />,
  )

  expect(queryByTestId('status')).toBeTruthy()
})
test('status that should be hidden', () => {
  const { queryByTestId } = renderWithProviders(
    <ComponentStatus status={STATUS_BRUIKBAAR} />,
  )

  expect(queryByTestId('status')).toBeFalsy()
})
