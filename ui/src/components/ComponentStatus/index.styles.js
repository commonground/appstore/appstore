// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { componentStatus } from '../../vocabulary'

export const StyledStatus = styled.span`
  display: inline-block;
  vertical-align: ${(p) =>
    p.preview
      ? 'middle'
      : `calc(${p.theme.tokens.spacing03} - ${p.theme.tokens.spacing01})`};
  text-transform: uppercase;
  color: ${(p) => componentStatus[p.type].textColor};
  background-color: ${(p) => componentStatus[p.type].color};
  padding: ${(p) => p.theme.tokens.spacing01} ${(p) => p.theme.tokens.spacing04};
  line-height: ${(p) => p.theme.tokens.lineHeightText};
  font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
`
