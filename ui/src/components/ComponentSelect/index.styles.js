// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Button } from '@commonground/design-system'

import theme from '../../theme'
import {
  selectStyles as baseSelectStyles,
  option,
} from '../design-system-candidates/Select/index.styles'

export const StyledOptionName = styled.span`
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};

  mark {
    font-weight: ${(p) => p.theme.tokens.fontWeightBold};
    background-color: inherit;
    color: inherit;
  }
`
export const StyledOptionOrganisation = styled.span`
  float: right;
  margin-left: ${(p) => p.theme.tokens.spacing06};
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  color: ${(p) => p.theme.tokens.colors.colorTextLabel};
`

export const Label = styled.label`
  display: block;
  word-break: keep-all;
  margin-bottom: ${(p) => p.theme.tokens.spacing01};
  margin-top: ${(p) => p.theme.tokens.spacing04};
`

export const Fieldset = styled.fieldset`
  border: 0 none;
  padding: 0;
`

export const StyledComponentDetailsWrapper = styled.div`
  margin: ${(p) => p.theme.tokens.spacing06} 0;
  padding: ${(p) => p.theme.tokens.spacing06};
  background: ${(p) => p.theme.tokens.colors.brandSecondary1};
  hr {
    background: ${(p) => p.theme.tokens.colors.colorPaletteGray400};
  }
`

export const StyledButton = styled(Button)`
  margin-top: ${(p) => p.theme.tokens.spacing06};
`

function getIsSelected(state) {
  if (state.getValue().length && state.getValue()[0].id === state.data.id) {
    return 'active'
  }
}

export const componentSelectStyles = {
  ...baseSelectStyles,
  option: (provided, state) => {
    const { isFocused } = state

    return {
      ...option(provided, state),
      backgroundColor: getIsSelected(state)
        ? theme.tokens.colors.colorPaletteGray200
        : isFocused
        ? theme.tokens.colors.colorPaletteGray100
        : theme.tokens.colors.colorBackground,
    }
  },
  indicatorsContainer: () => ({
    display: 'none',
  }),
}
