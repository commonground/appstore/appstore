// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React, { useContext, useEffect, useState, useRef } from 'react'
import { useLocation } from 'react-router-dom'
import { func, node, object, oneOf, shape, string } from 'prop-types'
import { ThemeProvider } from 'styled-components/macro'
import { useDebounce } from 'use-debounce'
import { components } from 'react-select'
import { Formik, Field } from 'formik'

import MarkText from '../MarkText'
import { Subtitle, Title } from '../Elements'
import ComponentDetails from '../ComponentDetails'
import { LAYER_TYPES } from '../../vocabulary'
import theme from '../../theme'
import ComponentRepository from '../../domain/component-repository'
import FieldValidationMessage from '../design-system-candidates/FieldValidationMessage'
import ProductRepository from '../../domain/product-repository'
import Alert from '../design-system-candidates/Alert'
import { FormikSelect } from '../design-system-candidates/Select'
import errorToUserFriendlyMessage from '../../pages/AddComponentPage/error-to-user-friendly-message'
import { ProductContext } from '../../pages/ProductPage'
import {
  Fieldset,
  Label,
  componentSelectStyles,
  StyledComponentDetailsWrapper,
  StyledOptionName,
  StyledOptionOrganisation,
  StyledButton,
} from './index.styles'

const MAX_SHOW_OPTIONS = 5

const SingleValue = ({ data, children, ...props }) => {
  return (
    <components.SingleValue {...props}>
      <StyledOptionName>{data.name}</StyledOptionName>
    </components.SingleValue>
  )
}

SingleValue.propTypes = {
  data: object.isRequired,
  children: node,
}

const FormatOptionLabel = ({ value, query }) => (
  <>
    <StyledOptionName data-testid="component-name" className="component-name">
      {query ? <MarkText search={query}>{value.name}</MarkText> : value.name}
    </StyledOptionName>
    <StyledOptionOrganisation className="organisation-name">
      {value.organisationName}
    </StyledOptionOrganisation>
  </>
)

FormatOptionLabel.propTypes = {
  value: shape({
    name: string,
    organisationName: string,
  }),
  query: string,
}

const useLocationQuery = (param, delay) => {
  const location = useLocation()
  const urlParams = new URLSearchParams(location.search)
  const initialSearchQuery = urlParams.get(param) || null
  const [query, setQuery] = useState(initialSearchQuery)
  const [debouncedQuery] = useDebounce(query, delay)
  return [debouncedQuery, setQuery]
}

const byLayerType = (layerType) => (component) =>
  component.layerType === layerType

const ComponentSelect = ({
  name,
  layerType,
  searchHandler,
  addComponentHandler,
  onSelectHandler,
}) => {
  const [query, setQuery] = useLocationQuery('q', 150)
  const [options, setOptions] = useState([])
  const [component, setComponent] = useState(null)
  const [errorMessage, setErrorMessage] = useState(null)
  const { product, setProduct } = useContext(ProductContext)
  const layerComponentIds = product.components
    .filter(byLayerType(layerType))
    .map((component) => component.id)
  const selectRef = useRef()

  useEffect(() => {
    const searchComponents = async (query) => {
      window.history.pushState({}, null, `?q=${encodeURIComponent(query)}`)
      const result = await searchHandler({ query: query, status: '' })
      setOptions(
        result
          .filter(byLayerType(layerType))
          .filter((component) => !layerComponentIds.includes(component.id))
          .slice(0, MAX_SHOW_OPTIONS),
      )
    }
    // Only search when the query is empty or filled, not when it has not been initialized. This means that
    // emptying (query === '') the query will fetch all components, and starting the form without (query === null) a
    // query will not fetch anything.
    if (query !== null) {
      searchComponents(query)
    }
  }, [query]) // eslint-disable-line react-hooks/exhaustive-deps

  const onSubmitHandler = async () => {
    if (!component) {
      throw new Error('Selecteer eerst een component')
    }

    try {
      const updatedProduct = await addComponentHandler(product.id, [component])
      setTimeout(() => setProduct(updatedProduct), 150)
      onSelectHandler()
    } catch (e) {
      setErrorMessage(errorToUserFriendlyMessage(e))
    }
  }

  const inputChange = (value, action) => {
    if (action && action.action === 'input-change') {
      setQuery(value)
    }
  }

  return (
    <ThemeProvider theme={theme}>
      <Title data-testid="heading">Component toevoegen</Title>
      <Subtitle>{name}</Subtitle>

      {errorMessage && (
        <Alert
          type="error"
          data-testid="error-message"
          title={errorMessage.title}
        >
          {errorMessage.content}
        </Alert>
      )}

      <Formik
        initialValues={{ query, search: null }}
        onSubmit={async (values, actions) => {
          try {
            await onSubmitHandler()
          } catch (err) {
            actions.setFieldError('query', err.message)
          }
        }}
      >
        {({ handleSubmit, errors }) => (
          <form data-testid="form" onSubmit={handleSubmit}>
            <Fieldset>
              <Label htmlFor="search">Welk component wil je toevoegen</Label>
              <Field name="search">
                {(props) => {
                  return (
                    <FormikSelect
                      ref={selectRef}
                      {...props}
                      placeholder=""
                      autoFocus
                      onChange={setComponent}
                      onInputChange={inputChange}
                      defaultMenuIsOpen
                      defaultInputValue={query || ''}
                      controlShouldRenderValue
                      components={{ SingleValue }}
                      options={options}
                      noOptionsMessage={
                        () =>
                          query !== null ? 'Geen producten gevonden' : null // explictly disable when no query is set
                      }
                      getOptionValue={(option) => option.id}
                      getOptionLabel={(option) => option.name}
                      formatOptionLabel={(value) => (
                        <FormatOptionLabel query={query} value={value} />
                      )}
                      styles={componentSelectStyles}
                      theme={{
                        borderRadius: '0px',
                        invalid: !!errors.query, // Overwrite default invalid condition
                      }}
                    />
                  )
                }}
              </Field>

              {errors.query && (
                <FieldValidationMessage data-testid="search-error">
                  {errors.query}
                </FieldValidationMessage>
              )}

              {component && (
                <StyledComponentDetailsWrapper data-testid="component-details">
                  <ComponentDetails
                    close={() => {
                      setComponent(null)
                      selectRef.current.select.clearValue()
                      setQuery(null)
                    }}
                    preview
                    component={component}
                  />
                </StyledComponentDetailsWrapper>
              )}
            </Fieldset>
            <StyledButton type="submit" data-testid="submit">
              Toevoegen aan product
            </StyledButton>
          </form>
        )}
      </Formik>
    </ThemeProvider>
  )
}

ComponentSelect.propTypes = {
  name: string.isRequired,
  layerType: oneOf(LAYER_TYPES).isRequired,
  searchHandler: func.isRequired,
  addComponentHandler: func.isRequired,
  onSelectHandler: func.isRequired,
}

ComponentSelect.defaultProps = {
  searchHandler: ComponentRepository.filterComponents,
  addComponentHandler: ProductRepository.addComponent,
}

export default ComponentSelect
