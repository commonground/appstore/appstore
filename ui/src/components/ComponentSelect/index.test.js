// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { StaticRouter as Router } from 'react-router-dom'
import { fireEvent, render, waitFor, act } from '@testing-library/react'
import {
  LAYER_TYPE_INTERACTIE,
  REUSE_TYPE_ZELF_INSTALLEREN,
  STATUS_BETA,
} from '../../vocabulary'
import { ProductContext } from '../../pages/ProductPage'
import ComponentSelect from './index'

describe('ComponentSelect', () => {
  const product = {
    id: 42,
    components: [],
  }
  const setProduct = jest.fn().mockReturnValue({})

  it('the searchHandler is not called on initial load without search string', async () => {
    const searchHandlerSpy = jest.fn().mockResolvedValue([])

    const { findByTestId } = render(
      <Router location="/product/42/componenten/interactie/kies?q=">
        <ProductContext.Provider value={{ product, setProduct }}>
          <ComponentSelect
            name="Name"
            layerType={LAYER_TYPE_INTERACTIE}
            searchHandler={searchHandlerSpy}
            onSelectHandler={() => {}}
          />
        </ProductContext.Provider>
      </Router>,
    )
    await waitFor(async () => findByTestId('heading'))

    expect(searchHandlerSpy).not.toHaveBeenCalled()
  })

  it('the searchHandler is called on initial load with search string', async () => {
    const searchHandlerSpy = jest.fn().mockResolvedValue([])

    const { findByTestId } = render(
      <Router location="/product/42/componenten/interactie/kies?q=test">
        <ProductContext.Provider value={{ product, setProduct }}>
          <ComponentSelect
            name="Name"
            layerType={LAYER_TYPE_INTERACTIE}
            searchHandler={searchHandlerSpy}
            onSelectHandler={() => {}}
          />
        </ProductContext.Provider>
      </Router>,
    )
    await waitFor(async () => findByTestId('heading'))
    expect(searchHandlerSpy).toHaveBeenCalledWith({ query: 'test', status: '' })
  })

  it('the search dropdown shows one result', async () => {
    const searchHandler = jest.fn().mockResolvedValue([
      {
        id: 1,
        layerType: 'interactie',
        name: 'test interactie component',
        reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
        createdAt: '2020-01-31T08:01:50.801000Z',
      },
      {
        id: 2,
        layerType: 'proces',
        name: 'test proces component',
        reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
        createdAt: '2020-01-31T08:01:50.801000Z',
      },
    ])

    const { findByTestId, getAllByTestId } = render(
      <Router location="/product/42/componenten/interactie/kies?q=test">
        <ProductContext.Provider value={{ product, setProduct }}>
          <ComponentSelect
            product={product}
            name="Name"
            layerType={LAYER_TYPE_INTERACTIE}
            searchHandler={searchHandler}
            onSelectHandler={() => {}}
          />
        </ProductContext.Provider>
      </Router>,
    )
    await waitFor(async () => findByTestId('heading'))
    expect(await getAllByTestId('component-name')).toHaveLength(1)
  })

  it('selecting a search result shows the component details block', async () => {
    const searchHandler = jest.fn().mockResolvedValue([
      {
        id: 1,
        layerType: 'interactie',
        name: 'test component',
        description: '',
        status: STATUS_BETA,
        reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
        createdAt: '2020-01-31T08:01:50.801000Z',
      },
    ])

    const { getByTestId, findByTestId } = render(
      <Router location="/product/42/componenten/interactie/kies?q=test">
        <ProductContext.Provider value={{ product, setProduct }}>
          <ComponentSelect
            name="Name"
            layerType={LAYER_TYPE_INTERACTIE}
            searchHandler={searchHandler}
            onSelectHandler={() => {}}
          />
        </ProductContext.Provider>
      </Router>,
    )
    await waitFor(async () => findByTestId('heading'))
    const searchResult = await getByTestId('component-name')

    await act(async () => {
      await fireEvent.click(searchResult)
    })

    expect(getByTestId('component-details')).toBeTruthy()
  })

  it('submitting a component calls the addComponentHandler', async () => {
    const testComponent = {
      id: 1,
      layerType: 'interactie',
      name: 'test interactie component',
      organisationName: 'organisatie',
      description: '',
      status: STATUS_BETA,
      reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
      createdAt: '2020-01-31T08:01:50.801000Z',
    }
    const searchHandler = jest.fn().mockResolvedValue([testComponent])

    const addComponentHandler = jest.fn().mockResolvedValue({})

    const { getByTestId, findByTestId } = render(
      <Router location="/product/42/componenten/interactie/kies?q=test">
        <ProductContext.Provider value={{ product, setProduct }}>
          <ComponentSelect
            product={product}
            name="Name"
            layerType={LAYER_TYPE_INTERACTIE}
            searchHandler={searchHandler}
            addComponentHandler={addComponentHandler}
            onSelectHandler={() => {}}
          />
        </ProductContext.Provider>
      </Router>,
    )

    const searchResult = await findByTestId('component-name')
    fireEvent.click(searchResult)

    const form = getByTestId('form')
    await act(async () => {
      fireEvent.submit(form)
    })

    expect(addComponentHandler).toHaveBeenCalledWith(product.id, [
      testComponent,
    ])
  })
})
