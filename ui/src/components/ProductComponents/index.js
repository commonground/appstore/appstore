// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { withRouter, Route, Link } from 'react-router-dom'
import { arrayOf, number, oneOf, shape, string } from 'prop-types'

import ComponentsGroupedByLayer from '../../components/ComponentsGroupedByLayer'
import ComponentDetailPane from '../../components/ComponentDetailPane'
import { LAYER_TYPES } from '../../vocabulary'
import { StyledComponentsIntroduction } from './index.styles'

const ProductComponents = ({ components, match }) => {
  const parentUrl = match.url

  return (
    <>
      <StyledComponentsIntroduction>
        We werken vanuit een architectuur op basis van het 5-lagen model.
        Hiermee creëren we meer flexibiliteit, spreiden we risico’s en wordt
        bovendien voorkomen dat een component meer doet dan waar het voor
        bedoeld is. <Link to="/5-lagen-model">Meer over het 5-lagenmodel.</Link>
      </StyledComponentsIntroduction>
      <ComponentsGroupedByLayer components={components} />
      <Route
        exact
        path={`${parentUrl}/:id`}
        component={() => <ComponentDetailPane parentUrl={parentUrl} />}
      />
    </>
  )
}

ProductComponents.propTypes = {
  components: arrayOf(shape({ id: number, layerType: oneOf(LAYER_TYPES) })),
  match: shape({
    url: string,
    params: shape({
      id: string,
    }),
  }),
}

export default withRouter(ProductComponents)
