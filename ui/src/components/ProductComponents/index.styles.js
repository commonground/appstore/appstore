// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledComponentsIntroduction = styled.div`
  margin-bottom: ${(p) => p.theme.tokens.spacing10};
`
