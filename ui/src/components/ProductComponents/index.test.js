// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { render } from '@testing-library/react'

import theme from '../../theme'
import { STATUS_GEWENST } from '../../vocabulary'
import { UserContextProvider } from '../../user-context'
import ProductComponents from './index'

const components = [
  {
    id: 0,
    name: 'Component 1',
    layerType: 'proces',
    status: STATUS_GEWENST,
  },
]

test('exists', () => {
  const { container } = render(
    <ThemeProvider theme={theme}>
      <Router>
        <UserContextProvider>
          <ProductComponents components={components} onChange={() => {}} />
        </UserContextProvider>
      </Router>
    </ThemeProvider>,
  )
  expect(container).toBeTruthy()
})
