// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'
import theme from '../../theme'

import ProductList from './index'

const dummyProducts = [
  {
    id: 42,
    name: 'Drukte',
    detailPageImageUrl: 'http://example.com/drukte.png',
    shortDescription: 'Meet de drukte',
    isPublished: true,
  },
  {
    id: 43,
    name: 'Verhuizen',
    detailPageImageUrl: 'http://example.com/verhuizen.png',
    shortDescription: 'Verhuis je inwoner',
    isPublished: true,
  },
]

describe('ProductList', () => {
  let findAllByTestId

  beforeEach(() => {
    const renderResult = render(
      <ThemeProvider theme={theme}>
        <Router>
          <ProductList products={dummyProducts} />
        </Router>
      </ThemeProvider>,
    )
    findAllByTestId = renderResult.findAllByTestId
  })

  it('should list the products', async () => {
    const cards = await findAllByTestId('product-card')
    const products = await findAllByTestId('product')

    expect(cards).toHaveLength(2)
    expect(products).toHaveLength(2)
  })
})
