// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { shape, string, arrayOf, bool, number } from 'prop-types'
import ProductCard from '../ProductCard'
import { CardView, ListView } from './index.styles'

const ProductList = ({ products, ...props }) => (
  <ListView data-test="product-list" {...props}>
    {products.map((product) => (
      <CardView
        as="li"
        width={[1, 1 / 2, 1 / 3]}
        key={product.id}
        data-testid="product-card"
      >
        <ProductCard
          id={product.id}
          name={product.name}
          shortDescription={product.shortDescription}
          isPublished={product.isPublished}
          data-testid="product"
        />
      </CardView>
    ))}
  </ListView>
)

ProductList.propTypes = {
  products: arrayOf(
    shape({
      id: number,
      name: string,
      isPublished: bool,
    }),
  ),
}

ProductList.defaultProps = {
  products: [],
}

export default ProductList
