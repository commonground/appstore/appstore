// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Link } from 'react-router-dom'
import Avatar from '../design-system-candidates/Avatar'

import { IconExternalLink } from '../../icons'

import CloseButton from '../design-system-candidates/CloseButton'
import ComponentStatus from '../ComponentStatus'
import bluePencilImage from './pencil.svg'

export const StyledName = styled.span`
  margin-right: ${(p) => p.theme.tokens.spacing06};
`

export const StyledStatus = styled(ComponentStatus)`
  vertical-align: ${(p) =>
    p.preview
      ? 'middle'
      : `calc(${p.theme.tokens.spacing03} - ${p.theme.tokens.spacing01})`};
`

export const StyledSeparator = styled.hr`
  display: block;
  height: 1px;
  background: ${(p) => p.theme.tokens.colors.colorPaletteGray300};
  margin: ${(p) => p.theme.tokens.spacing07} 0;
  border: 0 none;
`

export const StyledBlocks = styled.div`
  display: flex;
  ${(p) => (p.preview ? '' : `margin-bottom: ${p.theme.tokens.spacing05};`)}
`

export const StyledBlock = styled.div`
  width: 100%;
`

StyledBlock.Title = styled.p`
  margin-top: 0;
  margin-bottom: ${(p) => p.theme.tokens.spacing01};
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray700};
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
`

StyledBlock.Content = styled.p`
  margin-top: ${(p) => p.theme.tokens.spacing01};
  margin-bottom: 0;
`

export const StyledActionLinks = styled.div`
  text-align: right;
`

export const StyledEditLink = styled(Link)`
  background: url(${bluePencilImage}) no-repeat;
  color: ${(p) => p.theme.tokens.colors.colorPaletteBlue800};
  padding-left: 1.75rem;
  font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};
  text-decoration: none;
`

export const StyledOwner = styled.div`
  display: flex;
`

export const StyledOwnerAvatar = styled(Avatar)`
  flex: 1;
  margin-right: ${(p) => p.theme.tokens.spacing02};
  max-width: ${(p) => p.theme.tokens.spacing09};
`

export const StyledOwnerName = styled.span`
  color: ${(p) => p.theme.tokens.colors.colorText};
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
`

export const StyledOwnerEmailAddress = styled.a`
  font-size: ${(p) => p.theme.tokens.colors.fontSizeMedium};
`

export const StyledMissingValue = styled.span`
  font-size: ${(p) => p.theme.tokens.colors.fontSizeMedium};
`

export const StyledCloseButton = styled(CloseButton)`
  &:before {
    font-size: ${(p) =>
      p.preview
        ? `${p.theme.tokens.spacing05}`
        : `${p.theme.tokens.spacing09}`};
  }
`

export const StyledIconExternalLink = styled(IconExternalLink)`
  margin-left: ${(p) => p.theme.tokens.spacing05};
  margin-right: 0;
`
