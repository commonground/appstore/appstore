// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import {
  DEVELOPED_BY_SELF,
  DEVELOPED_BY_THIRDPARTY,
  DEVELOPED_BY_UNKNOWN,
} from '../../vocabulary'
import translateDevelopedBy from './translate-developed-by'

test.each([
  [DEVELOPED_BY_SELF, 'Wordt zelf ontwikkeld'],
  [DEVELOPED_BY_UNKNOWN, 'Niet bekend of het zelf ontwikkeld wordt'],
  [DEVELOPED_BY_THIRDPARTY, 'Wordt niet zelf ontwikkeld'],
])('translation for reuse type %s', (developedBy, expected) => {
  expect(translateDevelopedBy(developedBy)).toBe(expected)
})
