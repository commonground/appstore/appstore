// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import {
  REUSE_TYPE_LANDELIJKE_VOORZIENING,
  REUSE_TYPE_ZELF_INSTALLEREN,
} from '../../vocabulary'

const translateReuseType = (reuseType) => {
  switch (reuseType) {
    case REUSE_TYPE_LANDELIJKE_VOORZIENING:
      return 'Landelijke voorziening'
    case REUSE_TYPE_ZELF_INSTALLEREN:
      return 'Zelf installeren'
    default:
      throw Error(`Unknown reuse type ${reuseType}`)
  }
}

export default translateReuseType
