// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import {
  DEVELOPED_BY_SELF,
  DEVELOPED_BY_THIRDPARTY,
  DEVELOPED_BY_UNKNOWN,
} from '../../vocabulary'

const translateDevelopedBy = (developedBy) => {
  switch (developedBy) {
    case DEVELOPED_BY_SELF:
      return 'Wordt zelf ontwikkeld'
    case DEVELOPED_BY_THIRDPARTY:
      return 'Wordt niet zelf ontwikkeld'
    case DEVELOPED_BY_UNKNOWN:
      return 'Niet bekend of het zelf ontwikkeld wordt'
    default:
      throw Error(`Unknown developed by ${developedBy}`)
  }
}

export default translateDevelopedBy
