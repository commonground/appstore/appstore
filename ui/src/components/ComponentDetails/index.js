// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { bool, func, number, oneOf, shape, string, object } from 'prop-types'
import { ThemeProvider } from 'styled-components/macro'
import { Button } from '@commonground/design-system'

import UserContext from '../../user-context'
import { Subtitle as StyledInfo, Title as StyledTitle } from '../Elements'
import {
  DEVELOPED_BY_TYPES,
  LAYER_TYPES,
  REUSE_TYPES,
  STATUS_GEPLAND,
  STATUS_GEWENST,
  STATUSES,
} from '../../vocabulary'
import theme from '../../theme'
import { userOwnsItem } from '../../permissions'
import translateReuseType from './translate-reuse-type'
import translateDevelopedBy from './translate-developed-by'
import {
  StyledActionLinks,
  StyledBlock,
  StyledBlocks,
  StyledCloseButton,
  StyledEditLink,
  StyledIconExternalLink,
  StyledMissingValue,
  StyledName,
  StyledOwner,
  StyledOwnerAvatar,
  StyledOwnerEmailAddress,
  StyledOwnerName,
  StyledSeparator,
  StyledStatus,
} from './index.styles'

export const renderCreatedAtDate = (isoDate) => {
  if (!isoDate) {
    return '-'
  }

  return new Date(isoDate).toLocaleDateString()
}

const OwnerBlockContent = ({ owner }) => {
  if (!owner) {
    return <StyledMissingValue>Onbekend</StyledMissingValue>
  }

  return (
    <StyledOwner>
      <StyledOwnerAvatar url={owner.picture_url} />
      <StyledBlock.Content>
        <StyledOwnerName>{owner.full_name}</StyledOwnerName>
        <br />
        <StyledOwnerEmailAddress
          href={`mailto:${owner.email}`}
          title={`Stuur een e-mail naar ${owner.full_name}`}
        >
          {owner.email}
        </StyledOwnerEmailAddress>
      </StyledBlock.Content>
    </StyledOwner>
  )
}

OwnerBlockContent.propTypes = {
  owner: object,
}

const AuthorBlockContent = ({ name }) => {
  if (!name) {
    name = <StyledMissingValue>Onbekend</StyledMissingValue>
  }
  return <div className="content">{name}</div>
}

AuthorBlockContent.propTypes = {
  name: string,
}

const ComponentDetails = ({ component, preview, close }) => {
  const {
    id,
    name,
    layerType,
    technologyStack,
    description,
    owner,
    organisationName,
    status,
    reuseType,
    createdAt,
    repositoryUrl,
    expectedQuarter,
    expectedYear,
    developedBy,
  } = component

  return (
    <ThemeProvider theme={theme}>
      {close && <StyledCloseButton preview={preview} onClick={close} />}
      <StyledTitle preview={preview}>
        <StyledName>{name}</StyledName>
        <StyledStatus status={status} preview={preview} />
      </StyledTitle>
      <StyledInfo>
        <span>{layerType}</span> ·{' '}
        {status === STATUS_GEPLAND ? (
          <span data-testid="expected-on">
            Verwachte oplevering: {`${expectedQuarter} ${expectedYear}`}
          </span>
        ) : status === STATUS_GEWENST ? (
          <span data-testid="developed-by">
            {translateDevelopedBy(developedBy)}
          </span>
        ) : (
          <span>{technologyStack}</span>
        )}
      </StyledInfo>

      {repositoryUrl ? (
        <p>
          <Button as="a" variant="secondary" href={repositoryUrl}>
            Bekijk broncode <StyledIconExternalLink />
          </Button>
        </p>
      ) : null}

      <p>{description}</p>

      <StyledBlocks>
        <StyledBlock>
          <StyledBlock.Title>Type hergebruik</StyledBlock.Title>
          <StyledBlock.Content data-testid="reuse-type">
            {translateReuseType(reuseType)}
          </StyledBlock.Content>
        </StyledBlock>
        <StyledBlock>
          <StyledBlock.Title>Toegevoegd op</StyledBlock.Title>
          <StyledBlock.Content data-testid="created-at">
            {renderCreatedAtDate(createdAt)}
          </StyledBlock.Content>
        </StyledBlock>
      </StyledBlocks>

      <StyledSeparator />

      <StyledBlocks preview={preview}>
        <StyledBlock>
          <StyledBlock.Title>Eigenaar</StyledBlock.Title>
          <OwnerBlockContent owner={owner} />
        </StyledBlock>
        <StyledBlock>
          <StyledBlock.Title>Auteur</StyledBlock.Title>
          <AuthorBlockContent name={organisationName} />
        </StyledBlock>
      </StyledBlocks>

      {preview || (
        <StyledActionLinks>
          <UserContext.Consumer>
            {
              (userContext) =>
                userContext && userOwnsItem(userContext.user, component) ? (
                  <StyledEditLink
                    to={`/componenten/${id}/bewerk`}
                    data-testid="edit-component-button"
                  >
                    Aanpassen
                  </StyledEditLink>
                ) : null
              /* eslint-disable-next-line react/jsx-curly-newline */
            }
          </UserContext.Consumer>
        </StyledActionLinks>
      )}
    </ThemeProvider>
  )
}

ComponentDetails.propTypes = {
  component: shape({
    id: number.isRequired,
    name: string.isRequired,
    layerType: oneOf(LAYER_TYPES).isRequired,
    technologyStack: string,
    description: string.isRequired,
    status: oneOf(STATUSES),
    reuseType: oneOf(REUSE_TYPES).isRequired,
    createdAt: string.isRequired,
    repositoryUrl: string,
    expectedQuarter: string,
    expectedYear: string,
    developedBy: oneOf(DEVELOPED_BY_TYPES),
  }).isRequired,
  preview: bool,
  close: func,
}

ComponentDetails.defaultProps = {
  preview: false,
  close: null,
}
export default ComponentDetails
