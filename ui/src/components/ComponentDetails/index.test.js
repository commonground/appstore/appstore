// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { MemoryRouter as Router } from 'react-router-dom'
import { render } from '@testing-library/react'
import { UserContextProvider } from '../../user-context'

import {
  LAYER_TYPE_INTERACTIE,
  STATUS_GEWENST,
  STATUS_BRUIKBAAR,
  REUSE_TYPE_ZELF_INSTALLEREN,
  STATUS_GEPLAND,
  DEVELOPED_BY_SELF,
  STATUS_BETA,
} from '../../vocabulary'

import ComponentDetails, { renderCreatedAtDate } from './index'

describe('ComponentDetails', () => {
  it('should display the component properties', () => {
    const dateString = '2020-01-31T08:01:50.801000Z'
    const { getByText, getByTestId, queryByTestId } = render(
      <Router>
        <ComponentDetails
          component={{
            layerType: LAYER_TYPE_INTERACTIE,
            name: 'Name',
            id: 42,
            description: 'Description',
            status: STATUS_BETA,
            technologyStack: 'Technology stack',
            reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
            createdAt: dateString,
            repositoryUrl: 'https://www.gitlab.com/my-repository/',
          }}
        />
      </Router>,
    )
    expect(getByTestId('status').textContent).toBe('beta')
    expect(getByTestId('reuse-type').textContent).toBe('Zelf installeren')
    expect(getByTestId('created-at').textContent).toBe(
      new Date(dateString).toLocaleDateString(),
    )

    expect(getByText('Bekijk broncode').getAttribute('href')).toBe(
      'https://www.gitlab.com/my-repository/',
    )

    expect(queryByTestId('edit-component-button')).toBeNull()
  })

  describe('when the component is editable', () => {
    it('should show the edit button', () => {
      const { getByTestId } = render(
        <Router>
          <UserContextProvider user={{ id: 42 }}>
            <ComponentDetails
              component={{
                layerType: LAYER_TYPE_INTERACTIE,
                name: 'Name',
                id: 42,
                description: 'Description',
                status: STATUS_BETA,
                technologyStack: 'Technology stack',
                reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
                createdAt: '2020-01-31T08:01:50.801000Z',
                repositoryURL: 'https://www.gitlab.com/my-repository/',
                owner: {
                  id: 42,
                },
              }}
            />
          </UserContextProvider>
        </Router>,
      )

      expect(getByTestId('edit-component-button').getAttribute('href')).toBe(
        '/componenten/42/bewerk',
      )
    })
  })

  describe('for a component with status BRUIKBAAR', () => {
    it('should hide the status label', () => {
      const { queryByTestId } = render(
        <Router>
          <ComponentDetails
            component={{
              layerType: LAYER_TYPE_INTERACTIE,
              name: 'Name',
              id: 42,
              description: 'Description',
              status: STATUS_BRUIKBAAR,
              technologyStack: 'Technology stack',
              reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
              createdAt: '2020-01-31T08:01:50.801000Z',
              repositoryUrl: 'https://www.gitlab.com/my-repository/',
            }}
          />
        </Router>,
      )

      expect(queryByTestId('status')).toBeNull()
    })
  })

  describe('for a component with status GEPLAND', () => {
    it('should show the planned date', () => {
      const { getByTestId } = render(
        <Router>
          <ComponentDetails
            component={{
              layerType: LAYER_TYPE_INTERACTIE,
              name: 'Name',
              id: 42,
              description: 'Description',
              status: STATUS_GEPLAND,
              technologyStack: 'Technology stack',
              reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
              createdAt: '2020-01-31T08:01:50.801000Z',
              repositoryUrl: 'https://www.gitlab.com/my-repository/',
              expectedQuarter: 'Q1',
              expectedYear: '2020',
            }}
          />
        </Router>,
      )

      expect(getByTestId('expected-on').textContent).toBe(
        'Verwachte oplevering: Q1 2020',
      )
    })
  })

  describe('for a component with status GEWENST', () => {
    it('should show who will develop the component', () => {
      const { getByTestId } = render(
        <Router>
          <ComponentDetails
            component={{
              layerType: LAYER_TYPE_INTERACTIE,
              name: 'Name',
              id: 42,
              description: 'Description',
              status: STATUS_GEWENST,
              technologyStack: 'Technology stack',
              reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
              createdAt: '2020-01-31T08:01:50.801000Z',
              developedBy: DEVELOPED_BY_SELF,
            }}
          />
        </Router>,
      )

      expect(getByTestId('developed-by').textContent).toBe(
        'Wordt zelf ontwikkeld',
      )
    })
  })
})

describe('renderCreatedAtDate', () => {
  it('should show the date formatted according to the current locale', () => {
    const dateString = '2020-01-31T08:01:50.801000Z'
    const result = renderCreatedAtDate(dateString)
    expect(result).toBe(new Date(dateString).toLocaleDateString())
  })

  describe('when the date is empty', () => {
    it('should show a dash', () => {
      const result = renderCreatedAtDate('')
      expect(result).toBe('-')
    })
  })
})
