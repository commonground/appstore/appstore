// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import {
  REUSE_TYPE_LANDELIJKE_VOORZIENING,
  REUSE_TYPE_ZELF_INSTALLEREN,
} from '../../vocabulary'
import translateReuseType from './translate-reuse-type'

test.each([
  [REUSE_TYPE_ZELF_INSTALLEREN, 'Zelf installeren'],
  [REUSE_TYPE_LANDELIJKE_VOORZIENING, 'Landelijke voorziening'],
])('translation for reuse type %s', (reuseType, expected) => {
  expect(translateReuseType(reuseType)).toBe(expected)
})
