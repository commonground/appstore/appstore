// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { bool } from 'prop-types'
import { mediaQueries } from '@commonground/design-system'
import styled from 'styled-components'

const CloseButton = ({ preview, ...props }) => (
  <button type="button" {...props} />
)

CloseButton.propTypes = {
  preview: bool,
}

export default styled(CloseButton)`
  background: none;
  border: 0 none;
  cursor: pointer;
  display: block;
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray600};
  float: right;
  text-transform: uppercase;
  text-decoration: none;
  text-align: center;
  padding-left: ${(p) => p.theme.tokens.spacing04};
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  line-height: 1rem;
  padding-top: 8px;

  ${mediaQueries.smUp`
    margin-top: -${(p) => p.theme.tokens.spacing02};
    margin-right: -${(p) => p.theme.tokens.spacing02};
  `}

  &:before {
    display: block;
    content: '✕';
    font-size: ${(p) => p.theme.tokens.spacing09};
    margin-bottom: ${(p) => p.theme.tokens.spacing03};
  }
`
