// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render, act } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'

import { UserContextProvider } from '../../../user-context'
import theme from '../../../theme'
import PageTemplate from './index'

describe('the PageTemplate', () => {
  let getByTestId

  beforeEach(async () => {
    const fetchAuthenticatedUserStub = jest
      .fn()
      .mockRejectedValue(new Error('unauthenticated'))

    await act(async () => {
      const rendered = render(
        <Router>
          <ThemeProvider theme={theme}>
            <UserContextProvider
              fetchAuthenticatedUser={fetchAuthenticatedUserStub}
            >
              <PageTemplate />
            </UserContextProvider>
          </ThemeProvider>
        </Router>,
      )
      getByTestId = rendered.getByTestId
    })
  })

  describe('the header', () => {
    it('should contain the CG logo linking to the Homepage', () => {
      expect(getByTestId('cg-logo')).toBeTruthy()
      expect(getByTestId('cg-logo').getAttribute('href')).toBe('/')
    })

    describe('the navigation', () => {
      it('should contain a link to the Homepage', () => {
        expect(getByTestId('link-homepage').getAttribute('href')).toBe('/')
      })

      it('should contain a link to the Portfolio', () => {
        expect(getByTestId('link-portfolio').getAttribute('href')).toBe(
          '/portfolio',
        )
      })

      it('should contain a link to the Producten', () => {
        expect(getByTestId('link-products').getAttribute('href')).toBe(
          '/producten',
        )
      })

      it('should contain a link to the Componenten', () => {
        expect(getByTestId('link-components').getAttribute('href')).toBe(
          '/componenten',
        )
      })

      it('should contain a link to the Over de Componentencatalogus page', () => {
        expect(getByTestId('link-about').getAttribute('href')).toBe('/over')
      })

      it('should contain a link to the Contact page', () => {
        expect(getByTestId('link-contact').getAttribute('href')).toBe(
          '/contact',
        )
      })
    })

    it('should show the login button', () => {
      expect(getByTestId('link-login').getAttribute('href')).toBe(
        '/oidc/authenticate/',
      )
    })
  })

  describe('the footer', () => {
    it('should contain a link to the VNG Realisatie website', () => {
      expect(getByTestId('vng-link')).toBeTruthy()
      expect(getByTestId('vng-link').getAttribute('href')).toBe(
        'https://www.vngrealisatie.nl/',
      )
    })
  })
})
