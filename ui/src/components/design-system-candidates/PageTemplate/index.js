// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { node } from 'prop-types'
import { NavLink, useLocation, Link } from 'react-router-dom'
import { PrimaryNavigation } from '@commonground/design-system'
import UserNavigation from '../UserNavigation'
import { Container } from '../Grid'
import { IconBox, IconPresent, IconHome } from '../../../icons'
import VNGLogo from './assets/vng-logo.svg'
import ContentContainer from './ContentContainer'
import {
  LogoLink,
  HeaderNav,
  Footer,
  LayoutCenteredSingleColumn,
  StyledLogo,
} from './index.styles'

const PageTemplate = ({ children }) => {
  const { pathname } = useLocation()
  return (
    <>
      <Container as="header">
        <HeaderNav as="nav">
          <LogoLink to="/" data-testid="cg-logo">
            <StyledLogo />
            <div>
              <span className="title">Componentencatalogus</span>
              <span className="tagline">
                Herbruikbare software voor gemeenten
              </span>
            </div>
          </LogoLink>

          <UserNavigation />
        </HeaderNav>
      </Container>

      <PrimaryNavigation
        LinkComponent={NavLink}
        pathname={pathname}
        mobileMoreText="Meer"
        items={[
          {
            name: 'Home',
            Icon: IconHome,
            to: '/',
            exact: true,
            'data-testid': 'link-homepage',
          },
          {
            name: 'Portfolio',
            Icon: IconPresent,
            to: '/portfolio',
            'data-testid': 'link-portfolio',
          },
          {
            name: 'Software producten',
            Icon: IconBox,
            to: '/producten',
            'data-testid': 'link-products',
          },
          {
            name: 'Componenten',
            to: '/componenten',
            'data-testid': 'link-components',
          },
          {
            name: 'Over',
            to: '/over',
            'data-testid': 'link-about',
          },
          {
            name: 'Contact',
            to: '/contact',
            'data-testid': 'link-contact',
          },
        ]}
      />

      {children}

      <Footer>
        <Footer.Container>
          <Link to="/privacyverklaring">Privacyverklaring</Link>
          <a href="https://www.vngrealisatie.nl/" data-testid="vng-link">
            <img
              src={VNGLogo}
              alt="Logo VNG Realisatie"
              title="Logo VNG Realisatie"
            />
          </a>
        </Footer.Container>
      </Footer>
    </>
  )
}

PageTemplate.ContentContainer = ContentContainer
PageTemplate.LayoutCenteredSingleColumn = LayoutCenteredSingleColumn

PageTemplate.propTypes = {
  children: node,
}

export default PageTemplate
