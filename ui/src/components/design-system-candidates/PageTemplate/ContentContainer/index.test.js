// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components/macro'
import theme from '../../../../theme'
import ContentContainer from './index'

test('Renders without crashing', () => {
  expect(() =>
    render(
      <ThemeProvider theme={theme}>
        <ContentContainer heroImages={{ xsUp: 'background.png' }}>
          <p>Some content</p>
        </ContentContainer>
      </ThemeProvider>,
    ),
  ).not.toThrow()
})
