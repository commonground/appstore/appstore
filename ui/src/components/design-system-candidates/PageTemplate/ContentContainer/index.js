// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { mediaQueries } from '@commonground/design-system'
import styled, { css } from 'styled-components'
import { shape } from 'prop-types'

const setHeroImages = (heroImageObject) => {
  const images = Object.entries(heroImageObject)

  /* eslint-disable security/detect-object-injection */
  const backgroundStrings = images.reduce((accumulator, [size, src]) => {
    if (Object.keys(mediaQueries).includes(size)) {
      accumulator.push(css`
        ${mediaQueries[size]`
          background-image: url(${src});
        `}
      `)
    }
    return accumulator
  }, [])
  /* eslint-enable security/detect-object-injection */

  return css(backgroundStrings)
}

const StyledMain = styled.main`
  flex: 1;
  position: relative;

  &:before {
    background-repeat: no-repeat;
    content: '';
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 20rem;
    background-position: top center;
    z-index: -1;
    ${(p) => setHeroImages(p.heroImages)};
  }
`

StyledMain.propTypes = {
  heroImages: shape({}),
}

StyledMain.defaultProps = {
  heroImages: {},
}

export default StyledMain
