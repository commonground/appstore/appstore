// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Link } from 'react-router-dom'
import {
  mediaQueries,
  mobileNavigationHeight,
} from '@commonground/design-system'
import { Container, Row } from '../Grid'
import { ReactComponent as ComponentencatalogusLogo } from './assets/componentencatalogus-logo.svg'

export const StyledLogo = styled(ComponentencatalogusLogo)`
  width: 2rem;
  height: 2rem;

  ${mediaQueries.smUp`
    width: 3rem;
    height: 3rem;
  `};
`

export const LogoLink = styled(Link)`
  display: flex;
  align-items: center;
  text-decoration: none;

  div {
    padding-left: 1em;
    text-decoration: none;
    display: flex;
    flex-direction: column;

    .title {
      color: ${(p) => p.theme.tokens.colors.colorText};
      font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};
      font-size: 20px;
    }

    .tagline {
      color: ${(p) => p.theme.tokens.colorPaletteGray700};
      font-size: ${(p) => p.theme.tokens.fontSizeSmall};
    }
  }
`

export const HeaderNav = styled(Row)`
  justify-content: space-between;
  padding: ${(p) => p.theme.tokens.spacing05};
`

export const LayoutCenteredSingleColumn = styled(Container)`
  position: relative;
  max-width: ${(p) => p.theme.tokens.containerWidth};

  ${mediaQueries.xs`
    padding-top: ${(p) => p.theme.tokens.spacing05};
    padding-bottom: ${(p) => p.theme.tokens.spacing09};
  `}

  ${mediaQueries.smUp`
    padding: ${({ theme: { tokens } }) =>
      `${tokens.spacing10} ${tokens.spacing05} ${tokens.spacing12}`};
  `}
`

const StyledFooter = styled.footer`
  background: #474e57;
`

StyledFooter.Container = styled(Container)`
  display: flex;
  padding: 42px ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.mdDown`
    margin-bottom: ${mobileNavigationHeight};
  `}

  > a:last-child {
    margin-left: auto;
  }

  > a:not(:last-child) {
    align-items: center;
    display: flex;
    color: ${(p) => p.theme.tokens.colors.colorPaletteGray200};
  }
`

export const Footer = StyledFooter
