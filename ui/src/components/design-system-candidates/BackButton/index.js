// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { node, string } from 'prop-types'
import { Link } from 'react-router-dom'
import { Button, Icon } from '@commonground/design-system'
import { IconChevronLeft } from '../../../icons'

const BackButton = ({ to, title, children }) => {
  return (
    <Button as={Link} to={to} variant="link" aria-label={title}>
      <Icon as={IconChevronLeft} size="small" inline />
      {children}
    </Button>
  )
}

BackButton.propTypes = {
  to: string.isRequired,
  title: string.isRequired,
  children: node,
}

export default BackButton
