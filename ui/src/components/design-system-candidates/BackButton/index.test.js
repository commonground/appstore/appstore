// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import React from 'react'
import { StaticRouter as Router } from 'react-router-dom'
import { render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import theme from '../../../theme'
import BackButton from './index'

test('renders without crashing', () => {
  expect(() =>
    render(
      <Router>
        <ThemeProvider theme={theme}>
          <BackButton to="/link" title="back" />
        </ThemeProvider>
      </Router>,
    ),
  ).not.toThrow()
})
