// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'

export const UnauthenticatedUserMenu = styled.ul`
  li {
    display: inline-block;

    a {
      text-decoration: underline;
      text-decoration-skip-ink: auto;
    }
  }
`

export const AuthenticatedUserMenu = styled.div`
  display: flex;
  align-items: center;
  position: relative;

  img {
    margin-right: 8px;
  }

  ul {
    display: block;
    position: absolute;
    top: 35px;
    right: 0;
    padding: 0;
    background: #ffffff;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.25);
    list-style-type: none;
    min-width: 200px;
    z-index: 2;

    li {
      &:not(:last-child) {
        border-bottom: 1px solid #e6e6e6;
      }

      button {
        width: 100%;
        padding: 0;
        border: none;
        font: inherit;
        color: inherit;
        background-color: transparent;
        cursor: pointer;
      }

      a,
      button {
        display: block;
        text-align: left;
        text-decoration: none;
        color: ${(p) => p.theme.tokens.colors.colorPaletteGray900};
        font-size: ${(p) => p.theme.tokens.fontSizeMedium};
        padding: ${(p) => p.theme.tokens.spacing04}
          ${(p) => p.theme.tokens.spacing06};
      }

      a:hover,
      button:hover {
        background-color: ${(p) => p.theme.tokens.colors.colorPaletteGray100};
      }
    }
  }
`

export const StyledAvatar = styled.figure`
  padding: 4px 0;
  margin: 0;
  width: ${(p) => p.theme.tokens.spacing09};
  height: ${(p) => p.theme.tokens.spacing09};

  .avatar-image {
    max-height: 100%;
    max-width: 100%;
    border-radius: 100%;
  }
`

export const StyledMenuToggleButton = styled.button`
  padding: 0;
  border: none;
  font: inherit;
  color: inherit;
  background-color: transparent;
  cursor: pointer;

  display: flex;
  align-items: center;
`
