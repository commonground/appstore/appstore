// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components/macro'
import { MemoryRouter as Router } from 'react-router-dom'

import theme from '../../../theme'
import { UserContextProvider } from '../../../user-context'
import UserNavigation from './index'

describe('the UserNavigation', () => {
  describe('when authenticated', () => {
    let result

    beforeEach(() => {
      result = render(
        <Router>
          <ThemeProvider theme={theme}>
            <UserContextProvider user={{ fullName: 'John Doe' }}>
              <UserNavigation />
            </UserContextProvider>
            <div data-testid="outside-user-menu" />
          </ThemeProvider>
        </Router>,
      )
    })

    it('should display the the full name and avatar', () => {
      const { getByTestId } = result

      expect(getByTestId('full-name').textContent).toEqual('John Doe')
      expect(getByTestId('avatar')).toBeTruthy()
    })

    it('hides the user menu by default', () => {
      const { queryByTestId } = result

      expect(queryByTestId('user-menu-options')).toBeNull()
    })

    describe('and toggled the menu', () => {
      beforeEach(() => {
        const { queryByTestId } = result
        queryByTestId('user-menu-toggle').click()
      })

      it('should display the user menu', async () => {
        const { queryByTestId } = result
        expect(queryByTestId('user-menu-options')).toBeTruthy()
      })

      describe('on blur', () => {
        beforeEach(() => {
          const { queryByTestId } = result
          queryByTestId('user-menu-toggle').click()
          queryByTestId('outside-user-menu').click()
        })

        it('should hide the user menu', async () => {
          const { queryByTestId } = result
          expect(queryByTestId('user-menu-options')).toBeNull()
        })
      })
    })
  })

  describe('when not authenticated', () => {
    let result

    beforeEach(() => {
      const fetchAuthenticatedUserStub = jest
        .fn()
        .mockRejectedValue(new Error('unauthenticated'))

      result = render(
        <Router>
          <ThemeProvider theme={theme}>
            <UserContextProvider
              user={null}
              fetchAuthenticatedUser={fetchAuthenticatedUserStub}
            >
              <UserNavigation />
            </UserContextProvider>
          </ThemeProvider>
        </Router>,
      )
    })

    it('should display the login link', () => {
      const { getByTestId } = result

      expect(getByTestId('link-login')).toBeTruthy()
    })
  })
})
