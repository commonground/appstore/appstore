// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useContext, useState } from 'react'
import styled from 'styled-components'
import Cookies from 'js-cookie'
import { mediaQueries } from '@commonground/design-system'

import UserContext from '../../../user-context'
import Avatar from '../Avatar'
import {
  AuthenticatedUserMenu,
  UnauthenticatedUserMenu,
  StyledMenuToggleButton,
} from './index.styles'

const UserName = styled.span`
  white-space: nowrap;

  ${mediaQueries.xs`
    display: none;
  `}
`

const UserNavigation = () => {
  const context = useContext(UserContext)
  const [menuIsOpen, setMenuIsOpen] = useState(false)

  if (typeof context === 'undefined') {
    throw new Error('UserNavigation requires a UserProvider')
  }

  const onClickHandler = (event) => {
    setMenuIsOpen(!menuIsOpen)
    event.currentTarget.focus()
  }

  let timeoutId
  const onBlurHandler = () => {
    timeoutId = setTimeout(() => {
      setMenuIsOpen(false)
    })
  }

  const onFocusHandler = () => {
    clearTimeout(timeoutId)
  }

  const { user } = context

  return !user ? (
    <UnauthenticatedUserMenu>
      <li>
        <a href="/oidc/authenticate/" data-testid="link-login">
          Inloggen
        </a>
      </li>
    </UnauthenticatedUserMenu>
  ) : (
    <AuthenticatedUserMenu
      isOpen={menuIsOpen}
      onFocus={onFocusHandler}
      onBlur={onBlurHandler}
    >
      <StyledMenuToggleButton
        type="button"
        onClick={onClickHandler}
        aria-haspopup="true"
        aria-expanded={menuIsOpen}
        aria-controls="user-menu-options"
        aria-label="Account menu"
        data-testid="user-menu-toggle"
      >
        <Avatar data-testid="avatar" alt="User avatar" url={user.pictureUrl} />
        <UserName data-testid="full-name">{user.fullName}</UserName>
      </StyledMenuToggleButton>
      {menuIsOpen && (
        <ul id="user-menu-options" data-testid="user-menu-options">
          <li>
            <a href="https://account.pleio.nl/profile/">Account</a>
          </li>
          <li>
            <form method="POST" action="/oidc/logout/">
              <input
                type="hidden"
                name="csrfmiddlewaretoken"
                value={Cookies.get('csrftoken')}
              />
              <button type="submit">Uitloggen</button>
            </form>
          </li>
        </ul>
      )}
    </AuthenticatedUserMenu>
  )
}

export default UserNavigation
