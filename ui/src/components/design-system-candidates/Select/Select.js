// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { forwardRef } from 'react'
import { arrayOf, shape, string } from 'prop-types'
import ReactSelect from 'react-select'

import { selectStyles } from './index.styles'

export const SelectWithRef = (props, ref) => (
  <ReactSelect
    ref={ref}
    styles={selectStyles}
    placeholder="Maak een keuze..."
    {...props}
  />
)

const Select = forwardRef(SelectWithRef)

Select.propTypes = {
  options: arrayOf(
    shape({
      value: string,
      label: string,
    }),
  ).isRequired,
}

export default Select
