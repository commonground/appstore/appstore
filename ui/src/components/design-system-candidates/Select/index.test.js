// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useRef } from 'react'
import { render, act, fireEvent } from '@testing-library/react'
import selectEvent from 'react-select-event'
import { Formik, Form, Field } from 'formik'

import { Select, FormikSelect } from './index'

const options = [
  { value: 'one', label: 'Uno' },
  { value: 'two', label: 'Dos' },
  { value: 'three', label: 'Tres' },
]

test('Select should function', async () => {
  const { getByTestId, getByLabelText } = render(
    <form data-testid="form">
      <label htmlFor="selection">Choose</label>
      <Select
        options={options}
        name="selection"
        inputId="selection"
        defaultValue={options[2]}
      />
    </form>,
  )

  const form = getByTestId('form')
  expect(form).toHaveFormValues({ selection: 'three' })

  await selectEvent.select(getByLabelText('Choose'), ['Dos'])

  expect(form).toHaveFormValues({ selection: 'two' })
})

test('Select should pass ref to select element', async () => {
  const TestForm = () => {
    const select = useRef()
    return (
      <form
        data-testid="form"
        onReset={() => select.current.select.clearValue()}
      >
        <label htmlFor="selection">Choose</label>
        <Select
          ref={select}
          options={options}
          name="selection"
          inputId="selection"
        />
      </form>
    )
  }
  const { getByTestId, getByLabelText } = render(<TestForm />)

  await selectEvent.select(getByLabelText('Choose'), ['Dos'])

  const form = getByTestId('form')
  expect(form).toHaveFormValues({ selection: 'two' })

  fireEvent.reset(form)

  expect(form).toHaveFormValues({ selection: '' })
})

test('FormikSelect should function as Formik Field COMPONENT', async () => {
  const initialValues = { selection: 'three' }

  const { getByTestId, getByLabelText } = render(
    <Formik initialValues={initialValues}>
      {(formikProps) => {
        const selectionAsText = formikProps.values.selection
        return (
          <Form>
            <label htmlFor="selection">Choose</label>
            <Field
              component={FormikSelect}
              options={options}
              name="selection"
              inputId="selection"
            />
            <span data-testid="parsed-selection">{selectionAsText}</span>
          </Form>
        )
      }}
    </Formik>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('three')

  // `select` causes Formik to rerender, so we have to wrap it in `act`
  await act(async () => {
    await selectEvent.select(getByLabelText('Choose'), ['Dos'])
  })

  expect(getByTestId('parsed-selection')).toHaveTextContent('two')
})

test('FormikSelect should function as Formik Field COMPONENT and accept multiple values', async () => {
  // Note: for some reason it doesn't work when providing initial values,
  // so we need the extra steps below to set and then remove the selection.
  const initialValues = { selection: [] }

  const { getByTestId, getByLabelText } = render(
    <Formik initialValues={initialValues}>
      {(formikProps) => {
        const selectionAsText = formikProps.values.selection.join(',')

        return (
          <Form>
            <label htmlFor="selection">Choose</label>
            <Field
              component={FormikSelect}
              options={options}
              name="selection"
              inputId="selection"
              isMulti
            />
            <span data-testid="parsed-selection">{selectionAsText}</span>
          </Form>
        )
      }}
    </Formik>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('')

  await act(async () => {
    await selectEvent.select(getByLabelText('Choose'), ['Dos', 'Tres'])
  })

  expect(getByTestId('parsed-selection')).toHaveTextContent('two,three')

  await act(async () => {
    await selectEvent.clearAll(getByLabelText('Choose'))
  })

  expect(getByTestId('parsed-selection')).toHaveTextContent('')
})

test('FormikSelect should function as Formik Field CHILD and pass a ref', async () => {
  const initialValues = { selection: '' }

  const TestForm = () => {
    const select = useRef()
    return (
      <Formik initialValues={initialValues}>
        {(formikProps) => {
          const selectionAsText = formikProps.values.selection
          return (
            <Form
              data-testid="form"
              onReset={() => select.current.select.clearValue()}
            >
              <label htmlFor="selection">Choose</label>
              <Field name="selection">
                {(props) => (
                  <FormikSelect
                    ref={select}
                    {...props}
                    options={options}
                    inputId="selection"
                  />
                )}
              </Field>
              <span data-testid="parsed-selection">{selectionAsText}</span>
            </Form>
          )
        }}
      </Formik>
    )
  }

  const { getByTestId, getByLabelText } = render(<TestForm />)

  expect(getByTestId('parsed-selection')).toHaveTextContent('')

  await act(async () => {
    await selectEvent.select(getByLabelText('Choose'), ['Dos'])
  })

  expect(getByTestId('parsed-selection')).toHaveTextContent('two')

  await act(async () => {
    await fireEvent.reset(getByTestId('form'))
  })

  expect(getByTestId('parsed-selection')).toHaveTextContent('')
})

test('FormikSelect should function when passed a non-standard option object', async () => {
  const initialValues = { selection: '' }

  const nonDefaultOptions = [
    { id: 1, name: 'Option one' },
    { id: 2, name: 'Option two' },
    { id: 3, name: 'Option three' },
  ]

  const { getByTestId, getByLabelText } = render(
    <Formik initialValues={initialValues}>
      {(formikProps) => {
        const selectionAsText = formikProps.values.selection
        return (
          <Form data-testid="form">
            <label htmlFor="selection">Choose</label>
            <Field name="selection">
              {(props) => (
                <FormikSelect
                  {...props}
                  options={nonDefaultOptions}
                  getOptionValue={(option) => option.id}
                  getOptionLabel={(option) => option.name}
                  inputId="selection"
                />
              )}
            </Field>
            <span data-testid="parsed-selection">{selectionAsText}</span>
          </Form>
        )
      }}
    </Formik>,
  )

  expect(getByTestId('parsed-selection')).toHaveTextContent('')

  await act(async () => {
    await selectEvent.select(getByLabelText('Choose'), ['Option two'])
  })

  expect(getByTestId('parsed-selection')).toHaveTextContent('2')
})
