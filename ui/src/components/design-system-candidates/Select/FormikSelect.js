// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { forwardRef } from 'react'
import { object, arrayOf, shape, string } from 'prop-types'
import { ErrorMessage } from 'formik'

import FieldValidationMessage from '../FieldValidationMessage'
import Select from './Select'

/* eslint-disable react/prop-types */
/* prop-types not supported for forwardRef components */
export const SelectWithRef = ({ field, form, theme, ...props }, ref) => {
  const { name } = field
  const { setFieldValue, setTouched } = form
  const { options, isMulti, onChange } = props
  const formikRelated = {}

  // Integrate Formik touched state
  formikRelated.onBlur = () => {
    setTouched({ [name]: true })
  }

  // Extract chosen value and pass to Formik
  formikRelated.onChange = (selectedOption, actionType) => {
    const getOptionValue =
      props.getOptionValue ||
      function (option) {
        return option.value
      }

    if (selectedOption === null) {
      setFieldValue(name, isMulti ? [] : '')
    } else {
      setFieldValue(
        name,
        isMulti
          ? selectedOption.map((option) => getOptionValue(option))
          : getOptionValue(selectedOption),
      )
    }

    if (onChange) onChange(selectedOption, actionType)
  }

  // Loop back the selection to react-select using it's value stored in formik
  formikRelated.value = (() => {
    if (options) {
      return isMulti
        ? options.filter((option) => field.value.indexOf(option.value) >= 0)
        : options.find((option) => option.value === field.value)
    } else {
      return isMulti ? [] : ''
    }
  })()

  // Make sure we can style the invalid state
  // Access in styles file by `state.theme.invalid`
  formikRelated.theme = {
    // eslint-disable-next-line security/detect-object-injection
    invalid: !!form.errors[name] && form.touched[name],
    // Allow overwriting the invalid condition in case you want your own error handling
    ...theme,
  }

  return (
    <>
      <Select ref={ref} {...props} {...formikRelated} />
      <ErrorMessage name={field.name} component={FieldValidationMessage} />
    </>
  )
}
/* eslint-enable react/prop-types */

const FormikSelect = forwardRef(SelectWithRef)

FormikSelect.propTypes = {
  field: object.isRequired, // Formik field props
  form: object.isRequired, // Formik form props
  options: arrayOf(
    shape({
      value: string,
      label: string,
    }),
  ).isRequired,
}

export default FormikSelect
