// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import theme from '../../../theme'

export function control(provided, state) {
  const defaultStyle = {
    ...provided,
    borderRadius: '0',
    boxShadow: 'none',
    '&:hover': {
      borderColor: state.isFocused
        ? theme.tokens.colors.colorPaletteBlue700
        : theme.tokens.colors.colorPaletteGray500,
    },
  }

  const stateStyle = state.theme.invalid
    ? {
        padding: `calc(${theme.tokens.spacing02} - 1px)`,
        border: `2px solid ${theme.tokens.colors.colorAlertError}`,
      }
    : state.isFocused
    ? {
        padding: `calc(${theme.tokens.spacing02} - 1px)`,
        border: `2px solid ${theme.tokens.colors.colorPaletteBlue700}`,
      }
    : {
        padding: `${theme.tokens.spacing02}`,
        border: `1px solid ${theme.tokens.colors.colorPaletteGray500}`,
      }

  return { ...defaultStyle, ...stateStyle }
}

export function input(provided) {
  return {
    ...provided,
    fontSize: `${theme.tokens.fontSizeMedium}`,
    fontFamily: "'Source Sans Pro', sans-serif",
    color: `${theme.tokens.colors.colorText}`,
    outline: 'none',
    lineHeight: `${theme.tokens.lineHeightText}`,
  }
}

export function menu(provided) {
  return {
    ...provided,
    width: '100%',
    position: 'absolute',
    zIndex: '1000',
    background: `${theme.tokens.colors.colorBackground}`,
    marginTop: '0',
    marginBottom: '0',
    boxShadow: `${theme.tokens.shadowDropDown}`,
  }
}

export function menuList(provided) {
  return {
    ...provided,
    paddingBottom: `0`,
    paddingTop: `0`,
  }
}

export function noOptionsMessage(provided) {
  return {
    ...provided,
    padding: `${theme.tokens.spacing08} ${theme.tokens.spacing04}`,
  }
}

export function option(provided, state) {
  const { isDisabled, isSelected, isFocused } = state
  return {
    ...provided,
    width: '100%',
    padding: `${theme.tokens.spacing05}`,
    borderBottom: `1px solid ${theme.tokens.colors.colorPaletteGray300}`,
    lineHeight: `${theme.tokens.lineHeightText}`,
    fontSize: `${theme.tokens.fontSizeMedium}`,
    fontFamily: "'Source Sans Pro', sans-serif",
    color: isDisabled
      ? theme.tokens.colors.colorTextLabel
      : theme.tokens.colors.colorText,
    backgroundColor: isSelected
      ? theme.tokens.colors.colorPaletteGray200
      : isFocused
      ? theme.tokens.colors.colorPaletteGray100
      : theme.tokens.colors.colorBackground,

    ':active': {
      backgroundColor: !isDisabled && theme.tokens.colors.brandSecondary1,
    },
  }
}

export function singleValue(provided) {
  return {
    ...provided,
    width: `calc(100% - 1.25rem)`,
  }
}

export function valueContainer(provided) {
  return {
    ...provided,
    paddingTop: `${theme.tokens.spacing02}`,
    paddingBottom: `${theme.tokens.spacing02}`,
  }
}

/**
  You can change the styling of the child components by:
  
  - Extending our default styling
  
  import { selectStyles, option } from '.../Select/index.styles'

  const MyStyles = {
    ...selectStyles,
    option: (args) => ({
      ...option(...args),
      color: 'red',
    }),
  }

  - Ignore our styling for a component, but use the default styling

  const MyStyles = {
    ...selectStyles,
    option: (provided, state) => ({
      ...provided,
      color: 'red'
    })
  }

  - Ignore any styling for that component

  const MyStyles = {
    ...selectStyles,
    option: () => ({
      display: none
    })
  }
 */
export const selectStyles = {
  control,
  input,
  menu,
  menuList,
  noOptionsMessage,
  option,
  singleValue,
  valueContainer,
}
