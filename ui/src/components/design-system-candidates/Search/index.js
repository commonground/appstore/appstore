// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { object, func, node } from 'prop-types'
import { ThemeProvider } from 'styled-components/macro'
import theme from '../../../theme'
import { StyledSearch, StyledInput, StyledSearchIcon } from './index.styles'

const Search = ({ inputProps, onQueryChanged, children, ...props }) => (
  <ThemeProvider theme={theme}>
    <StyledSearch {...props}>
      <StyledInput
        onChange={(event) => onQueryChanged(event.target.value)}
        {...inputProps}
      />
      <StyledSearchIcon />
      {children}
    </StyledSearch>
  </ThemeProvider>
)

Search.propTypes = {
  onQueryChanged: func,
  inputProps: object,
  children: node,
}

Search.defaultProps = {
  onQueryChanged: () => {},
  inputProps: {
    placeholder: 'Zoeken…',
  },
}

export default Search
