// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { node, oneOf, string } from 'prop-types'
import { Container, Content, Title } from './index.styles'
import { TYPE_OPTIONS } from './const'

const Alert = ({ title, children, ...props }) => (
  <Container {...props}>
    {title && title.length > 1 ? (
      <Title data-testid="title">{title}</Title>
    ) : null}
    <Content data-testid="content">{children}</Content>
  </Container>
)

Alert.propTypes = {
  title: string,
  type: oneOf([
    TYPE_OPTIONS.INFO,
    TYPE_OPTIONS.WARNING,
    TYPE_OPTIONS.SUCCESS,
    TYPE_OPTIONS.ERROR,
  ]),
  children: node,
}

Alert.defaultProps = {
  type: TYPE_OPTIONS.INFO,
}

export default Alert
