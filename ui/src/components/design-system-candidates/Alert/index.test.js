// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components/macro'

import theme from '../../../theme'
import Alert from './index'

describe('Alert', () => {
  it('should exist', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <Alert title="My First Alert" data-testid="link">
          Hello, World!
        </Alert>
      </ThemeProvider>,
    )

    expect(getByTestId('link')).toBeTruthy()
    expect(getByTestId('title').textContent).toBe('My First Alert')
    expect(getByTestId('content').textContent).toBe('Hello, World!')
  })
})
