// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { TOKENS, TYPE_OPTIONS } from './const'

const getTypeToken =
  (name) =>
  ({ theme, type }) => {
    const tokens = {
      [TOKENS.colorBackgroundAlert]: {
        [TYPE_OPTIONS.INFO]: theme.tokens.colors.colorPaletteBlue100,
        [TYPE_OPTIONS.SUCCESS]: '#D6ECC1',
        [TYPE_OPTIONS.WARNING]: '#FFF9C3',
        [TYPE_OPTIONS.ERROR]: '#FFC6CE',
      },
      [TOKENS.colorBorderAlert]: {
        [TYPE_OPTIONS.INFO]: theme.tokens.colors.colorPaletteBlue800,
        [TYPE_OPTIONS.SUCCESS]: '#39870C',
        [TYPE_OPTIONS.WARNING]: theme.tokens.colors.brandPrimary,
        [TYPE_OPTIONS.ERROR]: '#F02B41',
      },
    }

    // eslint-disable-next-line security/detect-object-injection
    return tokens[name][type]
  }

export const Container = styled.div`
  padding: ${(p) => p.theme.tokens.spacing05};
  background-color: ${getTypeToken(TOKENS.colorBackgroundAlert)};
  border-left: 5px solid ${getTypeToken(TOKENS.colorBorderAlert)};
`

export const Title = styled.p`
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  margin: 0;
`

export const Content = styled.p`
  margin: 0;
`
