// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export const TYPE_OPTIONS = {
  WARNING: 'warning',
  INFO: 'info',
  ERROR: 'error',
  SUCCESS: 'success',
}

export const TOKENS = {
  colorBackgroundAlert: 'colorBackgroundAlert',
  colorBorderAlert: 'colorBorderAlert',
}
