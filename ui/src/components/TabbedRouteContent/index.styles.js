// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { NavLink } from 'react-router-dom'
import { mediaQueries } from '@commonground/design-system'

export const Wrapper = styled.div`
  margin-top: ${(p) => p.theme.tokens.spacing06};
`

export const Header = styled.div`
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
`

export const Content = styled.section``

export const Tabs = styled.ul`
  display: flex;
  padding: 0;
  margin: 0;
  border-bottom: 1px solid ${(p) => p.theme.tokens.colorPaletteGray400};
`

export const Tab = styled.li`
  display: inline-block;
  color: #676d80;
  margin-bottom: -1px;
`

export const TabLink = styled(NavLink)`
  display: inline-block;
  font-size: 1em;

  padding: ${(p) => p.theme.tokens.spacing04} ${(p) => p.theme.tokens.spacing05};

  ${mediaQueries.smUp`
    padding: ${(p) => p.theme.tokens.spacing05} ${(p) =>
    p.theme.tokens.spacing06};
  `}

  &.active {
    color: #000;
    text-decoration: none;
    font-weight: ${(p) => p.theme.tokens.fontWeightBold};
    border: 1px solid transparent;
    border-color: ${(p) => {
      const { colorPaletteGray400, colorBackground } = p.theme.tokens
      return `${colorPaletteGray400} ${colorPaletteGray400} ${colorBackground}`
    }};
  }
`
