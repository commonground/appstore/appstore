// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { fireEvent, render, act } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'

import theme from '../../theme'
import ProductForm from './index'

const mockInitialValues = {
  name: 'naam',
  shortDescription: 'korte omschrijving',
  description: 'omschrijving',
  detailPageImageUrl: 'https://www.example.com/image.jpg',
  documentationUrl: 'https://www.example.com/docs',
  demoUrl: 'https://www.example.com/demo',
  bpmnProcessUrl: 'https://www.example.com/bmpn.jpg',
}

describe('the ProductForm', () => {
  describe('with initial values', () => {
    it('should prefill the form fields with the initial values', () => {
      const { getByTestId, getByLabelText } = render(
        <ThemeProvider theme={theme}>
          <ProductForm initialValues={mockInitialValues} />
        </ThemeProvider>,
      )

      expect(getByLabelText('Productnaam').value).toBe('naam')
      expect(getByLabelText('Korte omschrijving').value).toBe(
        'korte omschrijving',
      )
      expect(getByLabelText('Screenshot URL').value).toBe(
        'https://www.example.com/image.jpg',
      )

      expect(getByLabelText('Documentatie URL').value).toBe(
        'https://www.example.com/docs',
      )

      expect(getByLabelText('Demo URL (optioneel)').value).toBe(
        'https://www.example.com/demo',
      )

      expect(getByLabelText('BPMN procesplaat URL (optioneel)').value).toBe(
        'https://www.example.com/bmpn.jpg',
      )

      Object.keys(mockInitialValues)
        .filter(
          (key) =>
            ![
              'name',
              'shortDescription',
              'detailPageImageUrl',
              'documentationUrl',
              'demoUrl',
              'bpmnProcessUrl',
            ].includes(key),
        )
        .forEach((key) => {
          // eslint-disable-next-line security/detect-object-injection
          expect(getByTestId(`${key}-field`).value).toBe(mockInitialValues[key])
        })
    })

    it('should allow configuring the submit button text', () => {
      const { getByRole } = render(
        <ThemeProvider theme={theme}>
          <ProductForm submitButtonText="Opslaan" />
        </ThemeProvider>,
      )

      expect(getByRole('button').textContent).toBe('Opslaan')
    })

    it('should allow configuring the button info text', () => {
      const { getByTestId } = render(
        <ThemeProvider theme={theme}>
          <ProductForm buttonInfoText="Some random text" />
        </ThemeProvider>,
      )

      expect(getByTestId('button-info-text')).toHaveTextContent(
        'Some random text',
      )
    })
  })

  describe('submitting the form with valid fields', () => {
    let onSubmitHandlerSpy, renderResult
    beforeEach(() => {
      onSubmitHandlerSpy = jest.fn()
      renderResult = render(
        <ThemeProvider theme={theme}>
          <ProductForm
            initialValues={mockInitialValues}
            onSubmitHandler={onSubmitHandlerSpy}
          />
        </ThemeProvider>,
      )
    })

    it('should trigger the onSubmitHandler with the form values', async () => {
      const form = renderResult.getByTestId('form')
      await act(async () => {
        fireEvent.submit(form)
      })

      expect(onSubmitHandlerSpy).toHaveBeenCalledWith(mockInitialValues)
    })
  })

  describe('submitting the form with invalid fields', () => {
    let onSubmitHandlerSpy, renderResult
    beforeEach(() => {
      onSubmitHandlerSpy = jest.fn()
      renderResult = render(
        <ThemeProvider theme={theme}>
          <ProductForm onSubmitHandler={onSubmitHandlerSpy} />
        </ThemeProvider>,
      )
    })

    it('should not trigger the onSubmitHandler with the form values', async () => {
      const form = renderResult.getByTestId('form')
      await act(async () => {
        fireEvent.submit(form)
      })

      expect(onSubmitHandlerSpy).not.toHaveBeenCalled()
    })

    it('should display an error message for an invalid field', async () => {
      const form = renderResult.getByTestId('form')
      await act(async () => {
        fireEvent.submit(form)
      })

      const nameError = renderResult.getByTestId('error-name')
      expect(nameError).not.toBeNull()
    })
  })
})
