// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'

export const StyledButtonGroup = styled.div`
  display: flex;
  align-items: center;
`

export const StyledButtonInfoText = styled.div`
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray700};
  margin-left: ${(p) => p.theme.tokens.spacing06};
`
