// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { func, shape, string } from 'prop-types'
import * as Yup from 'yup'
import { Formik } from 'formik'
import { Button, TextInput, Fieldset, Label } from '@commonground/design-system'

import Field from '../../components/design-system-candidates/Field'
import FieldValidationMessage from '../../components/design-system-candidates/FieldValidationMessage'
import { StyledButtonGroup, StyledButtonInfoText } from './index.styles'

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .max(60, 'De maximale lengte is 60 karakters')
    .required('Dit veld is verplicht'),
  shortDescription: Yup.string()
    .max(80, 'De maximale lengte is 80 karakters')
    .required('Dit veld is verplicht'),
  description: Yup.string().required('Dit veld is verplicht'),
  detailPageImageUrl: Yup.string()
    .url('Ongeldige URL')
    .required('Dit veld is verplicht'),
  documentationUrl: Yup.string()
    .url('Ongeldige URL')
    .required('Dit veld is verplicht'),
  demoUrl: Yup.string().url('Ongeldige URL'),
  bpmnProcessUrl: Yup.string().url('Ongeldige URL'),
})

const ProductForm = ({
  initialValues,
  onSubmitHandler,
  submitButtonText,
  buttonInfoText,
  ...props
}) => {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={(values) => onSubmitHandler(values)}
    >
      {({ handleSubmit, errors, touched }) => (
        <form onSubmit={handleSubmit} data-testid="form" {...props}>
          <Fieldset>
            <TextInput name="name" size="m">
              Productnaam
            </TextInput>

            <TextInput
              name="shortDescription"
              size="l"
              placeholder="Omschrijf het product in één zin"
            >
              Korte omschrijving
            </TextInput>

            <Label htmlFor="description">Omschrijving</Label>
            <Field
              id="description"
              component="textarea"
              name="description"
              data-testid="description-field"
            />
            {errors.description && touched.description ? (
              <FieldValidationMessage data-testid="description-error">
                {errors.description}
              </FieldValidationMessage>
            ) : null}

            <TextInput
              name="detailPageImageUrl"
              placeholder="Link naar een screenshot van het product"
              size="l"
            >
              Screenshot URL
            </TextInput>

            <TextInput name="documentationUrl" size="l">
              Documentatie URL
            </TextInput>

            <TextInput name="demoUrl" size="l">
              Demo URL (optioneel)
            </TextInput>

            <TextInput
              name="bpmnProcessUrl"
              size="l"
              placeholder="Link naar een afbeelding van de procesplaat"
            >
              BPMN procesplaat URL (optioneel)
            </TextInput>
          </Fieldset>

          <StyledButtonGroup>
            <Button type="submit">{submitButtonText}</Button>
            {buttonInfoText && (
              <StyledButtonInfoText data-testid="button-info-text">
                {buttonInfoText}
              </StyledButtonInfoText>
            )}
          </StyledButtonGroup>
        </form>
      )}
    </Formik>
  )
}

ProductForm.propTypes = {
  onSubmitHandler: func,
  initialValues: shape({
    name: string.isRequired,
    shortDescription: string.isRequired,
    description: string.isRequired,
    detailPageImageUrl: string.isRequired,
    documentationUrl: string.isRequired,
    demoUrl: string,
    bpmnProcessUrl: string,
  }),
  submitButtonText: string,
  buttonInfoText: string,
}

ProductForm.defaultProps = {
  initialValues: {
    name: '',
    shortDescription: '',
    description: '',
    detailPageImageUrl: '',
    documentationUrl: '',
    demoUrl: '',
    bpmnProcessUrl: '',
  },
  submitButtonText: 'Product toevoegen',
  buttonInfoText: '',
}

export default ProductForm
