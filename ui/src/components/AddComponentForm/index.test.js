// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { act, fireEvent, render, waitFor } from '@testing-library/react'
import {
  LAYER_TYPE_PROCES,
  STATUS_GEPLAND,
  STATUS_GEWENST,
  DEVELOPED_BY_SELF,
  STATUS_BETA,
  STATUS_BRUIKBAAR,
  STATUS_UITGEFASEERD,
  LAYER_TYPE_INTERACTIE,
  REUSE_TYPE_ZELF_INSTALLEREN,
} from '../../vocabulary'
import AddComponentForm from './index'

describe('the AddComponentForm', () => {
  describe('with initial values', () => {
    it('should prefill the form fields with the initial values', () => {
      const { container, getByRole } = render(
        <AddComponentForm
          initialValues={{
            name: 'naam',
            description: 'omschrijving',
            status: STATUS_BETA,
            developedBy: DEVELOPED_BY_SELF,
            layerType: LAYER_TYPE_PROCES,
            organisationName: 'organisatie',
            contact: 'contact informatie',
            repositoryUrl: 'repository url',
            reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
          }}
        />,
      )

      expect(container.querySelector('#name').value).toBe('naam')
      expect(container.querySelector('#description').value).toBe('omschrijving')
      expect(
        container.querySelector('input[id="layerTypeProces"]').checked,
      ).toBe(true)
      expect(container.querySelector('#organisationName').value).toBe(
        'organisatie',
      )
      expect(container.querySelector('#contact').value).toBe(
        'contact informatie',
      )
      expect(container.querySelector('#repositoryUrl').value).toBe(
        'repository url',
      )
      expect(getByRole('button').textContent).toBe('Component toevoegen')
    })

    it('should allow configuring the submit button text', () => {
      const { getByRole } = render(
        <AddComponentForm
          initialValues={{
            name: 'naam',
            description: 'omschrijving',
            layerType: LAYER_TYPE_PROCES,
            status: STATUS_GEWENST,
            developedBy: DEVELOPED_BY_SELF,
            organisationName: 'organisatie',
            contact: 'contact informatie',
            repositoryUrl: 'repository url',
          }}
          submitButtonText="Opslaan"
        />,
      )

      expect(getByRole('button').textContent).toBe('Opslaan')
    })
  })

  describe('configure layout on status', () => {
    it('status gewenst', () => {
      const { container } = render(
        <AddComponentForm
          initialValues={{
            name: 'naam',
            description: 'omschrijving',
            layerType: LAYER_TYPE_PROCES,
            status: STATUS_GEWENST,
            developedBy: DEVELOPED_BY_SELF,
            organisationName: 'organisatie',
            contact: 'contact informatie',
            repositoryUrl: 'repository url',
          }}
        />,
      )
      expect(container.querySelector('#layer-type')).toBeInTheDocument()
      expect(container.querySelector('#name')).toBeInTheDocument()
      expect(container.querySelector('#description')).toBeInTheDocument()
      expect(container.querySelector('#status')).toBeInTheDocument()
      expect(container.querySelector('#developedBy')).toBeInTheDocument()
      expect(container.querySelector('#repositoryUrl')).toBeNull()
      expect(container.querySelector('#organisationName')).toBeNull()
    })
    it('status gepland', () => {
      const { container } = render(
        <AddComponentForm
          initialValues={{
            name: 'naam',
            description: 'omschrijving',
            status: STATUS_GEPLAND,
            layerType: LAYER_TYPE_PROCES,
            organisationName: 'organisatie',
            contact: 'contact informatie',
            repositoryUrl: 'repository url',
          }}
        />,
      )
      expect(container.querySelector('#layer-type')).toBeInTheDocument()
      expect(container.querySelector('#name')).toBeInTheDocument()
      expect(container.querySelector('#description')).toBeInTheDocument()
      expect(container.querySelector('#status')).toBeInTheDocument()
      expect(container.querySelector('#expected_quarter')).toBeInTheDocument()
      expect(container.querySelector('#expected_year')).toBeInTheDocument()
      expect(container.querySelector('#contact')).toBeInTheDocument()
      expect(container.querySelector('#organisationName')).toBeInTheDocument()
      expect(container.querySelector('#repositoryUrl')).toBeNull()
    })

    it('status beta', () => {
      const { container } = render(
        <AddComponentForm
          initialValues={{
            name: 'naam',
            description: 'omschrijving',
            status: STATUS_BETA,
            layerType: LAYER_TYPE_PROCES,
            organisationName: 'organisatie',
            contact: 'contact informatie',
            repositoryUrl: 'repository url',
          }}
        />,
      )
      expect(container.querySelector('#layer-type')).toBeInTheDocument()
      expect(container.querySelector('#name')).toBeInTheDocument()
      expect(container.querySelector('#description')).toBeInTheDocument()
      expect(container.querySelector('#status')).toBeInTheDocument()
      expect(container.querySelector('#expected_quarter')).toBeNull()
      expect(container.querySelector('#expected_year')).toBeNull()
      expect(container.querySelector('#contact')).toBeInTheDocument()
      expect(container.querySelector('#organisationName')).toBeInTheDocument()
      expect(container.querySelector('#repositoryUrl')).toBeInTheDocument()
    })

    it('status bruikbaar', () => {
      const { container } = render(
        <AddComponentForm
          initialValues={{
            name: 'naam',
            description: 'omschrijving',
            status: STATUS_BRUIKBAAR,
            layerType: LAYER_TYPE_PROCES,
            organisationName: 'organisatie',
            contact: 'contact informatie',
            repositoryUrl: 'repository url',
          }}
        />,
      )
      expect(container.querySelector('#layer-type')).toBeInTheDocument()
      expect(container.querySelector('#name')).toBeInTheDocument()
      expect(container.querySelector('#description')).toBeInTheDocument()
      expect(container.querySelector('#repositoryUrl')).toBeInTheDocument()
      expect(container.querySelector('#expected_quarter')).toBeNull()
      expect(container.querySelector('#expected_year')).toBeNull()
      expect(container.querySelector('#contact')).toBeInTheDocument()
      expect(container.querySelector('#organisationName')).toBeInTheDocument()
    })

    it('status uitgefaseerd', () => {
      const { container } = render(
        <AddComponentForm
          initialValues={{
            name: 'naam',
            description: 'omschrijving',
            status: STATUS_UITGEFASEERD,
            layerType: LAYER_TYPE_PROCES,
            organisationName: 'organisatie',
            contact: 'contact informatie',
            repositoryUrl: 'repository url',
          }}
        />,
      )
      expect(
        container.querySelector('input[id="layerTypeProces"]'),
      ).toBeInTheDocument()
      expect(container.querySelector('#name')).toBeInTheDocument()
      expect(container.querySelector('#description')).toBeInTheDocument()
      expect(container.querySelector('#repositoryUrl')).toBeInTheDocument()
      expect(container.querySelector('#expected_quarter')).toBeNull()
      expect(container.querySelector('#expected_year')).toBeNull()
      expect(container.querySelector('#organisationName')).toBeInTheDocument()
    })
  })

  describe('the layerType field', () => {
    it('should have a fixed set of options', () => {
      const { container, getByLabelText } = render(
        <AddComponentForm
          initialValues={{
            name: 'naam',
            description: 'omschrijving',
            layerType: LAYER_TYPE_PROCES,
            organisationName: 'organisatie',
            contact: 'contact informatie',
            repositoryUrl: 'repository url',
          }}
        />,
      )

      const layerTypes = container.querySelectorAll('[name="layerType"]')
      expect(layerTypes).toHaveLength(5)

      expect(layerTypes[0].checked).toBe(false)
      expect(getByLabelText('Interactie')).toBeTruthy()

      expect(layerTypes[1].checked).toBe(true)
      expect(getByLabelText('Proces')).toBeTruthy()

      expect(layerTypes[2].checked).toBe(false)
      expect(getByLabelText('Integratie')).toBeTruthy()

      expect(layerTypes[3].checked).toBe(false)
      expect(getByLabelText('Services')).toBeTruthy()

      expect(layerTypes[4].checked).toBe(false)
      expect(getByLabelText('Data')).toBeTruthy()
    })
  })

  describe('the status field', () => {
    it('should contain all of the status options', () => {
      const { container, getByLabelText } = render(
        <AddComponentForm
          initialValues={{
            name: 'naam',
            description: 'omschrijving',
            layerType: LAYER_TYPE_PROCES,
            status: STATUS_GEWENST,
            organisationName: 'organisatie',
            contact: 'contact informatie',
            repositoryUrl: 'repository url',
          }}
        />,
      )

      const statusList = container.querySelectorAll(`[name="status"]`)
      expect(statusList).toHaveLength(5)

      expect(statusList[0].checked).toBe(true)
      expect(getByLabelText('Gewenst')).toBeTruthy()

      expect(statusList[1].checked).toBe(false)
      expect(getByLabelText('Gepland')).toBeTruthy()

      expect(statusList[2].checked).toBe(false)
      expect(getByLabelText('Beta')).toBeTruthy()

      expect(statusList[3].checked).toBe(false)
      expect(getByLabelText('Bruikbaar')).toBeTruthy()

      expect(statusList[4].checked).toBe(false)
      expect(getByLabelText('Uitgefaseerd')).toBeTruthy()
    })
  })

  it("the form values of the onSubmitHandler for the status 'gewenst'", async () => {
    const onSubmitHandlerSpy = jest.fn()

    const { getByTestId, findByTestId } = render(
      <AddComponentForm
        onSubmitHandler={onSubmitHandlerSpy}
        initialValues={{
          name: 'N',
          description: 'O',
          contact: 'Contactinformatie',
          layerType: LAYER_TYPE_INTERACTIE,
          status: STATUS_GEWENST,
          reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
        }}
      />,
    )

    // invalid form - status gewenst requires other fields to be filled-in
    const formElement = getByTestId('form')
    await act(async () => {
      fireEvent.submit(formElement)
    })

    // assert the conditional fields show validation feedback
    const developedByError = await findByTestId('developed-by-error')
    expect(developedByError).not.toBeNull()

    // fill-in required fields
    const developedBySelfField = getByTestId('developed-by-self-field')
    fireEvent.click(developedBySelfField)

    // re-submit the valid form
    await act(async () => {
      fireEvent.submit(formElement)
    })

    expect(onSubmitHandlerSpy).toHaveBeenCalledWith({
      name: 'N',
      description: 'O',
      developedBy: 'self',
      layerType: 'interactie',
      contact: 'Contactinformatie',
      reuseType: 'zelf installeren',
      status: STATUS_GEWENST,
    })
  })

  // TODO: can be enabled again once https://gitlab.com/commonground/appstore/appstore/issues/151 is merged
  // eslint-disable-next-line jest/no-disabled-tests
  it.skip("the form values of the onSubmitHandler for the status 'gepland'", async () => {
    const onSubmitHandlerSpy = jest.fn()

    const { getByTestId, findByTestId } = render(
      <AddComponentForm
        onSubmitHandler={onSubmitHandlerSpy}
        initialValues={{
          name: 'N',
          description: 'O',
          contact: 'Contactinformatie',
          layerType: LAYER_TYPE_INTERACTIE,
          status: STATUS_GEPLAND,
          reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
          organisationName: '',
        }}
      />,
    )

    // invalid form - status gewenst requires other fields to be filled-in
    const formElement = getByTestId('form')
    fireEvent.submit(formElement)
    await waitFor()

    // assert the conditional fields show validation feedback
    const expectedDeliveryError = await findByTestId('expected-delivery-error')
    expect(expectedDeliveryError).not.toBeNull()

    const organisationNameError = await findByTestId('organisation-name-error')
    expect(organisationNameError).not.toBeNull()

    // fill-in required fields
    const expectedQuartedField = await findByTestId('expected_quarter_field')
    fireEvent.change(expectedQuartedField, { target: { value: 'Q1' } })
    const expectedYearField = await findByTestId('expected_year_field')
    fireEvent.change(expectedYearField, { target: { value: '2020' } })
    const organisationNameField = await findByTestId('organisation-name-field')
    fireEvent.change(organisationNameField, {
      target: { value: 'OrganisatieNaam' },
    })
    await waitFor()

    // re-submit the valid form
    fireEvent.submit(formElement)
    await waitFor()

    expect(onSubmitHandlerSpy).toHaveBeenCalledWith({
      name: 'N',
      description: 'O',
      expectedQuarter: 'Q1',
      expectedYear: '2020',
      organisationName: 'OrganisatieNaam',
      layerType: 'interactie',
      contact: 'Contactinformatie',
      reuseType: 'zelf installeren',
      status: STATUS_GEPLAND,
    })
  })
})

test("the form values of the onSubmitHandler for the status 'beta'", async () => {
  const onSubmitHandlerSpy = jest.fn()

  const { container, getByTestId, findByTestId } = render(
    <AddComponentForm
      onSubmitHandler={onSubmitHandlerSpy}
      initialValues={{
        name: 'N',
        description: 'O',
        contact: 'Contactinformatie',
        layerType: LAYER_TYPE_INTERACTIE,
        status: STATUS_BETA,
        reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
        repositoryUrl: '',
        organisationName: '',
      }}
    />,
  )

  // invalid form - status gewenst requires other fields to be filled-in
  const formElement = getByTestId('form')
  await act(async () => {
    fireEvent.submit(formElement)
  })

  // assert the conditional fields show validation feedback
  const repositoryUrlError = await findByTestId('repository-url-error')
  expect(repositoryUrlError).not.toBeNull()
  const organisationNameError = await findByTestId('error-organisationName')
  expect(organisationNameError).not.toBeNull()

  // fill-in required fields
  const repositoryUrlField = getByTestId('repository-url-field')
  fireEvent.change(repositoryUrlField, {
    target: { value: 'https://repo.com' },
  })
  const organizationNameField = getByTestId('organisation-name-field')
  await act(async () => {
    fireEvent.change(organizationNameField, {
      target: { value: 'OrganisatieNaam' },
    })
  })

  // re-submit the valid form
  await act(async () => {
    fireEvent.submit(formElement)
  })

  expect(
    container.querySelectorAll('p[class*="FieldValidationMessage"'),
  ).toHaveLength(0)

  expect(onSubmitHandlerSpy).toHaveBeenCalledWith({
    name: 'N',
    description: 'O',
    layerType: 'interactie',
    contact: 'Contactinformatie',
    reuseType: 'zelf installeren',
    organisationName: 'OrganisatieNaam',
    repositoryUrl: 'https://repo.com',
    status: STATUS_BETA,
  })
})

test("the form values of the onSubmitHandler for the status 'bruikbaar'", async () => {
  const onSubmitHandlerSpy = jest.fn()

  const { container, getByTestId, findByTestId } = render(
    <AddComponentForm
      onSubmitHandler={onSubmitHandlerSpy}
      initialValues={{
        name: 'N',
        description: 'O',
        contact: 'Contactinformatie',
        layerType: LAYER_TYPE_INTERACTIE,
        status: STATUS_BRUIKBAAR,
        reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
        repositoryUrl: '',
        organisationName: '',
      }}
    />,
  )

  // invalid form - status gewenst requires other fields to be filled-in
  const formElement = getByTestId('form')
  await act(async () => {
    fireEvent.submit(formElement)
  })

  // assert the conditional fields show validation feedback
  const repositoryUrlError = await findByTestId('repository-url-error')
  expect(repositoryUrlError).not.toBeNull()
  const organisationNameError = await findByTestId('error-organisationName')
  expect(organisationNameError).not.toBeNull()

  // fill-in required fields
  const repositoryUrlField = getByTestId('repository-url-field')
  fireEvent.change(repositoryUrlField, {
    target: { value: 'https://mockrepository.com' },
  })
  const organizationNameField = getByTestId('organisation-name-field')
  await act(async () => {
    fireEvent.change(organizationNameField, {
      target: { value: 'OrganisatieNaam' },
    })
  })

  // re-submit the valid form
  await act(async () => {
    fireEvent.submit(formElement)
  })

  expect(
    container.querySelectorAll('p[class*="FieldValidationMessage"'),
  ).toHaveLength(0)

  expect(onSubmitHandlerSpy).toHaveBeenCalledWith({
    name: 'N',
    description: 'O',
    layerType: 'interactie',
    contact: 'Contactinformatie',
    reuseType: 'zelf installeren',
    organisationName: 'OrganisatieNaam',
    repositoryUrl: 'https://mockrepository.com',
    status: STATUS_BRUIKBAAR,
  })
})

test("the form values of the onSubmitHandler for the status 'uitgefaseerd'", async () => {
  const onSubmitHandlerSpy = jest.fn()

  const { container, getByTestId, findByTestId } = render(
    <AddComponentForm
      onSubmitHandler={onSubmitHandlerSpy}
      initialValues={{
        name: 'N',
        description: 'O',
        contact: 'Contactinformatie',
        layerType: LAYER_TYPE_INTERACTIE,
        status: STATUS_BRUIKBAAR,
        reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
        repositoryUrl: '',
        organisationName: '',
      }}
    />,
  )

  // invalid form - status gewenst requires other fields to be filled-in
  const formElement = getByTestId('form')
  await act(async () => {
    fireEvent.submit(formElement)
  })

  // assert the conditional fields show validation feedback
  const repositoryUrlError = await findByTestId('repository-url-error')
  expect(repositoryUrlError).not.toBeNull()
  const organisationNameError = await findByTestId('error-organisationName')
  expect(organisationNameError).not.toBeNull()

  // fill-in required fields
  const repositoryUrlField = getByTestId('repository-url-field')
  fireEvent.change(repositoryUrlField, {
    target: { value: 'https://mockrepository.com' },
  })
  const organizationNameField = getByTestId('organisation-name-field')
  await act(async () => {
    fireEvent.change(organizationNameField, {
      target: { value: 'OrganisatieNaam' },
    })
  })

  // re-submit the valid form
  await act(async () => {
    fireEvent.submit(formElement)
  })

  expect(
    container.querySelectorAll('p[class*="FieldValidationMessage"'),
  ).toHaveLength(0)

  expect(onSubmitHandlerSpy).toHaveBeenCalledWith({
    name: 'N',
    description: 'O',
    layerType: 'interactie',
    contact: 'Contactinformatie',
    reuseType: 'zelf installeren',
    organisationName: 'OrganisatieNaam',
    repositoryUrl: 'https://mockrepository.com',
    status: STATUS_BRUIKBAAR,
  })
})

test('repository URL should only be validated after submit', async () => {
  const onSubmitHandlerSpy = jest.fn()

  const { getByTestId, queryByTestId, findByTestId } = render(
    <AddComponentForm
      onSubmitHandler={onSubmitHandlerSpy}
      initialValues={{
        name: 'N',
        description: 'O',
        contact: 'Contactinformatie',
        layerType: LAYER_TYPE_INTERACTIE,
        status: STATUS_BRUIKBAAR,
        reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
        repositoryUrl: '',
        organisationName: '',
      }}
    />,
  )

  const fieldRepostoryURL = await findByTestId('repository-url-field')
  await act(async () => {
    fireEvent.blur(fieldRepostoryURL)
  })

  let repositoryUrlError = queryByTestId('repository-url-error')
  expect(repositoryUrlError).toBeNull()

  const formElement = getByTestId('form')
  await act(async () => {
    fireEvent.submit(formElement)
  })

  repositoryUrlError = await findByTestId('repository-url-error')
  expect(repositoryUrlError).not.toBeNull()
})

// TODO: can be enabled again once https://gitlab.com/commonground/appstore/appstore/issues/151 is merged
// eslint-disable-next-line jest/no-disabled-tests
test.skip('expected quarter and year should only be validated after submit', async () => {
  const onSubmitHandlerSpy = jest.fn()

  const { getByTestId, queryByTestId, findByTestId } = render(
    <AddComponentForm
      onSubmitHandler={onSubmitHandlerSpy}
      initialValues={{
        name: 'N',
        description: 'O',
        contact: 'Contactinformatie',
        layerType: LAYER_TYPE_INTERACTIE,
        status: STATUS_GEPLAND,
        reuseType: REUSE_TYPE_ZELF_INSTALLEREN,
      }}
    />,
  )

  const expectedQYearField = await findByTestId('expected_year_field')
  const expectedQuarterField = await findByTestId('expected_quarter')
  fireEvent.change(expectedQYearField, { target: { value: '' } })
  fireEvent.change(expectedQuarterField, { target: { value: '' } })
  await waitFor()

  let expectedDeliveryError = queryByTestId('expected-delivery-error')
  expect(expectedDeliveryError).toBeNull()

  await waitFor()

  const formElement = getByTestId('form')
  fireEvent.submit(formElement)
  await waitFor()
  expectedDeliveryError = await findByTestId('expected-delivery-error')
  expect(expectedDeliveryError).not.toBeNull()
})
