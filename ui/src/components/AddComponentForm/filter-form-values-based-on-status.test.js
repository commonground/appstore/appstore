// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import {
  DEVELOP_BY_THIRDPARTY,
  DEVELOP_BY_UNKOWN,
  DEVELOPED_BY_SELF,
  STATUS_BETA,
  STATUS_BRUIKBAAR,
  STATUS_GEPLAND,
  STATUS_GEWENST,
  STATUS_UITGEFASEERD,
} from '../../vocabulary'
import filterValuesBasedOnStatus from './filter-form-values-based-on-status'

describe('get subset of values based on the status property', () => {
  describe('status is gewenst', () => {
    it('develop yourself', () => {
      const values = {
        status: STATUS_GEWENST,
        developedBy: DEVELOPED_BY_SELF,
        contact: 'contact',
        repositoryUrl: 'repository url',
        organisationName: 'organisation name',
      }

      expect(filterValuesBasedOnStatus(values)).toEqual({
        status: STATUS_GEWENST,
        developedBy: DEVELOPED_BY_SELF,
        contact: 'contact',
      })
    })

    it('do not develop yourself', () => {
      const values = {
        status: STATUS_GEWENST,
        developedBy: DEVELOP_BY_THIRDPARTY,
        repositoryUrl: 'repository url',
        organisationName: 'organisation name',
      }

      expect(filterValuesBasedOnStatus(values)).toEqual({
        status: STATUS_GEWENST,
        developedBy: DEVELOP_BY_THIRDPARTY,
      })
    })

    it('develop yourself unknown', () => {
      const values = {
        status: STATUS_GEWENST,
        developedBy: DEVELOP_BY_UNKOWN,
        repositoryUrl: 'repository url',
        organisationName: 'organisation name',
      }

      expect(filterValuesBasedOnStatus(values)).toEqual({
        status: STATUS_GEWENST,
        developedBy: DEVELOP_BY_UNKOWN,
      })
    })
  })

  it('status is gepland', () => {
    const values = {
      status: STATUS_GEPLAND,
      expectedQuarter: 'q1',
      expectedYear: '2020',
      contact: 'contact',
      organisationName: 'organisation name',
      developedBy: DEVELOPED_BY_SELF,
    }

    expect(filterValuesBasedOnStatus(values)).toEqual({
      status: STATUS_GEPLAND,
      expectedQuarter: 'q1',
      expectedYear: '2020',
      contact: 'contact',
      organisationName: 'organisation name',
    })
  })

  it('status is beta', () => {
    const values = {
      status: STATUS_BETA,
      expectedQuarter: 'q1',
      expectedYear: '2020',
      repositoryUrl: 'repositoryUrl',
      contact: 'contact',
      organisationName: 'organisation name',
      developedBy: DEVELOPED_BY_SELF,
    }

    expect(filterValuesBasedOnStatus(values)).toEqual({
      status: STATUS_BETA,
      contact: 'contact',
      repositoryUrl: 'repositoryUrl',
      organisationName: 'organisation name',
    })
  })

  it('status is bruikbaar', () => {
    const values = {
      status: STATUS_BRUIKBAAR,
      expectedQuarter: 'q1',
      expectedYear: '2020',
      repositoryUrl: 'repositoryUrl',
      contact: 'contact',
      organisationName: 'organisation name',
      developedBy: DEVELOPED_BY_SELF,
    }

    expect(filterValuesBasedOnStatus(values)).toEqual({
      status: STATUS_BRUIKBAAR,
      repositoryUrl: 'repositoryUrl',
      contact: 'contact',
      organisationName: 'organisation name',
    })
  })

  it('status is uitgefaseerd', () => {
    const values = {
      status: STATUS_UITGEFASEERD,
      expectedQuarter: 'q1',
      expectedYear: '2020',
      repositoryUrl: 'repositoryUrl',
      contact: 'contact',
      organisationName: 'organisation name',
      developedBy: DEVELOPED_BY_SELF,
    }

    expect(filterValuesBasedOnStatus(values)).toEqual({
      status: STATUS_UITGEFASEERD,
      contact: 'contact',
      repositoryUrl: 'repositoryUrl',
      organisationName: 'organisation name',
    })
  })
})
