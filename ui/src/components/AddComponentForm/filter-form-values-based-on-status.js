// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import {
  STATUS_BETA,
  STATUS_BRUIKBAAR,
  STATUS_GEPLAND,
  STATUS_GEWENST,
  STATUS_UITGEFASEERD,
} from '../../vocabulary'

const filterValuesBasedOnStatus = (values) => {
  if (values.status === STATUS_GEWENST) {
    delete values.repositoryUrl
    delete values.organisationName
    delete values.expectedQuarter
    delete values.expectedYear
  }

  if (values.status === STATUS_GEPLAND) {
    delete values.repositoryUrl
    delete values.developedBy
  }

  if (values.status === STATUS_BETA) {
    delete values.expectedQuarter
    delete values.expectedYear
    delete values.developedBy
  }

  if (values.status === STATUS_BRUIKBAAR) {
    delete values.expectedQuarter
    delete values.expectedYear
    delete values.developedBy
  }

  if (values.status === STATUS_UITGEFASEERD) {
    delete values.expectedQuarter
    delete values.expectedYear
    delete values.developedBy
  }

  return values
}

export default filterValuesBasedOnStatus
