// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { func, shape, string, oneOf, number } from 'prop-types'
import { Field, Formik } from 'formik'
import { Button, TextInput, Label, Fieldset } from '@commonground/design-system'

import { ThemeProvider } from 'styled-components/macro'
import * as Yup from 'yup'
import {
  LAYER_TYPE_PROCES,
  LAYER_TYPE_DATA,
  LAYER_TYPE_INTEGRATIE,
  LAYER_TYPE_INTERACTIE,
  LAYER_TYPE_SERVICES,
  STATUS_GEWENST,
  STATUS_GEPLAND,
  STATUS_BETA,
  STATUS_BRUIKBAAR,
  STATUS_UITGEFASEERD,
  DEVELOPED_BY_SELF,
  DEVELOPED_BY_THIRDPARTY,
  DEVELOPED_BY_UNKNOWN,
  REUSE_TYPE_ZELF_INSTALLEREN,
  REUSE_TYPE_LANDELIJKE_VOORZIENING,
  LAYER_TYPES,
  STATUSES,
} from '../../vocabulary'
import { FormikSelect } from '../design-system-candidates/Select'
import FieldValidationMessage from '../design-system-candidates/FieldValidationMessage'
import theme from '../../theme'
import {
  StyledField,
  FieldInfo,
  StyledLayerTypeOption,
  StyledLayerTypeOptionGroup,
  StyledExpectedDateGroup,
} from './index.styles'
import filterValuesBasedOnStatus from './filter-form-values-based-on-status'

const DEFAULT_INITIAL_VALUES = {
  layerType: '',
  name: '',
  description: '',
  status: '',
  repositoryUrl: '',
  reuseType: '',
  expectedYear: 2020,
  contact: '',
  developedBy: '',
  organisationName: '',
}

const validationSchema = Yup.object().shape({
  layerType: Yup.string().oneOf(LAYER_TYPES).required('Dit veld is verplicht'),
  name: Yup.string()
    .max(255, 'De maximale lengte is 255 karakters')
    .required('Dit veld is verplicht'),
  description: Yup.string().required('Dit veld is verplicht'),
  status: Yup.string().oneOf(STATUSES).required('Dit veld is verplicht'),
  repositoryUrl: Yup.string()
    .max(2000, 'De maximale lengte is 2000 karakters')
    .when('status', {
      is: (value) =>
        [STATUS_BETA, STATUS_BRUIKBAAR, STATUS_UITGEFASEERD].includes(value),
      then: Yup.string().url('Ongeldige URL').required('Dit veld is verplicht'),
    }),
  expectedYear: Yup.number()
    .nullable()
    .min(2020)
    .max(2100)
    .when('status', {
      is: STATUS_GEPLAND,
      then: Yup.number().required('Kies een jaar'),
    }),
  reuseType: Yup.string()
    .oneOf(
      [REUSE_TYPE_ZELF_INSTALLEREN, REUSE_TYPE_LANDELIJKE_VOORZIENING],
      'Dit veld is verplicht',
    )
    .required('Dit veld is verplicht'),
  contact: Yup.string()
    .max(255, 'De maximale lengte is 255 karakters')
    .required('Dit veld is verplicht'),
  developedBy: Yup.string()
    .nullable()
    .when('status', {
      is: STATUS_GEWENST,
      then: Yup.string()
        .required('Dit veld is verplicht')
        .oneOf(
          [DEVELOPED_BY_SELF, DEVELOPED_BY_THIRDPARTY, DEVELOPED_BY_UNKNOWN],
          'Dit veld is verplicht',
        ),
    }),
  organisationName: Yup.string()
    .max(255, 'De maximale lengte is 255 karakters')
    .when('status', {
      is: STATUS_GEWENST,
      otherwise: Yup.string().required('Dit veld is verplicht'),
    }),
})

const AddComponentForm = ({
  initialValues,
  onSubmitHandler,
  submitButtonText,
  ...props
}) => {
  return (
    <ThemeProvider theme={theme}>
      <Formik
        initialValues={{ ...DEFAULT_INITIAL_VALUES, ...initialValues }}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          return onSubmitHandler(filterValuesBasedOnStatus(values))
        }}
      >
        {({ handleSubmit, errors, touched, values, submitCount }) => (
          <form onSubmit={handleSubmit} data-testid="form" {...props}>
            <>
              <Label>Type</Label>
              <StyledLayerTypeOptionGroup id="layer-type">
                <StyledLayerTypeOption>
                  <Field
                    id="layerTypeInteractie"
                    name="layerType"
                    type="radio"
                    value={LAYER_TYPE_INTERACTIE}
                  />
                  <Label htmlFor="layerTypeInteractie">Interactie</Label>
                </StyledLayerTypeOption>
                <StyledLayerTypeOption>
                  <Field
                    id="layerTypeProces"
                    name="layerType"
                    type="radio"
                    value={LAYER_TYPE_PROCES}
                  />
                  <Label htmlFor="layerTypeProces">Proces</Label>
                </StyledLayerTypeOption>

                <StyledLayerTypeOption>
                  <Field
                    id="layerTypeIntegratie"
                    name="layerType"
                    type="radio"
                    value={LAYER_TYPE_INTEGRATIE}
                  />
                  <Label htmlFor="layerTypeIntegratie">Integratie</Label>
                </StyledLayerTypeOption>

                <StyledLayerTypeOption>
                  <Field
                    id="layerTypeServices"
                    name="layerType"
                    type="radio"
                    value={LAYER_TYPE_SERVICES}
                  />
                  <Label htmlFor="layerTypeServices">Services</Label>
                </StyledLayerTypeOption>

                <StyledLayerTypeOption>
                  <Field
                    id="layerTypeData"
                    name="layerType"
                    type="radio"
                    value={LAYER_TYPE_DATA}
                  />
                  <Label htmlFor="layerTypeData">Data</Label>
                </StyledLayerTypeOption>
              </StyledLayerTypeOptionGroup>
              {errors.layerType && touched.layerType ? (
                <FieldValidationMessage data-testid="layerType-error">
                  {errors.layerType}
                </FieldValidationMessage>
              ) : null}

              <TextInput
                id="name"
                name="name"
                size="m"
                data-testid="name-field"
              >
                Naam component
              </TextInput>

              <Label htmlFor="description">Omschrijving</Label>
              <StyledField
                id="description"
                component="textarea"
                name="description"
                data-testid="description-field"
                placeholder="Omschrijf de rol van dit component"
                className={
                  errors.description && touched.description ? 'invalid' : null
                }
              />
              {errors.description && touched.description ? (
                <FieldValidationMessage data-testid="description-error">
                  {errors.description}
                </FieldValidationMessage>
              ) : null}

              <Label>Status</Label>
              <StyledLayerTypeOptionGroup id="status">
                <StyledLayerTypeOption>
                  <Field
                    data-testid="status-field-gewenst"
                    id="statusGewenst"
                    name="status"
                    type="radio"
                    value={STATUS_GEWENST}
                  />
                  <Label htmlFor="statusGewenst">Gewenst</Label>
                </StyledLayerTypeOption>
                <StyledLayerTypeOption>
                  <Field
                    data-testid="status-field-gepland"
                    id="statusGepland"
                    name="status"
                    type="radio"
                    value={STATUS_GEPLAND}
                  />
                  <Label htmlFor="statusGepland">Gepland</Label>
                </StyledLayerTypeOption>
                <StyledLayerTypeOption>
                  <Field
                    data-testid="status-field-beta"
                    id="statusBeta"
                    name="status"
                    type="radio"
                    value={STATUS_BETA}
                  />
                  <Label htmlFor="statusBeta">Beta</Label>
                </StyledLayerTypeOption>
                <StyledLayerTypeOption>
                  <Field
                    data-testid="status-field-bruikbaar"
                    id="statusBruikbaar"
                    name="status"
                    type="radio"
                    value={STATUS_BRUIKBAAR}
                  />
                  <Label htmlFor="statusBruikbaar">Bruikbaar</Label>
                </StyledLayerTypeOption>
                <StyledLayerTypeOption>
                  <Field
                    data-testid="status-field-uitgefaseerd"
                    id="statusUitgefaseerd"
                    name="status"
                    type="radio"
                    value={STATUS_UITGEFASEERD}
                  />
                  <Label htmlFor="statusUitgefaseerd">Uitgefaseerd</Label>
                </StyledLayerTypeOption>
              </StyledLayerTypeOptionGroup>
              {errors.status && touched.status ? (
                <FieldValidationMessage data-testid="status-error">
                  {errors.status}
                </FieldValidationMessage>
              ) : null}

              {values.status === STATUS_BETA ||
              values.status === STATUS_BRUIKBAAR ||
              values.status === STATUS_UITGEFASEERD ? (
                <>
                  <Label htmlFor="repositoryUrl">Git Repository URL</Label>
                  <StyledField
                    id="repositoryUrl"
                    type="text"
                    name="repositoryUrl"
                    data-testid="repository-url-field"
                    className={
                      errors.repositoryUrl &&
                      touched.repositoryUrl &&
                      submitCount > 0
                        ? 'invalid'
                        : null
                    }
                  />
                  {errors.repositoryUrl &&
                  touched.repositoryUrl &&
                  submitCount > 0 ? (
                    <FieldValidationMessage data-testid="repository-url-error">
                      {errors.repositoryUrl}
                    </FieldValidationMessage>
                  ) : null}
                </>
              ) : null}

              {values.status === STATUS_GEPLAND ? (
                <>
                  <Label>Verwachte oplevering</Label>
                  <FieldInfo>Mag een globale schatting zijn</FieldInfo>
                  <StyledExpectedDateGroup>
                    <StyledField
                      type="number"
                      data-testid="expected_year_field"
                      id="expected_year"
                      name="expectedYear"
                      min="2020"
                      max="2100"
                    />
                    <Field
                      component={FormikSelect}
                      data-testid="expected_quarter_field"
                      id="expected_quarter"
                      name="expectedQuarter"
                      placeholder="Kwartaal (optioneel)"
                      options={[
                        { value: 'Q1', label: '1e kwartaal' },
                        { value: 'Q2', label: '2e kwartaal' },
                        { value: 'Q3', label: '3e kwartaal' },
                        { value: 'Q4', label: '4e kwartaal' },
                      ]}
                    />
                  </StyledExpectedDateGroup>
                  {errors.expectedYear && submitCount > 0 ? (
                    <FieldValidationMessage data-testid="expected-delivery-error">
                      {errors.expectedYear}
                    </FieldValidationMessage>
                  ) : null}
                </>
              ) : null}

              <Label>Type hergebruik</Label>
              <StyledLayerTypeOptionGroup>
                <StyledLayerTypeOption>
                  <Field
                    id="reuseTypeSelfInstallation"
                    name="reuseType"
                    type="radio"
                    value={REUSE_TYPE_ZELF_INSTALLEREN}
                  />
                  <Label htmlFor="reuseTypeSelfInstallation">
                    Zelf installeren
                  </Label>
                </StyledLayerTypeOption>
                <StyledLayerTypeOption>
                  <Field
                    id="reuseTypeNationalFacility"
                    name="reuseType"
                    type="radio"
                    value={REUSE_TYPE_LANDELIJKE_VOORZIENING}
                  />
                  <Label htmlFor="reuseTypeNationalFacility">
                    Landelijke voorziening
                  </Label>
                </StyledLayerTypeOption>
              </StyledLayerTypeOptionGroup>
              {errors.reuseType && touched.reuseType ? (
                <FieldValidationMessage data-testid="reuseType-error">
                  {errors.reuseType}
                </FieldValidationMessage>
              ) : null}

              <Label htmlFor="contact">
                Contact informatie (e-mail of telefoonnummer)
              </Label>
              <FieldInfo>
                Dit wordt getoond bij het component samen met je naam
              </FieldInfo>
              <StyledField
                id="contact"
                type="text"
                name="contact"
                data-testid="contact-field"
                className={errors.contact && touched.contact ? 'invalid' : null}
              />
              {errors.contact && touched.contact ? (
                <FieldValidationMessage data-testid="contact-error">
                  {errors.contact}
                </FieldValidationMessage>
              ) : null}

              {values.status === STATUS_GEWENST ? (
                <Fieldset>
                  <Label htmlFor="developedBy">
                    Ga je dit component zelf ontwikkelen?
                  </Label>
                  <StyledLayerTypeOptionGroup id="developedBy">
                    <StyledLayerTypeOption>
                      <Field
                        id="developedBySelf"
                        data-testid="developed-by-self-field"
                        name="developedBy"
                        type="radio"
                        value={DEVELOPED_BY_SELF}
                      />
                      <Label htmlFor="developedBySelf">Ja</Label>
                    </StyledLayerTypeOption>
                    <StyledLayerTypeOption>
                      <Field
                        id="developedByThirdParty"
                        name="developedBy"
                        type="radio"
                        value={DEVELOPED_BY_THIRDPARTY}
                      />
                      <Label htmlFor="developedByThirdParty">Nee</Label>
                    </StyledLayerTypeOption>
                    <StyledLayerTypeOption>
                      <Field
                        id="developedByOnbekend"
                        name="developedBy"
                        type="radio"
                        value={DEVELOPED_BY_UNKNOWN}
                      />
                      <Label htmlFor="developedByOnbekend">
                        Weet ik nog niet
                      </Label>
                    </StyledLayerTypeOption>
                  </StyledLayerTypeOptionGroup>

                  {errors.developedBy && submitCount > 0 ? (
                    <FieldValidationMessage data-testid="developed-by-error">
                      {errors.developedBy}
                    </FieldValidationMessage>
                  ) : null}
                </Fieldset>
              ) : null}
              {values.status !== STATUS_GEWENST ? (
                <Fieldset>
                  <TextInput
                    id="organisationName"
                    name="organisationName"
                    size="m"
                    data-testid="organisation-name-field"
                  >
                    Welke organistatie is eigenaar van dit component?
                  </TextInput>
                </Fieldset>
              ) : null}
            </>

            <Button type="submit">{submitButtonText}</Button>
          </form>
        )}
      </Formik>
    </ThemeProvider>
  )
}

AddComponentForm.propTypes = {
  onSubmitHandler: func,
  initialValues: shape({
    name: string,
    description: string,
    layerType: string,
    contact: string,
    expectedYear: number,
    status: oneOf([
      STATUS_GEWENST,
      STATUS_BRUIKBAAR,
      STATUS_GEPLAND,
      STATUS_UITGEFASEERD,
      STATUS_BETA,
    ]),
    developedBy: oneOf([
      DEVELOPED_BY_UNKNOWN,
      DEVELOPED_BY_SELF,
      DEVELOPED_BY_THIRDPARTY,
    ]),
    reuseType: oneOf([
      REUSE_TYPE_ZELF_INSTALLEREN,
      REUSE_TYPE_LANDELIJKE_VOORZIENING,
    ]),
  }),
  submitButtonText: string,
}

AddComponentForm.defaultProps = {
  initialValues: DEFAULT_INITIAL_VALUES,
  submitButtonText: 'Component toevoegen',
}

export default AddComponentForm
