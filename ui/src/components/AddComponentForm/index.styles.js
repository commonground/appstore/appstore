// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Field } from 'formik'
import Color from 'color'

export const Label = styled.label`
  display: block;
  word-break: keep-all;
  margin-bottom: ${(p) => p.theme.tokens.spacing01};
  margin-top: ${(p) => p.theme.tokens.spacing06};
`

export const FieldInfo = styled.small`
  display: block;
  word-break: keep-all;
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  color: ${(p) => p.theme.tokens.colors.colorPaletteGray700};
`

export const StyledField = styled(Field)`
  display: block;
  width: 100%;
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  font-family: 'Source Sans Pro', sans-serif;
  padding: ${(p) => p.theme.tokens.spacing04};
  color: ${(p) => p.theme.tokens.colors.colorText};
  border: 1px solid ${(p) => p.theme.tokens.colors.colorPaletteGray500};
  outline: none;
  line-height: ${(p) => p.theme.tokens.lineHeightText};

  &:focus {
    padding: calc(${(p) => p.theme.tokens.spacing04} - 1px);
    border: 2px solid ${(p) => p.theme.tokens.colors.colorPaletteBlue700};
  }

  &:placeholder {
    color: ${(p) => p.theme.tokens.colors.colorPaletteGray600};
  }

  &.invalid {
    padding: calc(${(p) => p.theme.tokens.spacing04} - 1px);
    border: 2px solid ${(p) => p.theme.tokens.colors.colorAlertError};
  }
`

export const StyledLayerTypeOptionGroup = styled.div`
  border: 1px solid #afb2b7;
  display: flex;
`

export const StyledLayerTypeOption = styled.div`
  flex: 1 1 auto;
  font-family: 'Source Sans Pro', sans-serif;
  display: inline-block;
  color: ${(p) => p.theme.tokens.colors.colorText};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  position: relative;
  text-align: center;

  &:not(:last-child) {
    border-right: 1px solid #afb2b7;
  }

  label {
    background-color: ${(p) => p.theme.tokens.colors.brandSecondary1};
    border: 0 none;
    cursor: pointer;
    margin: 0;
    white-space: normal;
    height: 100%;
    padding: ${(p) => p.theme.tokens.spacing04}
      ${(p) => p.theme.tokens.spacing05};
    box-shadow: inset 0 -3px 0 ${(p) => Color(p.theme.tokens.colors.colorPaletteGray900).alpha(0.25).hsl().string()};
  }

  input {
    position: absolute;
    opacity: 0;

    &:checked + label {
      background-color: ${(p) => p.theme.tokens.colors.colorPaletteBlue900};
      color: white;
      box-shadow: inset 0 3px 0
        ${(p) =>
          Color(p.theme.tokens.colors.colorPaletteGray900)
            .alpha(0.25)
            .hsl()
            .string()};
    }

    &:focus + label {
      background-color: ${(p) => p.theme.tokens.colors.colorPaletteBlue800};
    }
  }
`
export const StyledExpectedDateGroup = styled.div`
  display: flex;
  > * {
    flex: 1;
  }
  #expected_quarter {
    margin: 0 ${(p) => p.theme.tokens.spacing05};
  }
  #expected_year {
    margin: 0 0;
  }
`

export const Fieldset = styled.fieldset`
  border: 0 none;
  padding: 0 0 3rem 0;
`
