// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { shape, string, func } from 'prop-types'

import { STATUSES } from '../../vocabulary'
import Search from '../design-system-candidates/Search'
import { Select } from '../design-system-candidates/Select'
import { SearchRow, SearchCol } from './index.styles'

const statusOptions = STATUSES.map((status) => ({
  label: `${status.substring(0, 1).toUpperCase()}${status.substring(1)}`,
  value: status,
}))

const ComponentSearch = ({
  filters,
  onQueryChanged,
  onStatusChanged,
  searchTestId,
}) => {
  const { query, status } = filters
  const defaultValue = statusOptions.find((option) => option.value === status)

  return (
    <SearchRow>
      <SearchCol width={[1, 1 / 2]}>
        <Search
          inputProps={{ value: query, placeholder: 'Zoek componenten' }}
          onQueryChanged={onQueryChanged}
          data-testid={searchTestId}
        />
      </SearchCol>
      <SearchCol width={[1, 1 / 4]}>
        <Select
          name="status-filter"
          options={statusOptions}
          onChange={onStatusChanged}
          placeholder="Alleen met de status…"
          defaultValue={defaultValue}
          isClearable
        />
      </SearchCol>
    </SearchRow>
  )
}

ComponentSearch.propTypes = {
  filters: shape({
    query: string.isRequired,
    status: string.isRequired,
  }),
  onQueryChanged: func.isRequired,
  onStatusChanged: func.isRequired,
  searchTestId: string,
}

ComponentSearch.defaultProps = {
  searchTestId: 'search-box',
}

export default ComponentSearch
