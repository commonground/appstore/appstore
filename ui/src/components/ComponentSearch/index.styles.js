// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { mediaQueries } from '@commonground/design-system'

import { Row, Col } from '../design-system-candidates/Grid'

export const SearchRow = styled(Row)`
  display: flex;
  flex-wrap: wrap;
  margin: ${(p) =>
    `${p.theme.tokens.spacing07} -${p.theme.tokens.spacing03} ${p.theme.tokens.spacing05}`};

  ${mediaQueries.smUp`
    margin-bottom: ${(p) => p.theme.tokens.spacing09};
  `}
`

export const SearchCol = styled(Col)`
  padding: 0 ${(p) => p.theme.tokens.spacing03};

  ${mediaQueries.xs`
    margin-bottom: ${(p) => p.theme.tokens.spacing05}
  `}
`
