// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import selectEvent from 'react-select-event'

import { STATUSES } from '../../vocabulary'
import { renderWithProviders, fireEvent } from '../../test-utils'
import ComponentSearch from './index'

describe('ComponentSearch', () => {
  const filters = { query: '', status: '' }
  let queryChangedMock
  let statusChangedMock
  let rendered

  beforeEach(() => {
    queryChangedMock = jest.fn()
    statusChangedMock = jest.fn()

    rendered = renderWithProviders(
      <ComponentSearch
        filters={filters}
        onQueryChanged={queryChangedMock}
        onStatusChanged={statusChangedMock}
      />,
    )
  })

  it('the component should render', () => {
    const { container, getByTestId } = rendered

    const searchBox = getByTestId('search-box')
    expect(searchBox).toBeInTheDocument()
    expect(searchBox.querySelector('input')).toHaveAttribute(
      'placeholder',
      'Zoek componenten',
    )

    expect(
      container.querySelector('input[name="status-filter"]'),
    ).toBeInTheDocument()
  })

  it('typing a search string should call expected function', () => {
    const { getByTestId } = rendered

    const searchInput = getByTestId('search-box').querySelector('input')
    fireEvent.change(searchInput, { target: { value: 'api' } })

    expect(queryChangedMock).toHaveBeenCalledWith('api')
  })

  // TODO: test changing Select - https://gitlab.com/commonground/appstore/appstore/issues/151
  // eslint-disable-next-line jest/no-disabled-tests
  it.skip('choosing a status should call expected function', async () => {
    const { container } = rendered

    const statusSelect = container.querySelector('#react-select-2-input')

    await selectEvent.create(statusSelect, STATUSES[1])

    expect(statusChangedMock).toHaveBeenCalledWith(STATUSES[1])
  })
})
