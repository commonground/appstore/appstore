// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import '@testing-library/jest-dom/extend-expect'

import { renderWithProviders, fireEvent } from '../../test-utils'
import ProductStatusToggleButton from './index'

describe('viewing a published component', () => {
  let renderResult
  const setProduct = jest.fn()
  const updateHandler = jest.fn().mockResolvedValue({
    id: 43,
    name: 'Verhuizen',
    detailPageImageUrl: 'http://example.com/verhuizen.png',
    shortDescription: 'Verhuis je inwoner',
    isPublished: false,
  })

  const publishedDummyProduct = {
    id: 43,
    name: 'Verhuizen',
    detailPageImageUrl: 'http://example.com/verhuizen.png',
    shortDescription: 'Verhuis je inwoner',
    isPublished: true,
  }

  beforeEach(() => {
    renderResult = renderWithProviders(
      <ProductStatusToggleButton
        product={publishedDummyProduct}
        setProduct={setProduct}
        updateHandler={updateHandler}
      />,
    )
  })

  it('should show a depublish button', () => {
    expect(renderResult.getByTestId('toggle-button')).toHaveTextContent(
      'Depubliceren',
    )
  })

  describe('clicking the depublish button', () => {
    it('should call the repository update method', () => {
      fireEvent.click(renderResult.getByTestId('toggle-button'))
      expect(updateHandler).toHaveBeenCalledWith(43, { isPublished: false })
    })

    it('should call the setProduct method', () => {
      expect(setProduct).toHaveBeenCalledWith({
        id: 43,
        name: 'Verhuizen',
        detailPageImageUrl: 'http://example.com/verhuizen.png',
        shortDescription: 'Verhuis je inwoner',
        isPublished: false,
      })
    })
  })
})

describe('viewing an unpublished component', () => {
  let renderResult
  const setProduct = jest.fn()
  const updateHandler = jest.fn().mockResolvedValue({
    id: 43,
    name: 'Verhuizen',
    detailPageImageUrl: 'http://example.com/verhuizen.png',
    shortDescription: 'Verhuis je inwoner',
    isPublished: true,
  })

  const unpublishedDummyProduct = {
    id: 43,
    name: 'Verhuizen',
    detailPageImageUrl: 'http://example.com/verhuizen.png',
    shortDescription: 'Verhuis je inwoner',
    isPublished: false,
  }

  beforeEach(() => {
    renderResult = renderWithProviders(
      <ProductStatusToggleButton
        product={unpublishedDummyProduct}
        setProduct={setProduct}
        updateHandler={updateHandler}
      />,
    )
  })

  it('should show a publish button', () => {
    expect(renderResult.getByTestId('toggle-button')).toHaveTextContent(
      'Publiceren',
    )
  })

  describe('clicking the depublish button', () => {
    it('should call the repository update method', () => {
      fireEvent.click(renderResult.getByTestId('toggle-button'))
      expect(updateHandler).toHaveBeenCalledWith(43, { isPublished: true })
    })

    it('should call the setProduct method', () => {
      expect(setProduct).toHaveBeenCalledWith({
        id: 43,
        name: 'Verhuizen',
        detailPageImageUrl: 'http://example.com/verhuizen.png',
        shortDescription: 'Verhuis je inwoner',
        isPublished: true,
      })
    })
  })
})
