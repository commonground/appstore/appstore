// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { func, object } from 'prop-types'
import ProductRepository from '../../domain/product-repository'
import { StyledButton } from './index.styles'

const ProductStatusToggleButton = ({ product, updateHandler, setProduct }) => {
  const [isPublished, setIsPublished] = useState(product.isPublished)

  const togglePublished = async () => {
    const newPublishedState = !isPublished

    // perform an optimistic update
    setIsPublished(newPublishedState)

    try {
      const updatedProduct = await updateHandler(product.id, {
        isPublished: newPublishedState,
      })

      setProduct(updatedProduct)
    } catch (e) {
      console.error(e)

      // revert optimistic update
      setIsPublished(!newPublishedState)
    }
  }

  return (
    <StyledButton
      type="button"
      variant="secondary"
      onClick={togglePublished}
      data-testid="toggle-button"
    >
      {isPublished ? 'Depubliceren' : 'Publiceren'}
    </StyledButton>
  )
}

ProductStatusToggleButton.propTypes = {
  product: object.isRequired,
  setProduct: func,
  updateHandler: func,
}

ProductStatusToggleButton.defaultProps = {
  updateHandler: ProductRepository.update,
}

export default ProductStatusToggleButton
