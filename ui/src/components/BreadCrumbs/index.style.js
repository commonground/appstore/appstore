// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Wrapper = styled.p`
  margin-bottom: ${(p) => p.theme.tokens.spacing08};

  & > svg {
    margin-bottom: 2px;
  }
`
