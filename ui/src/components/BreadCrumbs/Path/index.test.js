// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'

import { ThemeProvider } from 'styled-components/macro'
import theme from '../../../theme'
import Path from './index'

test('renders expected result', () => {
  const { container } = render(
    <ThemeProvider theme={theme}>
      <Path path={['a', 'b', 'c']} />
    </ThemeProvider>,
  )

  expect(container).toHaveTextContent('a / b / c')
})
