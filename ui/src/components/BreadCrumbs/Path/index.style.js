// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Slash = styled.span`
  margin: 0 ${(p) => p.theme.tokens.spacing02};
`
