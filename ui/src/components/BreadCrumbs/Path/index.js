// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { arrayOf, string } from 'prop-types'

import { Slash } from './index.style'

const CategoryPath = ({ path }) =>
  path
    .map((item) => <span key={item}>{item}</span>)
    .reduce((prev, curr, i) => [prev, <Slash key={i}> / </Slash>, curr])

CategoryPath.propTypes = {
  path: arrayOf(string).isRequired,
}

export default CategoryPath
