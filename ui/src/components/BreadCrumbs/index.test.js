// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'

import { ThemeProvider } from 'styled-components/macro'
import theme from '../../theme'
import BreadCrumbs from './index'

test('renders expected result', () => {
  const { container } = render(
    <ThemeProvider theme={theme}>
      <BreadCrumbs>
        <BreadCrumbs.Path path={['a', 'b', 'c']} />
      </BreadCrumbs>
    </ThemeProvider>,
  )

  expect(container).toBeTruthy()
})
