// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { node, oneOfType, arrayOf } from 'prop-types'

import Path from './Path'
import { Wrapper } from './index.style'

const BreadCrumbs = ({ children }) => <Wrapper>{children}</Wrapper>

BreadCrumbs.propTypes = {
  children: oneOfType([node, arrayOf(node)]),
}

BreadCrumbs.Path = Path

export default BreadCrumbs
