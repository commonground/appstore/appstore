// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { useHistory } from 'react-router-dom'
import React from 'react'
import { Drawer } from '@commonground/design-system'
import { oneOf, string } from 'prop-types'
import { LAYER_TYPES } from '../../vocabulary'
import ComponentSelect from '../ComponentSelect'

const ComponentSelectPane = ({ parentUrl, name, layerType }) => {
  const history = useHistory()

  const close = () => history.push(parentUrl)

  return (
    <Drawer closeHandler={() => {}}>
      <ComponentSelect
        name={name}
        layerType={layerType}
        onSelectHandler={close}
      />
    </Drawer>
  )
}
ComponentSelectPane.propTypes = {
  name: string.isRequired,
  layerType: oneOf(LAYER_TYPES).isRequired,
  parentUrl: string.isRequired,
}

export default ComponentSelectPane
