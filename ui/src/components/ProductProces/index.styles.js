// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const ProcesImage = styled.img`
  display: block;
  width: 100%;
  height: 250px;
`
