// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { shape, string } from 'prop-types'

import { ProcesImage } from './index.styles'

const ProductProces = ({ product }) => {
  return (
    <div>
      {product.bpmnProcessUrl ? (
        <ProcesImage
          data-testid="bpmn-image"
          src={product.bpmnProcessUrl}
          alt={`BPMN weergave van ${product.name}`}
        />
      ) : (
        <p data-testid="no-bpmn-message">
          Er is geen processweergave beschikbaar.
        </p>
      )}
    </div>
  )
}
ProductProces.propTypes = {
  product: shape({
    bpmnProcessUrl: string,
  }).isRequired,
}

export default ProductProces
