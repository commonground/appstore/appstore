// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'

import { ThemeProvider } from 'styled-components'
import theme from '../../theme'
import ProductProces from './index'

const productWithBpmn = { bpmnProcessUrl: 'https://www.duck.com' }
const productWithoutBpmn = {}

describe('the product contains a bpmn process', () => {
  it('the image is shown', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <ProductProces product={productWithBpmn} />
      </ThemeProvider>,
    )
    expect(getByTestId('bpmn-image')).toBeTruthy()
  })
})

describe('the product does not contain a bpmn process', () => {
  it('an informal message is shown', () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <ProductProces product={productWithoutBpmn} />
      </ThemeProvider>,
    )
    expect(getByTestId('no-bpmn-message')).toBeTruthy()
  })
})
