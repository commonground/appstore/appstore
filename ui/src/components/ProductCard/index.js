// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { string, number, bool } from 'prop-types'

import { Card, StyledIconEyeOff } from './index.styles'

const ProductCard = ({ id, name, shortDescription, isPublished, ...props }) => {
  let title = `${name}`
  if (!isPublished) title += ' - Product is niet gepubliceerd'

  return (
    <Card to={`/producten/${id}`} title={title} {...props}>
      <Card.Header>
        <Card.Title data-testid="title">{name}</Card.Title>
        {!isPublished && <StyledIconEyeOff data-testid="unpublished-icon" />}
      </Card.Header>
      <Card.Body data-testid="body">{shortDescription}</Card.Body>
    </Card>
  )
}

ProductCard.propTypes = {
  id: number.isRequired,
  name: string.isRequired,
  shortDescription: string.isRequired,
  isPublished: bool.isRequired,
}

export default ProductCard
