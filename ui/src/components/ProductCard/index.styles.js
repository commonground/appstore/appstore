// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components/macro'
import { Link } from 'react-router-dom'
import Color from 'color'

import { IconEyeOff } from '../../icons'

export const Card = styled(Link)`
  display: block;
  width: 100%;
  height: 100%;
  text-decoration: none;
  position: relative;
  padding: ${(p) => p.theme.tokens.spacing06};
  background-color: ${(p) => p.theme.tokens.colors.colorPaletteGray100};
  box-shadow: inset 0 -3px 0 ${(p) => Color(p.theme.tokens.colors.colorPaletteGray900).alpha(0.25).hsl().string()};
  &:hover {
    background: ${(p) => p.theme.tokens.colors.colorPaletteGray200};
  }
`

Card.Header = styled.div`
  display: flex;
  justify-content: space-between;
`

Card.Title = styled.h2`
  margin: 0;
  color: ${(p) => p.theme.tokens.colors.colorText};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
  margin-bottom: ${(p) => p.theme.tokens.spacing04};
`

Card.Icon = styled.img`
  max-height: 16px;
  margin: 5px;
`

Card.Body = styled.div`
  height: 3em;
  color: ${(p) => p.theme.tokens.colors.colorText};
`

export const StyledIconEyeOff = styled(IconEyeOff)`
  max-height: 16px;
  margin: 5px;
  fill: ${(p) => p.theme.tokens.colorPaletteGray600};
`
