// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { render } from '@testing-library/react'
import { MemoryRouter as Router } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'

import theme from '../../theme'
import ProductCard from './index'

describe('Unpublished product', () => {
  let renderResult

  beforeEach(() => {
    const dummyProduct = {
      id: 42,
      name: 'Drukte',
      shortDescription: 'Meet de drukte',
      isPublished: false,
    }

    renderResult = render(
      <ThemeProvider theme={theme}>
        <Router>
          <ProductCard {...dummyProduct} data-testid="product" />
        </Router>
      </ThemeProvider>,
    )
  })

  it('should link to its detail page', async () => {
    const product = renderResult.getByTestId('product')
    expect(product.getAttribute('href')).toEqual('/producten/42')
  })

  it('should contain the name in a header', async () => {
    const title = renderResult.getByTestId('title')
    expect(title.textContent).toEqual('Drukte')
  })

  it('contains the text in a block', async () => {
    const title = renderResult.getByTestId('body')
    expect(title.textContent).toEqual('Meet de drukte')
  })

  it('should display the unpublished icon', async () => {
    const icon = renderResult.getByTestId('unpublished-icon')
    expect(icon).toBeTruthy()
  })
})

describe('Published product', () => {
  let renderResult

  beforeEach(() => {
    const dummyProduct = {
      id: 42,
      name: 'Drukte',
      shortDescription: 'Meet de drukte',
      isPublished: true,
    }

    renderResult = render(
      <ThemeProvider theme={theme}>
        <Router>
          <ProductCard {...dummyProduct} data-testid="product" />
        </Router>
      </ThemeProvider>,
    )
  })

  it('should not display the unpublished icon', async () => {
    const icon = renderResult.queryByTestId('unpublished-icon')
    expect(icon).toBeNull()
  })
})
