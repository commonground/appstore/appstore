// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { StaticRouter as Router, Route } from 'react-router-dom'
import { renderWithProviders } from '../../test-utils'

import ComponentDetailPane from './index'

jest.mock('../ComponentDetails', () => () => (
  <div data-testid="component-details">a</div>
))

test('display component details', async () => {
  const getComponentByIdSpy = jest.fn().mockResolvedValue({})

  const { findByTestId } = renderWithProviders(
    <Router location="/componenten/42">
      <Route path="/componenten/:id">
        <ComponentDetailPane getComponentById={getComponentByIdSpy} />
      </Route>
    </Router>,
  )

  expect(await findByTestId('component-details')).toBeTruthy()
  expect(getComponentByIdSpy).toHaveBeenCalledWith('42')
})

test('fetching a non-existing component', async () => {
  const getComponentByIdSpy = jest
    .fn()
    .mockRejectedValue(new Error('not found'))

  const { findByTestId } = renderWithProviders(
    <Router location="/componenten/42">
      <Route path="/componenten/:id">
        <ComponentDetailPane getComponentById={getComponentByIdSpy} />
      </Route>
    </Router>,
  )

  const message = await findByTestId('error-message')
  expect(message).toBeTruthy()
  expect(message.textContent).toBe(
    'Kan het component niet vindenHet component kan niet gevonden worden.',
  )
})

test('fetching component details fails for an unknown reason', async () => {
  const getComponentByIdSpy = jest
    .fn()
    .mockRejectedValue(new Error('arbitrary reason'))

  const { findByTestId } = renderWithProviders(
    <Router location="/componenten/42">
      <Route path="/componenten/:id">
        <ComponentDetailPane getComponentById={getComponentByIdSpy} />
      </Route>
    </Router>,
  )

  const message = await findByTestId('error-message')
  expect(message).toBeTruthy()
  expect(message.textContent).toBe(
    'Oeps, er ging iets foutEr ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
  )
})
