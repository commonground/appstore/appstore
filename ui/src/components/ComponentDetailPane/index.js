// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useEffect, useState } from 'react'
import { func, string } from 'prop-types'
import { useParams, useHistory } from 'react-router-dom'

import { Drawer } from '@commonground/design-system'
import ComponentRepository from '../../domain/component-repository'
import Alert from '../design-system-candidates/Alert'
import ComponentDetails from '../ComponentDetails'
import errorToUserFriendlyMessage from './error-to-user-friendly-message'

const ComponentDetailPane = ({ getComponentById, parentUrl }) => {
  const { id } = useParams()
  const history = useHistory()
  const [component, setComponent] = useState(null)
  const [loading, setLoading] = useState(true)
  const [errorMessage, setErrorMessage] = useState(null)

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      setErrorMessage(null)

      try {
        const component = await getComponentById(id)
        setComponent(component)
      } catch (e) {
        setErrorMessage(errorToUserFriendlyMessage(e.message))
      }
      setLoading(false)
    }

    fetchData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id])

  const close = () => history.push(parentUrl)

  return !loading ? (
    <Drawer closeHandler={close}>
      {component ? (
        <ComponentDetails component={component} />
      ) : errorMessage ? (
        <Alert
          type="error"
          data-testid="error-message"
          title={errorMessage.title}
        >
          {errorMessage.content}
        </Alert>
      ) : null}
    </Drawer>
  ) : null
}

ComponentDetailPane.propTypes = {
  getComponentById: func.isRequired,
  parentUrl: string.isRequired,
}

ComponentDetailPane.defaultProps = {
  getComponentById: ComponentRepository.getById,
  parentUrl: '/componenten',
}

export default ComponentDetailPane
