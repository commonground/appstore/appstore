// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
const errorToUserFriendlyMessage = (error) => {
  const errorMessages = {
    'not found': {
      title: 'Kan het component niet vinden',
      content: 'Het component kan niet gevonden worden.',
    },
    unexpected: {
      title: 'Oeps, er ging iets fout',
      content:
        'Er ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
    },
  }

  /* eslint-disable security/detect-object-injection */
  if (errorMessages[error] !== undefined) {
    return errorMessages[error]
  }
  /* eslint-enable security/detect-object-injection */

  return errorMessages.unexpected
}

export default errorToUserFriendlyMessage
