// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import errorToUserFriendlyMessage from './error-to-user-friendly-message'

describe('convert error to user friendly message', () => {
  describe('for a known error message', () => {
    it('should return the error message for invalid user input', () => {
      expect(errorToUserFriendlyMessage('not found')).toEqual({
        title: 'Kan het component niet vinden',
        content: 'Het component kan niet gevonden worden.',
      })
    })
  })

  describe('with an unknown error message', () => {
    it('should return the unexpected error message', () => {
      expect(errorToUserFriendlyMessage('somerandomerrorcode1337')).toEqual({
        title: 'Oeps, er ging iets fout',
        content:
          'Er ging onverwachts iets fout bij ons. Probeer het later opnieuw.',
      })
    })
  })
})
