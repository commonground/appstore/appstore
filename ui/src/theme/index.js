// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { defaultTheme } from '@commonground/design-system'

export const breakpoints = defaultTheme.breakpoints

const tokens = {
  ...defaultTheme.tokens,

  containerWidth: '992px',

  lineHeightText: '150%',

  fontWeightRegular: '500',
  fontWeightSemiBold: '600',
  fontWeightBold: '700',

  fontSizeSmall: '0.875rem',
  fontSizeMedium: '1rem',
  fontSizeLarge: '1.125rem',
  fontSizeXLarge: '1.5rem',
  fontSizeXXLarge: '2rem',

  shadowDropDown: '0 0 0 2px hsla(0,0%,0%,0.12), 0 4px 10px hsla(0,0%,0%,0.24)',

  colors: {
    // Brand
    brandPrimary: '#FFBC2C',
    brandSecondary1: '#E0E4EA',
    brandSecondary2: '#474E57',

    // Grays
    colorPaletteGray50: '#FAFAFA',
    colorPaletteGray100: '#F5F5F5',
    colorPaletteGray200: '#EEEEEE',
    colorPaletteGray300: '#E0E0E0',
    colorPaletteGray400: '#BDBDBD',
    colorPaletteGray500: '#9E9E9E',
    colorPaletteGray600: '#757575',
    colorPaletteGray700: '#616161',
    colorPaletteGray800: '#424242',
    colorPaletteGray900: '#212121',

    // Blues
    colorPaletteBlue50: '#E1F4F9',
    colorPaletteBlue100: '#B2E2F1',
    colorPaletteBlue200: '#82CFE8',
    colorPaletteBlue300: '#56BCDF',
    colorPaletteBlue400: '#38AEDA',
    colorPaletteBlue500: '#1EA1D5',
    colorPaletteBlue600: '#1694C8',
    colorPaletteBlue700: '#0B82B5',
    colorPaletteBlue800: '#0B71A1',
    colorPaletteBlue900: '#005282',

    // Alerts
    colorAlertWarningLight: '#FFF9C3',
    colorAlertError: '#F02B41',
    colorAlertErrorLight: '#FFC6CE',
    colorAlertSuccess: '#39870C',
    colorAlertSuccessLight: '#D6ECC1',

    // Backgrounds
    colorBackground: '#FFFFFF',

    // Text
    colorTextWhite: '#FFFFFF',
  },
}

// Derived colors

// Text
tokens.colors.colorText = tokens.colors.colorPaletteGray900
tokens.colors.colorTextLabel = tokens.colors.colorPaletteGray600
tokens.colors.colorTextInputLabel = tokens.colors.colorPaletteGray800
tokens.colors.colorTextLink = tokens.colors.colorPaletteBlue800
tokens.colors.colorTextLinkHover = tokens.colors.colorPaletteBlue900
tokens.colors.colorTextError = tokens.colors.colorAlertError

// Notification
tokens.colors.colorNotificationInfo = tokens.colors.colorPaletteBlue800
tokens.colors.colorNotificationInfoLight = tokens.colors.colorPaletteBlue100
tokens.colors.colorNotificationWarning = tokens.colors.brandPrimary

// Collapsible
tokens.colors.colorCollapsibleBorder = tokens.colors.colorPaletteGray300

// Button
tokens.colors.colorButtonPrimary = tokens.colors.brandPrimary
tokens.colors.colorButtonSecondary = tokens.colors.brandSecondary1

// Border
tokens.colors.colorBorderInput = tokens.colors.colorPaletteGray500
tokens.colors.colorBorderError = tokens.colors.colorAlertError

export { tokens }

const theme = {
  ...defaultTheme,

  name: 'componentencatalogus',
  tokens,
  breakpoints: Object.values(breakpoints)
    .splice(1)
    .map((bp) => `${bp}px`),
}

export default theme
