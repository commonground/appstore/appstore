// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export const userIsProductOwner = (user) => !!user && !!user.isProductOwner
export const userOwnsItem = (user, item) =>
  Boolean(item) &&
  Boolean(user) &&
  Boolean(item.owner) &&
  item.owner.id === user.id
